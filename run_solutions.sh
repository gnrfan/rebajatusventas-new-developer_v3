#!/bin/bash

echo "Problem 1"
echo "#########"
echo

PATH="solutions/problem-01/solution.md"
echo "Displaying $PATH..."
echo
/usr/bin/cat $PATH

echo
echo "Problem 2"
echo "#########"
echo

PATH="solutions/problem-02/concepts.md"
echo "Displaying $PATH..."
echo
/usr/bin/cat $PATH

PATH="solutions/problem-02/ci_outline.txt"
echo "Displaying $PATH..."
echo
/usr/bin/cat $PATH

echo
echo "Problem 3"
echo "#########"
echo

PATH="solutions/problem-03/README.md"
echo "Displaying $PATH..."
echo
/usr/bin/cat $PATH


echo
echo "Problem 4"
echo "#########"
echo

PATH="solutions/problem-04/README.md"
echo "Displaying $PATH..."
echo
/usr/bin/cat $PATH

echo "Running tests..."
cd solutions/problem-04/
/usr/local/bin/python tests.py

echo "Running code..."
/usr/local/bin/python runner.py

cd ../../

echo
echo "Problem 5"
echo "#########"
echo

PATH="solutions/problem-05/README.md"
echo "Displaying $PATH..."
echo
/usr/bin/cat $PATH

cd solutions/problem-05/

echo "Running code..."
/usr/local/bin/python solution.py

cd ../../
