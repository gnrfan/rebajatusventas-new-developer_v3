FROM python:latest

WORKDIR /app

COPY . .

ENTRYPOINT [ "bash", "run_solutions.sh" ]