import binascii
import base64

HEX_STRING = (
    "4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c2"
    "0616772656761646d6520616c2068616e676f75743a200d0a0d0a226d617274696e40"
    "6d656e646f7a61646576736f6c61722e636f6d2220706172612073616265722071756"
    "5206c6c656761737465206120657374612070617274652e0d0a0d0a47726163696173"
    "2c20792065737065726f20766572746520706f722061717565c3ad212e"
)

BASE64_STRING = (
    "U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY"
    "29uIHVuIG1lbnNhamUu"
)


def decode_messages():
    decoded_messages = []
    decoded_hex_string = binascii.unhexlify(HEX_STRING).decode('latin-1')
    decoded_base64_string = base64.b64decode(BASE64_STRING).decode('utf-8')
    decoded_messages.append(decoded_hex_string)
    decoded_messages.append(decoded_base64_string)
    print("\n".join(decoded_messages))


if __name__ == '__main__':
    decode_messages()
