## RTFM

* I try to apply RTFM to myself whenever possible but everyone is starting to use LLM in order to find shortcuts to things or come up with ideas these days
* I have send other people to read the manual when there's a considerable knowledge gap that a relatively quick explanation or guidance can work around, mostly to get to the point where actual collaboration is reasonable because the person is now familiar with the basics or the pre-requisites

## LMGTFY

* Sometimes when the answer was sort of obvious I've shared a letmegooglethat.com URL to a co-worker or colleague
* I've found myself using this website with friends for lots of more mundane, not programming-related stuff

## OS

* I primarily work with Ubuntu-derived distros like Zorin OS or POP_OS!
* I've used Mac OS X too for many years
* I also use Windows 10 regularly including the WSL 2

## Languages

* I primarily code in Python (2006 to 2024)
* I also worked with PHP for many years (1998 to 2006)
* I've occasionally worked professionaly with existing Java codebases
* I've occasionally completed full projects in C# and ASP.Net Core