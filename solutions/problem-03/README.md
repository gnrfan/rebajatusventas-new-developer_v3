# Portafolio

## Old Django projects

* All public projects of mine in my previous Github account

### Fortunes app

* Written in Django 1.1
* https://github.com/gnrfan/chichafortunes

### Custom Django Fields

* https://github.com/gnrfan/django-sequence-field
* https://github.com/gnrfan/django-mailgun-validation

### Easier XML manipulation

* https://github.com/gnrfan/python-easyxsd

### Small utility

* https://github.com/gnrfan/extract-values

### A old attempt at a simple REST framework

* https://github.com/gnrfan/django-restful

### HTTP mocking tool

* https://github.com/gnrfan/restmocker

## More modern Django projects

* COVID-19 medical survey project
* https://gitlab.com/gnrfan/estela/

## More recent code snippets

### MentorMatch

* Django app which is part of a larger portal
* Includes heavily mocked tests
