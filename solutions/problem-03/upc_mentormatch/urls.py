from django.conf.urls import url

from .views import mentors, views
urlpatterns = [
    url(r'^create_mentor/$', views.MentorCreateByCsvFormView.as_view(),
        name='create_mentor'),
    url(r'^home/$', views.HomeMentee.as_view(), name='home_mentee'),
    url(r'^mentee/$', views.FormMentee.as_view(), name='form_mentee'),
    url(r'^gracias/(?P<dni>[0-9]+)$',
        views.ThanksMentee.as_view(), name='thanks_mentee'),
    url(r'^consejos/$', views.TipsMentee.as_view(), name='consejos_mentee'),
    url(r'^preguntas/$', views.AskMentee.as_view(), name='ask_mentee'),
    url(r'^mentores/$', views.MentorsMentee.as_view(), name='mentore_mentee'),

    url(r'^mentors/$', mentors.MentorIndex.as_view(), name='mentor_index'),
    url(
        r'^mentors/form/$',
        mentors.MentorForm.as_view(),
        name='mentor_form'
    ),
    url(
        r'^mentors/login/$',
        mentors.MentorLogin.as_view(),
        name='mentor_login'
    ),
    url(
        r'^mentors/thanks/$',
        mentors.MentorThanks.as_view(),
        name='mentor_thanks'
    ),
    url(
        r'^mentors/email/$',
        mentors.MentorEmail.as_view(),
        name='mentor_email'
    ),
    url(
        r'^mentors/consejos/$',
        mentors.MentorTips.as_view(),
        name='mentor_consejos'
    ),
    url(
        r'^mentors/preguntas/$',
        mentors.MentorAsk.as_view(),
        name='mentor_preguntas'
    ),
]
