from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, View
from upc_mentormatch.utils import deobfuscate
from upc_mentormatch.forms import MentorLoginForm

class MentorIndex(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'mentors/index.html')

    def post(self, request, *args, **kwargs):
        form = MentorLoginForm(request.POST)
        if form.is_valid():
            mentor = form.load_mentor()
            response = HttpResponse("", status=302)
            response.set_cookie('MENTOR_TOKEN', mentor.get_access_token())
            response['Location'] = reverse_lazy('mentormatch_app:mentor_form')
            return response
        else:
            return render(request, 'mentors/index.html', locals())

class MentorForm(TemplateView):
    template_name = 'mentors/form.html'


class MentorLogin(View):
    def get(self, request, *args, **kwargs):
        token = self.request.GET.get('token', None)
        if not token:
            raise Http404

        try:
            self.object = deobfuscate(token)
        except ObjectDoesNotExist:
            raise Http404

        response = HttpResponse("", status=302)
        response.set_cookie('MENTOR_TOKEN', self.object.get_access_token())
        response['Location'] = reverse_lazy('mentormatch_app:mentor_form')
        return response


class MentorThanks(TemplateView):
    template_name = 'mentors/thanks.html'


class MentorEmail(TemplateView):
    template_name = 'mentors/email.html'


class MentorAsk(TemplateView):
    template_name = 'mentors/preguntas.html'


class MentorTips(TemplateView):
    template_name = 'mentors/consejos.html'
