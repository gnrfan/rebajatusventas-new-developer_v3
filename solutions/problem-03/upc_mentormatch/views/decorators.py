import json
from collections import OrderedDict
from django.http import HttpResponse
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from upc_applicants.models import Applicant
from upc_mentormatch.validators import validate_applicant_status_for_mentormatch


class MentormatchApplicantRequiredDecorator(object):

    def __call__(self, view):
        def decorator(request, *args, **kwargs):
            self.request = request
            if not self.check_authenticated_user():
                return self.unauthorized_response()
            self.set_applicant()
            if not self.applicant:
                return self.generic_forbidden_response()
            if not self.validate_applicant():
                return self.validated_forbidden_response()
            return view(request, *args, **kwargs)
        return decorator

    def check_authenticated_user(self):
        try:
            return self.request.user.is_authenticated()
        except AttributeError:
            return False

    def set_applicant(self):
        try:
            self.applicant = self.request.user.applicant
        except Applicant.DoesNotExist:
            self.applicant = None

    def validate_applicant(self):
        try:
            validate_applicant_status_for_mentormatch(self.applicant)
            return True
        except ValidationError as the_exception:
            self.the_exception = the_exception
            return False

    def unauthorized_response(self):
        message = _("This endpoint requires user authentication")
        return self.json_response(message, status_code=401)

    def generic_forbidden_response(self):
        message = _(''.join([
            "The currently authenticated user ",
            "is forbidden to access this resource"
        ]))
        return self.json_response(message, status_code=403)

    def validated_forbidden_response(self):
        message = self.the_exception.message
        return self.json_response(message, status_code=403)

    def json_response(self, message, status_code=200):
        doc = OrderedDict()
        doc["error"] = str(message)
        json_payload = json.dumps(doc, indent=4)
        response = HttpResponse(json_payload)
        response.status_code = status_code
        response['Content-Type'] = 'application/json'
        return response


require_mentormatch_applicant = MentormatchApplicantRequiredDecorator()
