import csv
from io import StringIO

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse_lazy, reverse
from django.db import transaction
from django.template.loader import get_template
from django.views.generic import FormView, TemplateView
from upc_applicants.models import Period
from upc_commons.models import Career
from ..forms import MentorCreateCSV
from ..models import CategoryTopic, Mentor, MentorAvailability, Mentorship
from ..utils.helpers import headers_in_list


class MentorCreateByCsvFormView(FormView):
    form_class = MentorCreateCSV
    template_name = 'mentor_create_csv.html'
    success_url = reverse_lazy('mentormatch_app:create_mentor')


    def form_valid(self, form):
        file = StringIO(form.cleaned_data['file'].read().decode())
        reader = csv.DictReader(file)
        file_headers = reader.fieldnames
        valid, msg = headers_in_list(file_headers)
        count = 0
        if valid:
            try:
                with transaction.atomic():
                    for n,row in enumerate(reader, 1):
                        count = n
                        mentor = Mentor.objects.filter(document_number=row['document_number'])
                        if mentor.exists():
                            mentor = mentor.first()
                        else:
                            mentor = Mentor()
                        mentor.name = row['name']
                        mentor.father_name = row['father_name']
                        mentor.mother_name = row['mother_name']
                        mentor.document_number = row['document_number']
                        mentor.is_active = row['is_active']
                        mentor.career = Career.objects.get(id=row['career_id'])
                        mentor.availability = MentorAvailability.objects.get(id=row['availability_id'])
                        mentor.graduation_year = row['graduation_year']
                        mentor.email = row['email'] if row['email'] != '' else ''
                        mentor.phone = row['phone'] if row['phone'] != '' else ''
                        mentor.linkedin = row['linkedin'] if row['linkedin'] != '' else ''
                        mentor.skype = row['skype'] if row['skype'] != '' else ''
                        mentor.is_email_preferred = row['is_email_preferred'] if row['is_email_preferred'] != '' else ''
                        mentor.is_phone_preferred = row['is_phone_preferred'] if row['is_phone_preferred'] != '' else ''
                        mentor.is_linkedin_preferred = row['is_linkedin_preferred'] if row['is_linkedin_preferred'] != '' else ''
                        mentor.is_skype_preferred = row['is_skype_preferred'] if row['is_skype_preferred'] != '' else ''
                        mentor.is_active=True
                        mentor.save()
                        if row['period'] != '':
                            period = Period.objects.get(code=row['period'])
                            mentor.periods.add(period)
                        if row['topics'] != '':
                            topics = list(row['topics'].split(','))
                            mentor.topics_of_interest.clear()
                            for topic in topics:
                                topic = CategoryTopic.objects.get(id=topic)
                                mentor.topics_of_interest.add(topic)
            except Exception as e:
                form.add_error('file', 'error en la linea {}: {}'.format(count, e))
                return self.form_invalid(form)
        else:
            form.add_error('file', msg)
            return self.form_invalid(form)
        return super().form_valid(form)


class HomeMentee(TemplateView):
    template_name = 'mentee/mentee_home.html'


class FormMentee(TemplateView):
    template_name = 'mentee/mentee.html'


class ThanksMentee(TemplateView):
    template_name = 'mentee/mentorship.html'

    def get_context_data(self, **kwargs):
        mentorship = Mentorship.objects.filter(id=self.kwargs['dni']).first()
        context = super(ThanksMentee, self).get_context_data(**kwargs)
        mail_plain_msg01 = get_template('mentee/email.txt')
        mail_html_msg01 = get_template('mentee/email.html')
        data_email = {
            'mentee_name': mentorship.mentee.applicant.first_name,
            'mentor_name': mentorship.mentor.name,
            'topic_name': mentorship.topic.name,
            'url_absolute': "{}://{}".format(self.request.scheme,
                                             self.request.META['HTTP_HOST'])
        }
        from_email = 'viveupc@upc.edu.pe'

        data_email_mentee = data_email
        data_email_mentee['mentor_gradutation_year'] = mentorship.mentor.graduation_year
        data_email_mentee['mentor_career'] = mentorship.mentor.career.name.es
        data_email_mentee['url_tips'] = settings.URL_PREFIX + reverse('mentormatch_app:consejos_mentee')
        data_email_mentee['mentor_phone'] = mentorship.mentor.phone
        data_email_mentee['mentor_email'] = mentorship.mentor.email
        if mentorship.mentor.skype:
            data_email_mentee['mentor_skype'] = mentorship.mentor.skype
        if mentorship.mentor.linkedin:
            data_email_mentee['mentor_linkedin'] = mentorship.mentor.linkedin

        wxs = mentorship.mentor.work_experiences.all()
        wxs_list = list()
        for wx in wxs:
            wx_str = '{} {} - {} - ({} - {})'
            wx_date = wx.ending_date - wx.starting_date

            if wx_date.days >= 365:
                wx_time = wx_date.days // 365
                wx_time_str = 'Con {} años en'.format(wx_time)
                if wx_time == 1:
                    wx_time_str = 'Con {} año en'.format(wx_time)
            elif wx_date.days >= 30:
                wx_time = wx_date.days // 30
                wx_time_str = 'Con {} meses en'.format(wx_time)
                if wx_time == 1:
                    wx_time_str = 'Con {} mes en'.format(wx_time)
            else:
                wx_time = wx_date.days
                wx_time_str = 'Con {} dias en'.format(wx_time)

            wx_str = wx_str.format(wx_time_str, wx.company, wx.position,
                                   wx.starting_date, wx.ending_date)
            wxs_list.append(wx_str)
        data_email_mentee['wxs'] = wxs_list
        subject = 'Hola %s' % mentorship.mentee.applicant.first_name
        to = mentorship.mentee.applicant.email
        text_content = mail_plain_msg01.render(data_email_mentee)
        html_content = mail_html_msg01.render(data_email_mentee)
        msg01 = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg01.attach_alternative(html_content, "text/html")
        if not mentorship.send_email_mentee:
            msg01.send()
            mentorship.send_email_mentee = True
            mentorship.save(update_fields=['send_email_mentee'])

        data_email_mentor = data_email
        data_email_mentor['mentee_phone'] = mentorship.mentee.applicant.phone_mobile
        data_email_mentor['mentee_email'] = mentorship.mentee.applicant.email
        data_email_mentor['url_tips'] = settings.URL_PREFIX
        mail_plain_msg02 = get_template('mentors/email.txt')
        mail_html_msg02 = get_template('mentors/email.html')
        subject = 'Hola %s' % mentorship.mentor.name
        to = mentorship.mentor.email
        text_content = mail_plain_msg02.render(data_email_mentor)
        html_content = mail_html_msg02.render(data_email_mentor)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        if not mentorship.send_email_mentor:
            msg.send()
            mentorship.send_email_mentor = True
            mentorship.save(update_fields=['send_email_mentor'])
        if mentorship.mentee:
            context['mentee_name'] = mentorship.mentee.applicant.full_name
        return context


class TipsMentee(TemplateView):
    template_name = 'mentee/consejos.html'


class AskMentee(TemplateView):
    template_name = 'mentee/ask.html'


class MentorsMentee(TemplateView):
    template_name = 'mentee/mentores.html'

