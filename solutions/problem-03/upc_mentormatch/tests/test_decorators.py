import json
import unittest
from unittest import mock

from django.http import HttpResponse
from django.test import Client

from upc_commons.models import IdentityDocumentType
from upc_mentormatch.views.decorators import require_mentormatch_applicant


class MentorMatchDecoratorsTestCase(unittest.TestCase):

    def get_mock_non_mentormatch_applicant(self):
        applicant = mock.MagicMock()
        applicant.__str__.return_value = 'upc.applicant@maildrop.cc'
        applicant.status = self.get_mock_non_mentormatch_applicant_status()
        applicant.first_name = 'Carlos Alberto'
        applicant.paternal_last_name = 'Torres'
        applicant.maternal_last_name = 'Caballero'
        applicant.email = 'upc.applicant@maildrop.cc'
        applicant.idt = IdentityDocumentType(name='DNI')
        applicant.idn = '23455432'
        return applicant


    def get_mock_non_mentormatch_applicant_status(self):
        status = mock.Mock()
        status.pk = 20000
        status.description = 'Some required applicant status'
        return status


    def get_mock_user_is_authenticated_true(self):
        user = mock.MagicMock()
        user.is_authenticated.return_value = True
        user.is_superuser.return_value = False
        user.username = 'UserTest'
        user.email = 'lucuma@lucumalabs.com'
        user.applicant = self.get_mock_non_mentormatch_applicant()
        return user


    def get_mock_user_is_authenticated_false(self):
        user = mock.MagicMock()
        user.is_authenticated.return_value = False
        user.is_superuser.return_value = False
        user.username = 'UserTest'
        user.email = 'lucuma@lucumalabs.com'
        return user


    def get_mock_required_applicant_status(self):
        status = mock.Mock()
        status.pk = 10000
        status.description = 'Some required applicant status'
        return status


    def test_decorators_mentor_match_user_is_authenticated_false(self):

        @require_mentormatch_applicant
        def as_view(request):
            return HttpResponse()
            

        request = Client().get('/test/decorator/')
        request.user = self.get_mock_user_is_authenticated_false()

        resp = as_view(request)
        content = json.loads(resp.content)

        self.assertEqual(
            content['error'],
            'This endpoint requires user authentication'
        )
        self.assertEqual(resp.status_code, 401)


    @mock.patch('upc_mentormatch.validators.EntityStatus')
    @mock.patch('upc_mentormatch.validators.mentormatch_settings')
    def test_applicant_required_decorator_forbidden(self, s, es):

        @require_mentormatch_applicant
        def as_view(request):
            return HttpResponse()

        s.MENTORMATCH_APPLICANT_STATUS_PK = 10000
        es.objects.get.return_value = self.get_mock_required_applicant_status()

        request = Client().get('/test/decorator/')
        request.user = self.get_mock_user_is_authenticated_true()

        resp = as_view(request)
        content = json.loads(resp.content)

        expected_message = "".join([
            'Applicant upc.applicant@maildrop.cc has a status ',
            'value of "Some required applicant status" (20000)',
            ' instead of "Some required applicant status"',
            ' (10000) which is required as per the value of ',
            'the MENTORMATCH_APPLICANT_STATUS_PK settings variable'])

        self.assertEqual(content['error'], expected_message)
        self.assertEqual(resp.status_code, 403)

