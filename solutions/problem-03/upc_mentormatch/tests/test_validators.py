# -*- coding:utf-8 -*-

import unittest
from unittest import mock
from django.core.exceptions import ValidationError
from upc_mentormatch.validators import *

class MentorMatchValidatorsTestCase(unittest.TestCase):
    
    def test_validate_parent_is_another_record_should_raise_exception(self):
        record = self.get_mock_category()
        record.parent = record # record is its own parent
        args = ()
        kwargs = {'value': record}
        self.assertRaises(ValidationError, validate_parent_is_another_record, *args, **kwargs)

    def test_validate_topic_parent_is_category_should_raise_exception(self):
        record = self.get_mock_topic()
        record.parent = self.get_mock_topic() # not a category, another topic
        args = ()
        kwargs = {'value': record}
        self.assertRaises(ValidationError, validate_topic_parent_is_category, *args, **kwargs)

    def test_validate_category_has_no_parent_should_raise_exception(self):
        record = self.get_mock_category()
        record.parent = self.get_mock_category() # a category with a category as parent
        args = ()
        kwargs = {'value': record}
        self.assertRaises(ValidationError, validate_category_has_no_parent, *args, **kwargs)


    # ApplicantStatusValidator

    @mock.patch('upc_mentormatch.validators.EntityStatus')
    @mock.patch('upc_mentormatch.validators.mentormatch_settings')
    def test_applicant_status_validator_raises_mismatch_exception(self, s, es):
        # Mock settings object
        s.MENTORMATCH_APPLICANT_STATUS_PK = 10000
        # Mock EntityStatus class to retrieve desired applicant status
        es.objects.get.return_value = self.get_mock_required_applicant_status()
        # Mock applicant
        applicant = self.get_mock_applicant()
        # Capture exception
        with self.assertRaises(ValidationError) as cm:
            validate_applicant_status_for_mentormatch(applicant)
        the_exception = cm.exception
        # Check exception message
        expect_exception_message = ''.join([
            'Applicant upc.applicant@maildrop.cc has a status value of ',
            '"Applicant Status #1" (1) instead of ',
            '"Some required applicant status" (10000) ',
            'which is required as per the value of the ',
            'MENTORMATCH_APPLICANT_STATUS_PK settings variable'
        ])
        self.assertEqual(the_exception.message, expect_exception_message)


    @mock.patch('upc_mentormatch.validators.mentormatch_settings')
    def test_applicant_status_validator_raises_config_exception(self, s):
        # Mock settings object
        s.mock_add_spec([]) # Spec with no attributes so it raises AttributeError
        # Mock applicant
        applicant = self.get_mock_applicant()
        # Capture exception
        with self.assertRaises(ValidationError) as cm:
            validate_applicant_status_for_mentormatch(applicant)
        the_exception = cm.exception
        # Check exception message
        expected_exception_message = ''.join([
            'Applicant "upc.applicant@maildrop.cc"\'s status can not be ',
            'validated because the MENTORMATCH_APPLICANT_STATUS_PK settings ',
            'variable is missing from the project\'s configuration'
        ])
        self.assertEqual(the_exception.message, expected_exception_message)

    @mock.patch('upc_mentormatch.validators.EntityStatus')
    @mock.patch('upc_mentormatch.validators.mentormatch_settings')
    def test_applicant_status_validator_raises_missing_status_ex(self, s, es):
        # Mock settings object
        s.MENTORMATCH_APPLICANT_STATUS_PK = 10000
        # Mock EntityStatus class to retrieve desired applicant status
        es.objects.get.return_value = self.get_mock_required_applicant_status()
        # Mock applicant
        applicant = self.get_mock_applicant()
        # Capture exception
        with self.assertRaises(ValidationError) as cm:
            validate_applicant_status_for_mentormatch(applicant)
        the_exception = cm.exception
        # Check exception message
        expected_exception_message = ''.join([
            'Applicant upc.applicant@maildrop.cc has a status value of ',
            '"Applicant Status #1" (1) instead of ',
            '"Some required applicant status" (10000) which is required ',
            'as per the value of the MENTORMATCH_APPLICANT_STATUS_PK ',
            'settings variable'
        ])
        self.assertEqual(the_exception.message, expected_exception_message)


    def get_mock_category(self):
        category = mock.Mock()
        category.record_type = 'category'
        category.parent = None
        return category

    def get_mock_topic(self):
        topic = mock.Mock()
        topic.record_type = 'topic'
        topic.parent = self.get_mock_category()
        return topic

    def get_mock_applicant(self):
        applicant = mock.MagicMock()
        applicant.__str__.return_value = 'upc.applicant@maildrop.cc'
        applicant.status.pk = 1
        applicant.status.description = 'Applicant Status #1'
        return applicant

    def get_mock_required_applicant_status(self):
        status = mock.Mock()
        status.pk = 10000
        status.description = 'Some required applicant status'
        return status

    # MaxMenteeMentorshipsPerPeriodValidator

    @mock.patch('upc_mentormatch.validators.mentormatch_settings')
    def test_max_mentorships_raises_config_exception(self, s):
        # Mock settings object
        s.mock_add_spec([]) # Spec with no attributes so it raises AttributeError
        # Mock mentorship
        mentorship = self.get_mock_mentorship()
        # Capture exception
        with self.assertRaises(ValidationError) as cm:
            validate_max_number_of_mentee_mentorships_per_period(mentorship)
        the_exception = cm.exception
        # Check exception message
        expected_exception_message = ''.join([
            'The system can not validate if the The max number of ',
            'mentorships per period has been reached for mentee ',
            '"Carlos Alberto Torres Caballero (DNI 23455432)" ',
            'because the MENTORMATCH_MENTEE_MAX_NUMBER_OF_',
            'MENTORSHIPS_PER_PERIOD settings variable is ',
            'missing from the project\'s configuration'
        ])
        self.assertEqual(the_exception.message, expected_exception_message)

    @mock.patch('upc_mentormatch.validators.mentormatch_settings')
    def test_max_mentorships_raises_max_reached_exception(self, s):
        max_value = 3
        s.MENTORMATCH_MENTEE_MAX_NUMBER_OF_MENTORSHIPS_PER_PERIOD = max_value
        # Mock mentorship
        mentorship = self.get_mock_mentorship()
        qs = mock.Mock()
        qs.count.return_value = max_value
        mentorship.mentee.mentorships_as_mentee.filter.return_value = qs
        # Capture exception
        with self.assertRaises(ValidationError) as cm:
            validate_max_number_of_mentee_mentorships_per_period(mentorship)
        the_exception = cm.exception
        # Check exception message
        expected_exception_message = ''.join([
            'The mentee "Carlos Alberto Torres Caballero (DNI 23455432)" ',
            'has reached the maximum number of 3 allowed mentorships ',
            'per period'
        ])
        self.assertEqual(the_exception.message, expected_exception_message)

    def get_mock_mentorship(self):
        mentorship = mock.MagicMock()
        mentorship.mentee.__str__.return_value = \
            'Carlos Alberto Torres Caballero (DNI 23455432)'
        return mentorship
        