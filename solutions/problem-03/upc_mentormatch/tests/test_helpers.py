import unittest
from unittest import mock

from upc_mentormatch.utils.helpers import get_career_availability


class GetCareerAvailabilityTest(unittest.TestCase):

    @mock.patch('upc_mentormatch.models.Mentor')
    def test_get_career_availability_true(self, m):
        queryset_mock = mock.Mock()
        queryset_mock.exists.return_value = True
        m.objects.filter.return_value = queryset_mock
        result = get_career_availability('Some-Career-Code-00')
        self.assertTrue(result)

    @mock.patch('upc_mentormatch.models.Mentor')
    def test_get_career_availability_false(self, m):
        queryset_mock = mock.Mock()
        queryset_mock.exists.return_value = False
        m.objects.filter.return_value = queryset_mock
        result = get_career_availability('Some-Career-Code-00')
        self.assertFalse(result)
