# -*- coding:utf-8 -*-

import uuid
import unittest
from unittest import mock
from upc_mentormatch.utils import *

class MentorMatchUtilsTestCase(unittest.TestCase):

    def test_file_get_extension_should_extract_extension(self):
        filename = 'some-image.jpg'
        ext = get_file_extension(filename)
        self.assertGreater(len(ext), 0)

    def test_file_get_extension_should_extract_empty_extension(self):
        filename = 'passwd'
        ext = get_file_extension(filename)
        self.assertEqual(ext, '')

    def test_file_get_extension_should_normalize_to_uppercase(self):
        filename = 'some-image.jpg'
        kwargs = dict(normalize=True, normalization='uppercase')
        ext = get_file_extension(filename, **kwargs)
        self.assertEqual(ext, '.JPG')

    def test_file_get_extension_should_normalize_to_uppercase(self):
        filename = 'some-image.JPG'
        kwargs = dict(normalize=True, normalization='lowercase')
        ext = get_file_extension(filename, **kwargs)
        self.assertEqual(ext, '.jpg')
    
    def test_file_get_extension_should_normalize_to_lowercase_by_default(self):
        filename = 'some-image.JPG'
        ext = get_file_extension(filename, normalize=True)
        self.assertEqual(ext, '.jpg')

    @mock.patch('upc_mentormatch.utils.uploads.mentormatch_settings')
    @mock.patch('upc_mentormatch.utils.uploads.uuid')
    def test_category_or_topic_uuid_upload_to_should_return_path(self, mock_uuid, mock_settings):
        uuid4_string = '359f4e58-311f-4a69-b09f-d2086cc53332'
        test_path = 'mentormatch/categories_or_topics'
        ext = '.jpg'
        filename = ''.join(['image', ext])
        mock_uuid.uuid4.return_value = uuid.UUID(uuid4_string)
        mock_settings.MENTORMATCH_CATEGORY_TOPIC_IMAGE_FOLDER = test_path
        upload_to = category_or_topic_uuid_upload_to(mock.Mock(), filename)
        expected_path = ''.join([test_path, '/', uuid4_string, ext])
        self.assertEqual(upload_to, expected_path)

    @mock.patch('upc_mentormatch.utils.uploads.mentormatch_settings')
    @mock.patch('upc_mentormatch.utils.uploads.uuid')
    def test_img_uuid_upload_to_should_return_path(self, mock_uuid, mock_settings):
        uuid4_string = '359f4e58-311f-4a69-b09f-d2086cc53332'
        test_path = 'mentormatch/mentor'
        ext = '.jpg'
        filename = ''.join(['image', ext])
        mock_uuid.uuid4.return_value = uuid.UUID(uuid4_string)
        mock_settings.MENTORMATCH_MENTOR_IMAGE_FOLDER = test_path
        upload_to = img_uuid_upload_to(mock.Mock(), filename)
        expected_path = ''.join([test_path, '/', uuid4_string, ext])
        self.assertEqual(upload_to, expected_path)
