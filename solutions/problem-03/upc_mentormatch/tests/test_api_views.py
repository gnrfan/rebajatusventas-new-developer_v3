import os
import unittest
from copy import copy
from unittest import mock

from PIL import Image
from rest_framework import status
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.test import APIRequestFactory
from django.core.files.uploadedfile import SimpleUploadedFile
from upc_mentormatch.api.views import (
    MentorAvailabilityView,
    CareerAvailabilityView,
    MentorViewSet,
    CategoryViewSet,
    TopicViewSet,
    PhotoViewSet,
)
from upc_mentormatch.utils import headers_in_list


class ApiViewsTests(unittest.TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.DEFAULT_HANDLER = api_settings.EXCEPTION_HANDLER

        def exception_handler(exc, request):
            data = {}
            data['status_code'] = exc.status_code
            if isinstance(exc.detail, list):
                data['error'] = exc.detail[0]
            else:
                data['error'] = exc.detail
            return Response(data, status=exc.status_code)

        api_settings.EXCEPTION_HANDLER = exception_handler

    def tearDown(self):
        api_settings.EXCEPTION_HANDLER = self.DEFAULT_HANDLER

    # MentorAvailabilityView

    @mock.patch('upc_mentormatch.api.views.MentorAvailability')
    def test_mentor_availability_view(self, a):

        mock_queryset = [{"code": 1, "description": "Una vez por semana"},
                         {"code": 2, "description": "dos vez por semana"}]

        a.objects.all.return_value = mock_queryset
        request = self.factory.get('/')
        view = MentorAvailabilityView.as_view()
        response = view(request)
        response.render()
        expected = mock_queryset
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['Content-Type'], "application/json")
        self.assertEqual(response.data, expected)

    # CategoryViewSet

    @mock.patch('upc_mentormatch.api.views.CategoryTopic')
    def test_category_collection_should_support_is_active_parameter(self, ct):
        # Testing is_active=True
        request = self.factory.get('/?is_active=T')
        view = CategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        ct.objects.categories.assert_called_with(is_active=True)
        # Testing is_active=False
        request = self.factory.get('/?is_active=F')
        view = CategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        ct.objects.categories.assert_called_with(is_active=False)
        # Testing is_active=both
        request = self.factory.get('/?is_active=both')
        view = CategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        ct.objects.categories.assert_called_with()

    @mock.patch('upc_mentormatch.api.views.CategoryTopic')
    def test_category_collection_should_support_category_parameter(self, ct):

        # Testing if the queryset gets called properly
        request = self.factory.get('/?category=1')
        view = CategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        ct.objects.categories.assert_called_with(is_active='both', pk=1)

        # Testing if the category parameter value gets validated
        request = self.factory.get('/?category=hello')
        view = CategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.data['status_code'],
                         status.HTTP_400_BAD_REQUEST)

        # Testing the happy path should return a collection with one item
        # Setting up the mocking data structures
        category = self.get_category_mock(
            pk=1,
            name='Category #1',
            image_url='http://assets.university.com/categories/category-001.png',
            endpoint_url='http://api.university.com/categories/1'
        )
        topics = []
        topics.append(
            self.get_topic_mock(
                pk=1000,
                name="Topic #1000",
                endpoint_url='http://api.university.com/categories/1/topics/1000',
                parent=category
            )
        )
        category.children.filter.return_value = topics
        ct.objects.categories.return_value = [category]
        # Performing the request
        request = self.factory.get('/?category=1')
        view = CategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        response.render()
        expected_content = b''.join([
            b'[{"code":1,"name":"Category #1",',
            b'"url":"http://api.university.com/categories/1",',
            b'"image_url":"http://assets.university.com/categories/category-001.png",',
            b'"is_active":true,"topics":[{"code":1000,"name":"Topic #1000",',
            b'"url":"http://api.university.com/categories/1/topics/1000",',
            b'"image_url":null,',
            b'"parent_category_url":"http://api.university.com/categories/1",',
            b'"is_active":true}]}]'
        ])
        ct.objects.categories.assert_called_with(is_active='both', pk=1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content, expected_content)

    def test_category_collection_should_support_expected_parameters_only(self):
        # Testing if the category parameter value gets validated
        request = self.factory.get('/?xyz=1&opq=2')
        view = CategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        response.render()
        self.assertEqual(response.data['status_code'],
                         status.HTTP_400_BAD_REQUEST)
        expected_message = "Query parameters 'xyz' and 'opq' are not supported."
        self.assertEqual(response.data['error'], expected_message)

    @mock.patch('upc_mentormatch.api.views.get_object_or_404')
    @mock.patch('upc_mentormatch.api.views.CategoryTopic')
    def test_category_detail_should_support_retrieving_one_category(self, ct,
                                                                    g):

        # Testing the happy path should return a collection with one item
        # Setting up the mocking data structures
        category = self.get_category_mock(
            pk=1,
            name='Category #1',
            image_url='http://assets.university.com/categories/category-001.png',
            endpoint_url='http://api.university.com/categories/1'
        )
        topics = []
        topics.append(
            self.get_topic_mock(
                pk=1000,
                name="Topic #1000",
                endpoint_url='http://api.university.com/categories/1/topics/1000',
                parent=category
            )
        )
        category.children.filter.return_value = topics
        g.return_value = category
        # Performing the request
        request = self.factory.get('/')
        view = CategoryViewSet.as_view({'get': 'retrieve'})
        response = view(request, category_pk=1)
        response.render()
        expected_content = b''.join([
            b'{"code":1,"name":"Category #1",',
            b'"url":"http://api.university.com/categories/1",',
            b'"image_url":"http://assets.university.com/categories/category-001.png",',
            b'"is_active":true,"topics":[{"code":1000,"name":"Topic #1000",',
            b'"url":"http://api.university.com/categories/1/topics/1000",',
            b'"image_url":null,',
            b'"parent_category_url":"http://api.university.com/categories/1",',
            b'"is_active":true}]}'
        ])
        ct.objects.categories.assert_called_with(is_active='both')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content, expected_content)


    def test_category_detail_should_support_expected_parameters_only(self):
        # Testing if the category parameter value gets validated
        request = self.factory.get('/?xyz=1&opq=2')
        view = CategoryViewSet.as_view({'get': 'retrieve'})
        response = view(request)
        response.render()
        self.assertEqual(response.data['status_code'], status.HTTP_400_BAD_REQUEST)
        expected_message = "Query parameters not supported."
        self.assertEqual(response.data['error'], expected_message)

    def test_topic_collection_should_return_implementation_error(self):
        # Testing if the category parameter value gets validated
        request = self.factory.get('/')
        view = TopicViewSet.as_view({'get': 'list'})
        response = view(request, category_pk=1)
        response.render()
        self.assertEqual(response.data['status_code'], status.HTTP_501_NOT_IMPLEMENTED)
        expected_message = "Not implemented. See category collection or category detail for topics"
        self.assertEqual(response.data['error'], expected_message)

    @mock.patch('upc_mentormatch.api.views.get_object_or_404')
    @mock.patch('upc_mentormatch.api.views.CategoryTopic')
    def test_topic_detail_should_support_retrieving_one_topic(self, ct, g):

        # Testing the happy path should return a collection with one item
        # Setting up the mocking data structures
        category = self.get_category_mock(
            pk=1,
            endpoint_url='http://api.university.com/categories/1'
        )
        topic = self.get_topic_mock(
            pk=1000,
            name="Topic #1000",
            endpoint_url='http://api.university.com/categories/1/topics/1000',
            parent=category
        )

        mock_qs = [{}, {}]
        ct.objects.topics.return_value = mock_qs
        g.return_value = topic

        # Performing the request
        request = self.factory.get('/')
        view = TopicViewSet.as_view({'get': 'retrieve'})
        response = view(request, category_pk=topic.parent.pk, topic_pk=topic.pk)
        response.render()
        expected_content = b''.join([
            b'{"code":1000,"name":"Topic #1000",',
            b'"url":"http://api.university.com/categories/1/topics/1000",',
            b'"image_url":null,',
            b'"parent_category_url":"http://api.university.com/categories/1",',
            b'"is_active":true}'
        ])

        # Assertions
        ct.objects.topics.assert_called_with(is_active='both')
        g.assert_called_with(mock_qs, pk=topic.pk, parent__pk=category.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content, expected_content)


    def test_topic_detail_should_support_expected_parameters_only(self):
        # Testing if the category parameter value gets validated
        request = self.factory.get('/?xyz=1&opq=2')
        view = CategoryViewSet.as_view({'get': 'retrieve'})
        response = view(request)
        response.render()
        self.assertEqual(response.data['status_code'], status.HTTP_400_BAD_REQUEST)
        expected_message = "Query parameters not supported."
        self.assertEqual(response.data['error'], expected_message)

    def get_topic_mock(self, **kwargs):
        kwargs['record_type'] = 'topic'
        return self.get_category_or_topic_mock(**kwargs)

    def get_category_mock(self, **kwargs):
        kwargs['record_type'] = 'category'
        obj = self.get_category_or_topic_mock(**kwargs)
        if 'topics' in kwargs:
            obj.children.filter.return_value = topics
        return obj

    def get_category_or_topic_mock(self, **kwargs):
        obj = mock.Mock()
        obj.pk = kwargs.get('pk', 1)
        obj.name = kwargs.get('name', 'Some name')
        obj.record_type = kwargs.get('record_type', 'topic')
        obj.parent = kwargs.get('parent', None)
        obj.is_active = kwargs.get('is_active', True)
        obj.get_image_url.return_value = kwargs.get('image_url', None)
        obj.get_endpoint_url.return_value = kwargs.get('endpoint_url', None)
        return obj

    def get_mentor_mock(self, **kwargs):
        obj = mock.Mock()
        obj.pk = kwargs.get('pk', 1)
        obj.name = kwargs.get('name', 'Some name')
        obj.document_number = '12345678'
        obj.topics_of_interest = self.get_category_or_topic_mock()
        obj.availability = self.get_availability_mock()
        obj.photo = kwargs.get('photo', '/photo/path/img.png')
        return obj

    def get_availability_mock(self, **kwargs):
        obj = mock.Mock()
        obj.pk = kwargs.get('pk', 1)
        obj.code = kwargs.get('code', '0123')
        obj.description = kwargs.get('description', 'una vez al mes')
        return obj
        
    # Mentor
    @mock.patch('upc_mentormatch.api.views.Mentor')
    def test_mentor_view_should_support_parameter(self, m):
        # Testing is_active_mentor=True
        request = self.factory.get('/?is_active_mentor=t')
        view = MentorViewSet.as_view({'get': 'list'})
        view(request)
        m.objects.filter.assert_called_with(is_active=True)

        # Testing is_active_mentor=False
        request = self.factory.get('/?is_active_mentor=F')
        view = MentorViewSet.as_view({'get': 'list'})
        view(request)
        m.objects.filter.assert_called_with(is_active=False)

        # Testing is_active_mentor=both
        request = self.factory.get('/?is_active_mentor=both')
        view = MentorViewSet.as_view({'get': 'list'})
        view(request)
        m.objects.filter.assert_called_with(is_active='both')

        # Testing identity param
        request = self.factory.get('/?identity=12345678')
        view = MentorViewSet.as_view({'get': 'list'})
        view(request)
        m.objects.filter.assert_called_with(document_number='12345678')

        # Testing topic param
        request = self.factory.get('/?topic=2000')
        view = MentorViewSet.as_view({'get': 'list'})
        view(request)
        m.objects.filter.assert_called_with(topics_of_interest__pk='2000')

        # Testing career param
        request = self.factory.get('/?career=unbreakable')
        view = MentorViewSet.as_view({'get': 'list'})
        view(request)
        m.objects.filter.assert_called_with(
            career__name__es__icontains='unbreakable')

        # Testing join params
        request = self.factory.get(
            '/?identity=12345678&topic=2000&career=unbreakable')
        view = MentorViewSet.as_view({'get': 'list'})
        view(request)
        m.objects.filter.assert_called_with(
            document_number='12345678',
            topics_of_interest__pk='2000',
            career__name__es__icontains='unbreakable')

    @mock.patch('upc_mentormatch.api.views.Mentor')
    def test_mentor_upload_photo_valid(self, m):
        img = Image.new('RGB', (100,100), color='red')
        img.save('/tmp/mentor_photo.png')
        with open('/tmp/mentor_photo.png', 'rb') as photo:
            f = photo.read()
            b = bytearray(f)            
            img_upload = SimpleUploadedFile('imagen.png', b, content_type='image/png')
            data = {'photo': img_upload}
            request = self.factory.post('/', data, format='multipart')
            view = PhotoViewSet.as_view({'post': 'post'})
            response = view(request)
            response.render()
            self.assertEquals(response.status_code, 200)
            msg = 'Foto guardada'
            self.assertEquals(response.data['detail'], msg)
        os.remove('/tmp/mentor_photo.png')

    @mock.patch('upc_mentormatch.api.views.Mentor')
    def test_mentor_upload_photo_invalid(self, m):
        img = Image.new('RGB', (100, 100), color='red')
        img.save('/tmp/mentor_photo.png')
        with open('/tmp/mentor_photo.png', 'rb') as photo:
            f = photo.read()
            b = bytearray(f)
            img_upload = SimpleUploadedFile('imagen.xxx', b, content_type='image/png')
            data = {'photo': img_upload}
            request = self.factory.post('/', data, format='multipart')
            view = PhotoViewSet.as_view({'post': 'post'})
            response = view(request)
            response.render()
            self.assertEquals(response.status_code, 400)
            msg = 'Extension de archivo no permitido, extensiones validas: jpeg, jpg, png'
            self.assertEquals(response.data['detail'], msg)
        os.remove('/tmp/mentor_photo.png')

    @mock.patch('upc_mentormatch.api.views.get_object_or_404')
    def xtest_mentor_view(self, g):
        mentor = self.get_mentor_mock(
            pk = 2,
            name = 'Some name',
            document_number = '12345678',
            topics_of_interest = self.get_category_or_topic_mock(),
            availability=self.get_availability_mock()
        )
        request = self.factory.get('/')
        view = MentorViewSet.as_view({'get': 'retrieve'})
        response = view(request, document_number='12345678')
        response.render()
        self.assertEquals(response.status_code, 200)

    def test_headers_csv(self):
        headers_csv = ['name', 'father_name', 'mother_name', 'document_number',
               'is_active', 'career_id', 'availability_id', 'graduation_year',
               'email', 'phone', 'linkedin', 'skype',
               'is_email_preferred', 'is_phone_preferred',
               'is_linkedin_preferred', 'is_skype_preferred', 'period', 'topics']
        valid, msg = headers_in_list(headers_csv)
        self.assertTrue(valid)
        self.assertEqual(msg, 'ok')
        headers_csv_01 = copy(headers_csv)
        headers_csv_01.remove('name')
        valid01, msg01 = headers_in_list(headers_csv_01)
        self.assertFalse(valid01)
        self.assertEquals(msg01, 'Falta header {\'name\'}.')
        headers_csv_02 = copy(headers_csv)
        headers_csv_02.append('field_extra')
        valid02, msg02 = headers_in_list(headers_csv_02)
        self.assertFalse(valid02)
        self.assertEquals(msg02, ' Header {\'field_extra\'} desconocido.')
        headers_csv_03 = copy(headers_csv)
        headers_csv_03.remove('name')
        headers_csv_03.append('field_extra')
        valid03, msg03 = headers_in_list(headers_csv_03)
        self.assertFalse(valid03)
        self.assertEquals(msg03, 'Falta header {\'name\'}. Header {\'field_extra\'} desconocido.')
