# -*- coding:utf-8 -*-

import unittest
from unittest.mock import Mock
from django.conf import settings
from upc_mentormatch.utils import *
from upc_mentormatch import default_app_config
from upc_mentormatch.apps import UpcMentormatchConfig

class MentorMatchAppTestCase(unittest.TestCase):

    def test_mentormatch_app_should_be_installed(self):
        self.assertIn('upc_mentormatch', settings.INSTALLED_APPS)
    
    def test_mentormatch_app_should_have_custom_app_config(self):
        self.assertEqual(default_app_config, 'upc_mentormatch.apps.UpcMentormatchConfig')

    def test_mentormatch_app_should_have_custom_verbose_name(self):
        self.assertGreater(len(UpcMentormatchConfig.verbose_name), 0)