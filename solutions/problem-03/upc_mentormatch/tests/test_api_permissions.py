import unittest
from unittest import mock
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import BaseAuthentication
from rest_framework.test import APIRequestFactory
from upc_commons.models import IdentityDocumentType
from upc_mentormatch.api.permissions import IsAuthenticatedM3Applicant


class MentorMatchAPIPermissionsTestCase(unittest.TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()

    def get_mock_non_mentormatch_applicant(self):
        applicant = mock.MagicMock()
        applicant.__str__.return_value = 'upc.applicant@maildrop.cc'
        applicant.status = self.get_mock_non_mentormatch_applicant_status()
        applicant.first_name = 'Carlos Alberto'
        applicant.paternal_last_name = 'Torres'
        applicant.maternal_last_name = 'Caballero'
        applicant.email = 'upc.applicant@maildrop.cc'
        applicant.idt = IdentityDocumentType(name='DNI')
        applicant.idn = '23455432'
        return applicant

    def get_mock_non_mentormatch_applicant_status(self):
        status = mock.Mock()
        status.pk = 20000
        status.description = 'Some required applicant status'
        return status

    def get_mock_non_m3_user(self):
        user = mock.MagicMock()
        user.is_authenticated.return_value = True
        user.is_superuser.return_value = False
        user.username = 'UserTest'
        user.email = 'lucuma@lucumalabs.com'
        user.as_upc_applicant = self.get_mock_non_mentormatch_applicant()
        return user

    def get_mock_required_applicant_status(self):
        status = mock.Mock()
        status.pk = 10000
        status.description = 'Some required applicant status'
        return status

    @mock.patch('upc_mentormatch.validators.EntityStatus')
    @mock.patch('upc_mentormatch.validators.mentormatch_settings')
    def test_m3_api_authenticator_has_permission_fails(self, s, es):

        # Mocking authentication by a Momento 3 user
        user = self.get_mock_non_m3_user()

        # Mocking a view
        class MockView(APIView):

            def get(self, request):
                return Response({"status": "ok"})

        # Mocking Momento 3 user authentication 
        class MockM3Authentication(BaseAuthentication):

            def authenticate(self, request):
                return (user, None)


        # Mocking a view
        view = MockView.as_view(
            authentication_classes=[MockM3Authentication],
            permission_classes=[IsAuthenticatedM3Applicant]
        )

        # Mocking M3 required applicant status
        s.MENTORMATCH_APPLICANT_STATUS_PK = 10000
        es.objects.get.return_value = self.get_mock_required_applicant_status()

        # Mocking request
        request = self.factory.get('/')

        # Getting response
        response = view(request)
        response.render()
        expected_error_message = ''.join([
            'Applicant upc.applicant@maildrop.cc has a status value of ',
            '"Some required applicant status" (20000) instead of ',
            '"Some required applicant status" (10000) which is required ',
            'as per the value of the MENTORMATCH_APPLICANT_STATUS_PK ',
            'settings variable'
        ])
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(response.data['error'], expected_error_message)