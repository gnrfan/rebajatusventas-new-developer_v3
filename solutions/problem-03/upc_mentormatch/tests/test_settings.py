# -*- coding:utf-8 -*-

import unittest
from unittest import mock
from django.core.exceptions import ValidationError
from upc_mentormatch import settings

class MentorMatchSettingsTestCase(unittest.TestCase):

    def test_settings_should_have_default_values(self):
        key = 'MENTORMATCH_DEFAULTS'
        self.assertTrue(hasattr(settings,key))

    def test_category_topic_image_folder_should_have_default_value(self):
        key = 'CATEGORY_TOPIC_IMAGE_FOLDER'
        self.assertIn(key, settings.MENTORMATCH_DEFAULTS)

    def test_category_topic_image_folder_should_have_non_empty_default_value(self):
        key = 'CATEGORY_TOPIC_IMAGE_FOLDER'
        self.assertGreater(len(settings.MENTORMATCH_DEFAULTS[key]), 0)

    def test_category_topic_image_folder_should_be_set(self):
        key = 'MENTORMATCH_CATEGORY_TOPIC_IMAGE_FOLDER'
        self.assertTrue(hasattr(settings, key))

    def test_category_topic_image_folder_should_not_have_empty_value(self):
        key = 'MENTORMATCH_CATEGORY_TOPIC_IMAGE_FOLDER'
        value = getattr(settings, key, '')
        self.assertGreater(len(value), 0)

    def test_applicant_status_pk_should_be_present_in_the_settings(self):
        key = 'MENTORMATCH_APPLICANT_STATUS_PK'
        self.assertTrue(hasattr(settings, key))