# -*- coding:utf-8 -*-

import unittest
from unittest import mock
from django.db import models
from django.utils.translation import ugettext_lazy as _
from upc_core.models import Entity, User
from upc_commons.models import IdentityDocumentType
from upc_applicants.models import Applicant
from upc_mentormatch.models import *
from upc_mentormatch.utils import category_or_topic_uuid_upload_to


class MentorMatchModelsTestCase(unittest.TestCase):

    # CategoryTopic

    def test_category_topic_model_class_should_inherit_from_a_model(self):
        self.assertTrue(issubclass(CategoryTopic, models.Model))

    def test_category_topic_model_class_should_have_a_name_field(self):
        field = CategoryTopic._meta.get_field('name')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 128)
        self.assertGreater(len(field.verbose_name), 0)
    
    def test_category_topic_model_class_should_have_a_record_type_field(self):
        field = CategoryTopic._meta.get_field('record_type')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 8)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.choices, CATEGORYTOPIC_TYPE_CHOICES)
        self.assertGreater(len(CATEGORYTOPIC_TYPE_CHOICES), 0)

    def test_category_topic_model_class_should_have_a_parent_field(self):
        field = CategoryTopic._meta.get_field('parent')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertEqual(field.null, True)
        self.assertEqual(field.blank, True)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.rel.related_name, 'children')
    
    def test_category_topic_model_class_should_have_an_image_field(self):
        field = CategoryTopic._meta.get_field('image')
        self.assertIsInstance(field, models.ImageField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.upload_to, category_or_topic_uuid_upload_to)
        self.assertEqual(field.null, True)
        self.assertEqual(field.blank, True)
 
    def test_category_topic_model_class_should_have_an_is_active_field(self):
        field = CategoryTopic._meta.get_field('is_active')
        self.assertIsInstance(field, models.BooleanField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.default, True)

    def test_category_topic_model_class_should_have_a_created_at_field(self):
        field = CategoryTopic._meta.get_field('created_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.auto_now_add, True)

    def test_category_topic_model_class_should_have_a_updated_at_field(self):
        field = CategoryTopic._meta.get_field('updated_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.auto_now, True)

    def test_category_topic_model_class_should_have_a_verbose_name(self):
        self.assertGreater(len(CategoryTopic._meta.verbose_name), 0)

    def test_category_topic_model_class_should_have_a_verbose_name_for_plurals(self):
        self.assertGreater(len(CategoryTopic._meta.verbose_name_plural), 0)

    def test_category_topic_model_class_should_have_a_custom__str__method(self):
        name = 'Some name'
        instance = CategoryTopic(name=name)
        self.assertEqual(name, str(instance))

    def test_category_topic_model_class_should_have_a_custom__repr__method(self):
        category = CategoryTopic(name='Category', record_type='category')
        topic = CategoryTopic(name='Topic', record_type='topic', parent=category)
        # For categories
        expected = '<CategoryTopic "Category" record_type="category" parent=None>'
        self.assertEqual(category.__repr__(), expected)
        # For topics
        expected = '<CategoryTopic "Topic" record_type="topic" parent="Category">'
        self.assertEqual(topic.__repr__(), expected)

 
    @mock.patch('upc_mentormatch.models.validate_category_has_no_parent')
    @mock.patch('upc_mentormatch.models.validate_topic_parent_is_category')
    @mock.patch('upc_mentormatch.models.validate_parent_is_another_record')
    def test_category_topic_model_class_should_have_custom_validations(self, v1, v2, v3):
        some_category = CategoryTopic(
            name='Some category',
            record_type='category'
        )
        some_topic = CategoryTopic(
            name='Some topic',
            record_type='topic',
            parent=some_category
        )
        instance = some_topic
        instance.clean()
        v1.assert_called_once_with(instance)
        v2.assert_called_once_with(instance)
        v3.assert_called_once_with(instance)

    # MentorAvailability

    def test_mentor_availability_field_code(self):
        field = MentorAvailability._meta.get_field('code')
        self.assertIsInstance(field, models.PositiveSmallIntegerField)
        self.assertEqual(field.unique, True)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_availability_field_description(self):
        field = MentorAvailability._meta.get_field('description')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_availability_model_class_should_have_a_custom__str__method(self):
        code = '1'
        description = 'Some description'
        instance = MentorAvailability(code=code, description=description)
        self.assertEqual(description, str(instance))

    def get_mock_mentor_availability(self):
        code = 1
        description = 'Una vez por semana'
        instance = MentorAvailability(code=code,
                                      description=description)
        return instance

    def test_mentor_availability_model_class_should_have_a_custom__repr__method(self):
        instance = self.get_mock_mentor_availability()
        expected_string = ''.join([
            '<MentorAvailability "Una vez por semana" ',
            'code="1">'
        ])
        self.assertEqual(expected_string, instance.__repr__())

    # Mentor

    def test_mentor_field_career(self):
        field = Mentor._meta.get_field('career')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_cmp(self):
        field = Mentor._meta.get_field('cmp')
        self.assertIsInstance(field, models.TextField)
        self.assertEqual(field.blank, True)
        self.assertEqual(field.null, True)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_email(self):
        field = Mentor._meta.get_field('email')
        self.assertIsInstance(field, models.EmailField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_father_name(self):
        field = Mentor._meta.get_field('father_name')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_is_email_preferred(self):
        field = Mentor._meta.get_field('is_email_preferred')
        self.assertIsInstance(field, models.BooleanField)
        self.assertEqual(field.default, False)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_is_linkedin_preferred(self):
        field = Mentor._meta.get_field('is_linkedin_preferred')
        self.assertIsInstance(field, models.BooleanField)
        self.assertEqual(field.default, False)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_is_phone_preferred(self):
        field = Mentor._meta.get_field('is_phone_preferred')
        self.assertIsInstance(field, models.BooleanField)
        self.assertEqual(field.default, False)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_favorite_skype(self):
        field = Mentor._meta.get_field('is_skype_preferred')
        self.assertIsInstance(field, models.BooleanField)
        self.assertEqual(field.default, False)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_is_active(self):
        field = Mentor._meta.get_field('is_active')
        self.assertIsInstance(field, models.BooleanField)
        self.assertEqual(field.default, True)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_linkedin(self):
        field = Mentor._meta.get_field('linkedin')
        self.assertIsInstance(field, models.URLField)
        self.assertEqual(field.blank, True)
        self.assertEqual(field.null, True)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_availability(self):
        field = Mentor._meta.get_field('availability')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_mother_name(self):
        field = Mentor._meta.get_field('mother_name')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_name(self):
        field = Mentor._meta.get_field('name')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_number_document(self):
        field = Mentor._meta.get_field('document_number')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 20)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_period(self):
        field = Mentor._meta.get_field('period')
        self.assertIsInstance(field, models.ManyToManyField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_phone(self):
        field = Mentor._meta.get_field('phone')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 12)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_photo(self):
        field = Mentor._meta.get_field('photo')
        self.assertIsInstance(field, models.ImageField)
        self.assertEqual(field.upload_to, img_uuid_upload_to)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_skype(self):
        field = Mentor._meta.get_field('skype')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_themes_of_interest(self):
        field = Mentor._meta.get_field('topics_of_interest')
        self.assertIsInstance(field, models.ManyToManyField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_field_year_of_graduation(self):
        field = Mentor._meta.get_field('graduation_year')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_model_class_should_have_a_custom__str__method(self):
        name = 'Some Name'
        father_name = 'Some Father Name'
        mother_name = 'Some Mother Name'
        instance = Mentor(
            name=name,
            father_name=father_name,
            mother_name=mother_name
        )
        expected_string = u'{} {} {}'.format(name, father_name, mother_name)
        self.assertEqual(expected_string, str(instance))

    def get_mock_mentor(self):
        name = 'Sr.'
        father_name = 'Father'
        mother_name = 'Mother'
        document_number = '12345678'
        instance = Mentor(name=name,
                          father_name=father_name,
                          mother_name=mother_name,
                          document_number=document_number)
        return instance

    def test_mentor_model_class_should_have_a_custom__repr__method(self):
        instance = self.get_mock_mentor()
        expected_string = ''.join([
            '<Mentor "Sr. Father Mother" ',
            'document_number="12345678">'
        ])
        self.assertEqual(expected_string, instance.__repr__())

    # WorkExperience

    def test_work_experience_field_current_position(self):
        field = WorkExperience._meta.get_field('current_position')
        self.assertIsInstance(field, models.BooleanField)
        self.assertEqual(field.default, False)
        self.assertGreater(len(field.verbose_name), 0)

    def test_work_experience_end_date(self):
        field = WorkExperience._meta.get_field('ending_date')
        self.assertIsInstance(field, models.DateField)
        self.assertEqual(field.blank, True)
        self.assertEqual(field.null, True)
        self.assertGreater(len(field.verbose_name), 0)

    def test_work_experience_field_enterprise(self):
        field = WorkExperience._meta.get_field('company')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_work_experience_field_init_date(self):
        field = WorkExperience._meta.get_field('starting_date')
        self.assertIsInstance(field, models.DateField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_work_experience_field_mentor(self):
        field = WorkExperience._meta.get_field('mentor')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertGreater(len(field.verbose_name), 0)

    def test_work_experience_field_position(self):
        field = WorkExperience._meta.get_field('position')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 150)
        self.assertGreater(len(field.verbose_name), 0)

    def test_work_experience_model_class_should_have_a_custom__str__method(self):
        company = 'Some Company'
        position = 'Some Position'
        instance = WorkExperience(company=company, position=position)
        self.assertEqual(u'{} en {}'.format(position, company), str(instance))

    def get_mock_work_experience(self):
        company = 'Enterprise'
        position = 'Position'
        instance = WorkExperience(company=company,
                                  position=position)
        return instance

    def test_work_experience_model_class_should_have_a_custom__repr__method(self):
        instance = self.get_mock_work_experience()
        expected_string = ''.join([
            '<WorkExperience pk="{}" '.format(instance.pk),
            'position="Position" at "Enterprise">'
        ])
        self.assertEqual(expected_string, instance.__repr__())

    # Mentee

    def test_mentee_model_class_should_inherit_from_a_model(self):
        self.assertTrue(issubclass(Mentee, models.Model))

    def test_mentee_model_class_should_have_an_applicant_field(self):
        field = Mentee._meta.get_field('applicant')
        self.assertIsInstance(field, models.OneToOneField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentee_model_class_should_have_a_created_at_field(self):
        field = Mentee._meta.get_field('created_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.auto_now_add, True)

    def test_mentee_model_class_should_have_a_updated_at_field(self):
        field = Mentee._meta.get_field('updated_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.auto_now, True)

    def test_mentee_model_class_should_have_a_custom__str__method(self):
        instance = self.get_mentee_instance()
        expected_string = 'Carlos Alberto Torres Caballero'
        self.assertEqual(expected_string, str(instance))

    def test_mentee_model_class_should_have_a_custom__repr__method(self):
        instance = self.get_mentee_instance()
        expected_string = ''.join([
            '<Mentee "Carlos Alberto Torres Caballero" ',
            'email="upc.applicant@maildrop.cc" ',
            'document_type="DNI" ',
            'document_number="23455432">'
        ])
        self.assertEqual(expected_string, instance.__repr__())

    @mock.patch('upc_mentormatch.models.validate_applicant_status_for_mentormatch')
    def test_mentee_model_class_should_have_custom_validations(self, v):
        instance = self.get_mentee_instance()
        instance.clean()
        v.assert_called_once_with(instance.applicant)

    def get_mentee_instance(self):
        applicant = Applicant()
        applicant.entity = Entity()
        applicant.user = User()
        applicant.first_name = 'Carlos Alberto'
        applicant.paternal_last_name = 'Torres'
        applicant.maternal_last_name = 'Caballero'
        applicant.email = 'upc.applicant@maildrop.cc'
        applicant.idt = IdentityDocumentType(name='DNI')
        applicant.idn = '23455432'
        instance = Mentee(applicant=applicant)
        return instance

    # Mentorship

    def test_mentorship_model_class_should_inherit_from_a_model(self):
        self.assertTrue(issubclass(Mentorship, models.Model))

    def test_mentorship_model_class_should_have_a_mentor_field(self):
        field = Mentorship._meta.get_field('mentor')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertEqual(field.rel.related_name, 'mentorships_as_mentor')
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentorship_model_class_should_have_a_mentee_field(self):
        field = Mentorship._meta.get_field('mentee')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertEqual(field.rel.related_name, 'mentorships_as_mentee')
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentorship_model_class_should_have_a_period_field(self):
        field = Mentorship._meta.get_field('period')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertEqual(field.rel.related_name, 'mentorships_in_period')
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentorship_model_class_should_have_a_cmp_field(self):
        field = Mentorship._meta.get_field('cmp')
        self.assertIsInstance(field, models.TextField)
        self.assertEqual(field.blank, True)
        self.assertEqual(field.null, True)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentorship_model_class_should_have_a_guid_hd_field(self):
        field = Mentorship._meta.get_field('guid_hd')
        self.assertIsInstance(field, models.CharField)
        self.assertEqual(field.max_length, 255)
        self.assertEqual(field.blank, True)
        self.assertEqual(field.null, True)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentorship_model_class_should_have_a_created_at_field(self):
        field = Mentorship._meta.get_field('created_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.auto_now_add, True)

    def test_mentorship_model_class_should_have_a_updated_at_field(self):
        field = Mentorship._meta.get_field('updated_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertEqual(field.auto_now, True)

    def test_mentorship_model_class_should_have_a_custom__str__method(self):
        instance = self.get_mentorship_instance()
        expected_string = ''.join([
            'Mentoría #None Mentor: "Marcos Maderos Morales (77778888)" ',
            'Mentee: "Carlos Alberto Torres Caballero (DNI 23455432)" ',
            'Periodo: "Some-Period-2018-01 (Some period 2018-01)"'
        ])
        self.assertEqual(expected_string, str(instance))

    def test_mentorship_model_class_should_have_a_custom__repr__method(self):
        instance = self.get_mentorship_instance()
        expected_string = ''.join([
            '<Mentorship id="None" mentor="Marcos Maderos Morales (77778888)" ',
            'mentee="Carlos Alberto Torres Caballero (DNI 23455432)" ',
            'period="Some-Period-2018-01 (Some period 2018-01)">'
        ])
        self.assertEqual(expected_string, instance.__repr__())

    @mock.patch('upc_mentormatch.models.validate_max_number_of_mentee_mentorships_per_period')
    def test_mentorship_model_class_should_have_custom_validations(self, v):
        instance = self.get_mentorship_instance()
        instance.clean()
        v.assert_called_once_with(instance)

    def get_mentor_instance(self):
        mentor = Mentor()
        mentor.name = 'Marcos'
        mentor.father_name = 'Maderos'
        mentor.mother_name = 'Morales'
        mentor.document_number = '77778888'
        return mentor

    def get_period_instance(self):
        period = Period()
        period.code = 'Some-Period-2018-01'
        period.description = 'Some period 2018-01'
        return period

    def get_mentorship_instance(self):
        mentorship = Mentorship()
        mentorship.mentor = self.get_mentor_instance()
        mentorship.mentee = self.get_mentee_instance()
        mentorship.period = self.get_period_instance()
        return mentorship

    # MentorUpdate

    def test_mentor_update_field_mentor(self):
        field = MentorUpdate._meta.get_field('mentor')
        self.assertIsInstance(field, models.ForeignKey)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_update_field_code(self):
        field = MentorUpdate._meta.get_field('code')
        self.assertIsInstance(field, models.UUIDField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_update_field_payload(self):
        field = MentorUpdate._meta.get_field('payload')
        self.assertIsInstance(field, JSONField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_update_field_creation_ip_address(self):
        field = MentorUpdate._meta.get_field('creation_ip_address')
        self.assertIsInstance(field, models.GenericIPAddressField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_update_field_created_at(self):
        field = MentorUpdate._meta.get_field('created_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_update_field_review_ip_address(self):
        field = MentorUpdate._meta.get_field('review_ip_address')
        self.assertIsInstance(field, models.GenericIPAddressField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_update_field_reviewed_at(self):
        field = MentorUpdate._meta.get_field('reviewed_at')
        self.assertIsInstance(field, models.DateTimeField)
        self.assertGreater(len(field.verbose_name), 0)

    def test_mentor_update_field_state(self):
        field = MentorUpdate._meta.get_field('state')
        self.assertIsInstance(field, models.CharField)
        self.assertGreater(len(field.verbose_name), 0)
        self.assertTupleEqual(field.choices,
                              (('created', _('creado')),
                               ('accepted', _('aceptado')),
                               ('rejected', _('rechazado')),)
                              )
