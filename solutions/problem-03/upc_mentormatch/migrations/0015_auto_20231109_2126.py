# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2023-11-10 02:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('upc_mentormatch', '0014_auto_20190813_1322'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mentor',
            options={'ordering': ['father_name', 'mother_name', 'name'], 'permissions': (('create_mentor_for_csv', 'Cargar desde CSV'),), 'verbose_name': 'mentor', 'verbose_name_plural': 'mentores'},
        ),
        migrations.AlterField(
            model_name='mentor',
            name='periods',
            field=models.ManyToManyField(related_name='mentors', to='upc_applicants.Period', verbose_name='Periodos'),
        ),
    ]
