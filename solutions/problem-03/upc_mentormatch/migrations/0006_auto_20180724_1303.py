# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-07-24 18:03
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('upc_applicants', '0060_merge'),
        ('upc_mentormatch', '0005_auto_20180724_1223'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mentorship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cmp', models.TextField(blank=True, null=True, verbose_name='cmp')),
                ('guid_hd', models.CharField(blank=True, max_length=255, null=True, verbose_name='guid hd (CRM)')),
                ('utm_source', models.CharField(blank=True, max_length=255, null=True, verbose_name='UTM Fuente')),
                ('utm_medium', models.CharField(blank=True, max_length=255, null=True, verbose_name='UTM medio')),
                ('utm_campaign', models.CharField(blank=True, max_length=255, null=True, verbose_name='UTM campaña')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='fecha y hora de creación')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='fecha y hora de actualización')),
                ('mentee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='upc_mentormatch.Mentee', verbose_name='Mentee')),
            ],
            options={
                'verbose_name': 'mentorship',
                'verbose_name_plural': 'mentorships',
            },
        ),
        migrations.AlterModelOptions(
            name='workexperience',
            options={'verbose_name': 'Experiencia laboral', 'verbose_name_plural': 'Experiencias laborales'},
        ),
        migrations.RenameField(
            model_name='workexperience',
            old_name='enterprise',
            new_name='company',
        ),
        migrations.RemoveField(
            model_name='mentor',
            name='is_favorite_email',
        ),
        migrations.RemoveField(
            model_name='mentor',
            name='is_favorite_linkedin',
        ),
        migrations.RemoveField(
            model_name='mentor',
            name='is_favorite_phone',
        ),
        migrations.RemoveField(
            model_name='mentor',
            name='is_favorite_skype',
        ),
        migrations.RemoveField(
            model_name='workexperience',
            name='current_work',
        ),
        migrations.RemoveField(
            model_name='workexperience',
            name='end_date',
        ),
        migrations.RemoveField(
            model_name='workexperience',
            name='init_date',
        ),
        migrations.AddField(
            model_name='mentor',
            name='is_email_preferred',
            field=models.BooleanField(default=False, verbose_name='¿Prefieres la comunicación por correo?'),
        ),
        migrations.AddField(
            model_name='mentor',
            name='is_linkedin_preferred',
            field=models.BooleanField(default=False, verbose_name='¿Prefieres la comunicación por LinkedIn?'),
        ),
        migrations.AddField(
            model_name='mentor',
            name='is_phone_preferred',
            field=models.BooleanField(default=False, verbose_name='¿Prefieres la comunicación por teléfono?'),
        ),
        migrations.AddField(
            model_name='mentor',
            name='is_skype_preferred',
            field=models.BooleanField(default=False, verbose_name='¿Prefieres la comunicación por Skype?'),
        ),
        migrations.AddField(
            model_name='workexperience',
            name='current_position',
            field=models.BooleanField(default=False, verbose_name='¿Actualmente trabaja aqui?'),
        ),
        migrations.AddField(
            model_name='workexperience',
            name='ending_date',
            field=models.DateField(blank=True, null=True, verbose_name='Fecha de fin'),
        ),
        migrations.AddField(
            model_name='workexperience',
            name='starting_date',
            field=models.DateField(default=datetime.date(2018, 7, 24), verbose_name='Fecha de inicial'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='mentor',
            name='linkedin',
            field=models.URLField(blank=True, null=True, verbose_name='URL de cuenta en Linkedin'),
        ),
        migrations.AlterField(
            model_name='mentor',
            name='mentor_availability',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='upc_mentormatch.MentorAvailability', verbose_name='Disponibilidad'),
        ),
        migrations.AlterField(
            model_name='mentor',
            name='topics_of_interest',
            field=models.ManyToManyField(to='upc_mentormatch.CategoryTopic', verbose_name='Temas de interés'),
        ),
        migrations.AlterField(
            model_name='workexperience',
            name='position',
            field=models.CharField(max_length=150, verbose_name='Cargo en la empresa'),
        ),
        migrations.AddField(
            model_name='mentorship',
            name='mentor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='upc_mentormatch.Mentor', verbose_name='Mentor'),
        ),
        migrations.AddField(
            model_name='mentorship',
            name='period',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='upc_applicants.Period', verbose_name='Periodo'),
        ),
    ]
