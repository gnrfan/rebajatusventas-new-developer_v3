# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-10-29 22:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('upc_mentormatch', '0008_auto_20180801_1448'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mentor',
            options={'ordering': ['name'], 'permissions': ('Create Mentor for Upload CSV', 'Upload CSV'), 'verbose_name': 'Mentor', 'verbose_name_plural': 'Mentores'},
        ),
        migrations.AlterModelOptions(
            name='mentorship',
            options={'permissions': ('Descargar csv', 'Descargar reporte de la mentorias'), 'verbose_name': 'mentoría', 'verbose_name_plural': 'mentorías'},
        )
    ]
