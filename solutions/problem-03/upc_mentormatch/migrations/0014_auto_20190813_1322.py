# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-08-13 18:22
from __future__ import unicode_literals

from django.db import migrations, models
import upc_mentormatch.utils.uploads


class Migration(migrations.Migration):

    dependencies = [
        ('upc_mentormatch', '0013_auto_20190405_1710'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mentor',
            options={'ordering': ['father_name', 'mother_name', 'name'], 'permissions': (('create_mentor_for_csv', 'Upload CSV'),), 'verbose_name': 'Mentor', 'verbose_name_plural': 'Mentores'},
        ),
        migrations.AlterField(
            model_name='mentor',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='mentor',
            name='phone',
            field=models.CharField(blank=True, max_length=12, null=True, verbose_name='Celular'),
        ),
        migrations.AlterField(
            model_name='mentor',
            name='photo',
            field=models.ImageField(blank=True, null=True, upload_to=upc_mentormatch.utils.uploads.img_uuid_upload_to, verbose_name='Foto'),
        ),
        migrations.AlterField(
            model_name='mentor',
            name='topics_of_interest',
            field=models.ManyToManyField(blank=True, related_name='interested_mentors', to='upc_mentormatch.CategoryTopic', verbose_name='Temas de interés'),
        ),
    ]
