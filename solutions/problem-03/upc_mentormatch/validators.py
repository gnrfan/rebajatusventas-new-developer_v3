from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from upc_core.models import EntityStatus
from upc_applicants.models import Period
from . import settings as mentormatch_settings
from .utils import get_applicants_for_mentormatch

def validate_parent_is_another_record(value):
    if value.parent and value.parent == value:
         raise ValidationError(
            _('The parent cannot be the same record')
        )    

def validate_topic_parent_is_category(value):
    condition = value.record_type == 'topic'
    condition = condition and value.parent is not None
    condition = condition and value.parent.record_type != 'category'
    if condition:
        raise ValidationError(
            _('The parent of a topic must be category')
        )

def validate_category_has_no_parent(value):
    condition = value.record_type == 'category'
    condition = condition and value.parent is not None
    if condition:
        raise ValidationError(
            _('Categories can not have parent records')
        )

class ApplicantStatusValidator(object):

    CONFIG_VARIABLE = 'MENTORMATCH_APPLICANT_STATUS_PK'

    def __call__(self, value):
        self.set_applicant(value)
        self.set_required_status_pk_from_settings()
        self.set_required_status_instance()
        if not self.check_matching_status_condition():
            error_msg = self.generate_mismatch_error_message()
            raise ValidationError(error_msg)

    def set_applicant(self, value):
        self.applicant = value

    def set_required_status_pk_from_settings(self):
        try:
            pk = getattr(mentormatch_settings, self.CONFIG_VARIABLE)
            self.required_status_pk = pk
        except AttributeError:
            error_msg = self.generate_misconfiguration_error_message()
            raise ValidationError(error_msg)

    def set_required_status_instance(self):
        try:
            obj = EntityStatus.objects.get(pk=self.required_status_pk)
            self.required_status = obj
        except EntityStatus.DoesNotExist:
            error_msg = self.generate_missing_status_error_message()
            raise ValidationError(error_msg)

    def generate_misconfiguration_error_message(self):
        error_msg = ''.join([
            "Applicant \"%(applicant)s\"'s status can not be ",
            "validated because the %(config_var)s settings variable "
            "is missing from the project's configuration"
        ])
        params = {
            'applicant': self.applicant,
            'config_var': self.CONFIG_VARIABLE
        }
        return _(error_msg) % params

    def generate_missing_status_error_message(self):
        error_msg = ''.join([
            "Applicant \"%(applicant)s\"'s status can not be ",
            "validated because an EntityStatus entry with ",
            "pk of %(required_status_pk)s as configured in the ",
            "%(config_var)s settings variable ",
            "is missing from the database"
        ])
        params = {
            'applicant': self.applicant,
            'required_status_pk': self.required_status_pk,
            'config_var': self.CONFIG_VARIABLE
        }
        return _(error_msg) % params

    def generate_mismatch_error_message(self):
        error_msg = ''.join([
            "Applicant %(applicant)s has a status value of ",
            "\"%(status_description)s\" (%(status_pk)s) instead of ",
            "\"%(required_status_description)s\" (%(required_status_pk)s) ",
            "which is required as per the value of the ",
            "%(config_var)s settings variable"
        ])
        params = {
            'applicant': self.applicant,
            'status_description': self.applicant.status.description,
            'status_pk': self.applicant.status.pk,
            'required_status_description': self.required_status.description,
            'required_status_pk': self.required_status.pk,
            'config_var': self.CONFIG_VARIABLE
        }
        return _(error_msg) % params

    def check_matching_status_condition(self):
        return self.applicant.status.pk == self.required_status.pk

validate_applicant_status_for_mentormatch = ApplicantStatusValidator()


class MaxMenteeMentorshipsPerPeriodValidator(object):

    CONFIG_VARIABLE = 'MENTORMATCH_MENTEE_MAX_NUMBER_OF_MENTORSHIPS_PER_PERIOD'

    def __call__(self, value):
        self.set_mentorship(value)
        self.set_required_maximum_from_settings()
        if not self.check_validation_condition():
            error_msg = self.generate_max_reached_error_message()
            raise ValidationError(error_msg)

    def set_mentorship(self, value):
        self.mentorship = value

    def set_required_maximum_from_settings(self):
        try:
            m = getattr(mentormatch_settings, self.CONFIG_VARIABLE)
            self.max_mentorships_per_period = m
        except AttributeError:
            error_msg = self.generate_misconfiguration_error_message()
            raise ValidationError(error_msg)

    def generate_misconfiguration_error_message(self):
        error_msg = ''.join([
            "The system can not validate if the "
            "The max number of mentorships per period ",
            "has been reached for mentee \"%(mentee)s\" ",
            "because the %(config_var)s settings variable ",
            "is missing from the project's configuration"
        ])
        params = {
            'mentee': self.mentorship.mentee,
            'config_var': self.CONFIG_VARIABLE
        }
        return _(error_msg) % params

    def generate_max_reached_error_message(self):
        error_msg = ''.join([
            "The mentee \"%(mentee)s\" has reached ",
            "the maximum number of %(max)s ",
            "allowed mentorships per period"
        ])
        params = {
            'mentee': self.mentorship.mentee,
            'max': self.max_mentorships_per_period
        }
        return _(error_msg) % params

    def get_queryset(self):
        return self.mentorship.mentee.mentorships_as_mentee.filter(
            period=self.mentorship.period
        )

    def check_validation_condition(self):
        qs = self.get_queryset()
        return qs.count() < self.max_mentorships_per_period


validate_max_number_of_mentee_mentorships_per_period = \
    MaxMenteeMentorshipsPerPeriodValidator()


def validate_document_number_belongs_to_a_m3_applicant(document_number):
    momento3_applicants = get_applicants_for_mentormatch()
    if momento3_applicants.filter(idn=document_number).count() == 0:
        raise ValidationError(msg.format(document_number=document_number))
        msg = ''.join([
            'Identity document number \'{document_number}\' '
            'does not belong to any of the registered Momento 3',
            'applicants'
        ])
        raise ValidationError(msg.format(document_number=document_number))


def validate_mentor_is_active_in_current_period(document_number):
    from .models import Mentor
    try:
        mentor = Mentor.objects.get(document_number=document_number)
        if not mentor.is_active:
            msg = ''.join([
                'Mentor \'{mentor}\' ',
                'identified by document number \'{document_number}\' '
                'is not active'
            ])
            raise ValidationError(
                msg.format(
                    mentor=mentor,
                    document_number=document_number
                )
            )
        period = Period.objects.get_current_period_at()
        if period not in mentor.periods.all():
            msg = ''.join([
                'Mentor \'{mentor}\' ',
                'identified by document number \'{document_number}\' '
                'is not active for current period \'{period}\''
            ])
            raise ValidationError(
                msg.format(
                    mentor=mentor,
                    document_number=document_number,
                    period=period
                )
            )
    except Mentor.DoesNotExist:
        msg = ''.join([
            'Identity document number \'{document_number}\' '
            'does not belong to any of the registered mentors'
        ])
        raise ValidationError(msg.format(document_number=document_number))