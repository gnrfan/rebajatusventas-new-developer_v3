import re
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from upc_applicants.models import Applicant
from .utils import get_applicants_for_mentormatch
from .models import *


class CategoryTopicAdminForm(forms.ModelForm):

    parent = forms.ModelChoiceField(
        label=_('Categoría padre'),
        queryset=CategoryTopic.objects.categories(is_active='both'),
        empty_label="(Ninguna)",
        required=False
    )

    class Meta:
        model = CategoryTopic
        exclude = ['id', 'created_at', 'updated_at']


class MenteeAdminForm(forms.ModelForm):

    applicant = forms.ModelChoiceField(
        label=_('Alumno (Momento 3)'),
        # queryset=Applicant.objects.filter(status__pk=12),
        queryset=get_applicants_for_mentormatch(),
        empty_label="-- Escoja un alumno --"
    )

    class Meta:
        model = CategoryTopic
        exclude = ['id', 'created_at', 'updated_at']


class MentorCreateCSV(forms.Form):
    file = forms.FileField()


class MentorLoginForm(forms.Form):

    idn = forms.CharField(
        label='DNI',
        max_length=8,
        strip=True
    )

    def clean_idn(self):
        idn = self.cleaned_data['idn']
        if not bool(re.match(r'^[0-9]{8}$', idn)):
            raise ValidationError("El valor no es un DNI válido")
        try:
            mentor = Mentor.objects.get(document_number=idn)
            if not mentor.is_active:
                raise ValidationError(
                    "El mentor existe, pero no se encuentra activo.")
            self.mentor = mentor
        except Mentor.DoesNotExist:
            raise ValidationError("No existe un mentor con el DNI indicado.")
        return idn

    def load_mentor(self):

        if hasattr(self, 'mentor'):
            return self.mentor

        idn = self.cleaned_data['idn']
        try:
            return Mentor.objects.get(
                document_number=idn,
                is_active=True
            )
        except Mentor.DoesNotExist:
            return None
