from django.core.exceptions import ValidationError
from upc_applicants.api.permissions import IsAuthenticatedApplicant
from upc_mentormatch.validators import (
    validate_applicant_status_for_mentormatch
)

class IsAuthenticatedM3Applicant(IsAuthenticatedApplicant):

    def has_permission(self, request, view):
        ok = super().has_permission(request, view)
        # User might have no permission at this point
        if not ok:
            return False
        # Let's validate the applicant for "Momento 3"
        applicant = request.user.as_upc_applicant
        try:
            validate_applicant_status_for_mentormatch(applicant)
            return True
        except ValidationError as the_exception:
            self.message = the_exception.message
            return False