from collections import OrderedDict
from django.utils.translation import (
    ugettext as _,
    ugettext_noop as _noop
)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from upc_applicants.models import Period
from upc_commons.api.serializers import Career as CareerSerializer

from upc_mentormatch.models import (
    CategoryTopic,
    MentorAvailability,
    Mentor,
    WorkExperience,

    Mentorship
)
from ..validators import (
    validate_document_number_belongs_to_a_m3_applicant,
    validate_mentor_is_active_in_current_period
)


class MentorAvailabilitySerializer(serializers.ModelSerializer):

    class Meta:
        model = MentorAvailability
        fields = ('code', 'description')


class CareerAvailabilitySerializer(serializers.Serializer):
    availability = serializers.IntegerField()

    class Meta:
        fields = ('availability',)


class WorkExperienceSerializer(serializers.ModelSerializer):
    is_current_job = serializers.BooleanField(source='current_position')

    class Meta:
        model = WorkExperience
        fields = ('company',
                  'position',
                  'starting_date',
                  'ending_date',
                  'is_current_job')

    def validate(self, data):
        from dateutil.parser import parse
        if data['ending_date']:
            if data['starting_date'] > data['ending_date']:
                msg = dict()
                txt = (
                    "Fecha fin: \"{}\" debe de ser menor a"
                    "fecha de inicio: \"{}\""
                ).format(
                    data['ending_date'],
                    data['starting_date']
                )
                msg.update({'msg': txt})
                raise serializers.ValidationError({'ending_date': msg})

        return data


class TopicSerializer(serializers.ModelSerializer):

    code = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()
    parent_category_url = serializers.SerializerMethodField()

    class Meta:
        model = CategoryTopic
        fields = (
            'code',
            'name',
            'url',
            'image_url',
            'parent_category_url',
            'is_active'
        )

    def get_code(self, obj):
        return obj.pk

    def get_url(self, obj):
        return obj.get_endpoint_url()

    def get_image_url(self, obj):
        return obj.get_image_url()

    def get_parent_category_url(self, obj):
        if not obj.parent:
            return
        return obj.parent.get_endpoint_url()


class CategorySerializer(serializers.ModelSerializer):

    code = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()
    topics = serializers.SerializerMethodField()

    class Meta:
        model = CategoryTopic
        fields = (
            'code',
            'name',
            'url',
            'image_url',
            'is_active',
            'topics'
        )

    def get_code(self, obj):
        return obj.pk

    def get_url(self, obj):
        return obj.get_endpoint_url()

    def get_image_url(self, obj):
        return obj.get_image_url()

    def get_topics(self, obj):
        result = []
        is_active = self.context.get('is_active', None)
        params = {}
        if is_active is not None:
            params['is_active'] = is_active
        qs = obj.children.filter(**params)
        for topic in qs:
            serializer = TopicSerializer(topic)
            result.append(serializer.data)
        return result


class MentorContactDataSerializer(serializers.Serializer):
    value = serializers.CharField(allow_blank=True, allow_null=True)
    preferred = serializers.NullBooleanField(required=False)

    class Meta:
        fields = ('value', 'preferred')

    def validate_preferred(self, data):
        if data in [False, None]:
            return False
        else:
            return data


class MentorContactSerializer(serializers.Serializer):
    phone = MentorContactDataSerializer()
    email = MentorContactDataSerializer()
    skype = MentorContactDataSerializer()
    linkedin = MentorContactDataSerializer()

    class Meta:
        fields = ('phone', 'email', 'skype', 'linkedin')

    def validate(self, data):
        count_preferred = 0
        for d in data.values():
            if d['preferred'] == True:
                count_preferred += 1
        if count_preferred == 0:
            msg = dict()
            msg.update(
                {'preferred': 'Debe seleccionar un medio de contacto preferido'})
            raise ValidationError(msg)
        return data

    def validate_phone(self, data):
        msg = dict()
        try:
            int(data['value'])
        except Exception:
            txt = "Numero Telefono '{}' no es Numerico".format(data['value'])
            msg.update({'value': txt})
            raise ValidationError(msg)
        else:
            return data


class PeriodSerializer(serializers.ModelSerializer):

    class Meta:
        model = Period
        fields = ('code', )


class AvailabilitySerializer(serializers.ModelSerializer):

    class Meta:
        model = MentorAvailability
        fields = ('code',)

    def validate(self, data):
        msg = dict()
        try:
            MentorAvailability.objects.get(code=data['code'])
        except MentorAvailability.DoesNotExist:
            txt = "MentorAvailability code={} no encontrado".format(
                data['code'])
            msg.update({'code': txt})
            raise ValidationError(msg)
        return data


class MentorTopicSerializer(serializers.ModelSerializer):

    class Meta:
        model = CategoryTopic
        fields = ('id',)


class MentorSerializer(serializers.ModelSerializer):
    career = CareerSerializer()
    url = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()
    periods = PeriodSerializer(many=True)
    availability = serializers.StringRelatedField()
    contact_information = MentorContactSerializer(
        source='get_contact_information')
    professional_experiences = WorkExperienceSerializer(
        source='work_experiences.all',
        many=True
    )
    topics = serializers.SerializerMethodField()

    class Meta:
        model = Mentor
        fields = (
                  'id',
                  'document_number',
                  'name',
                  'father_name',
                  'mother_name',
                  'career',
                  'graduation_year',
                  'periods',
                  'url',
                  'image_url',
                  'availability',
                  'contact_information',
                  'professional_experiences',
                  'topics')

    def get_url(self, obj):
        return obj.get_endpoint_url()

    def get_image_url(self, obj):
        return obj.get_image_url()

    def get_topics(self, obj):
        result = []
        is_active = self.context.get('is_active', None)
        params = {}
        if is_active is not None:
            params['is_active'] = is_active
        qs = obj.topics_of_interest.filter(**params)
        for topic in qs:
            serializer = TopicSerializer(topic)
            result.append(serializer.data)
        return result


class PhotoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mentor
        fields = ('photo',)


class MentorSaveSerializer(serializers.ModelSerializer):

    availability = serializers.PrimaryKeyRelatedField(
        queryset=MentorAvailability.objects.all()
    )
    contact_information = MentorContactSerializer(
        source='get_contact_information'
    )
    professional_experiences = WorkExperienceSerializer(
        source='work_experiences.all',
        many=True
    )
    topics_of_interest = serializers.PrimaryKeyRelatedField(
        queryset=CategoryTopic.objects.all(),
        many=True
    )

    class Meta:
        model = Mentor
        fields = ('availability',
                  'contact_information',
                  'professional_experiences',
                  'topics_of_interest',
                  'father_name',
                  'mother_name',
                  'name',
                  'graduation_year',
                  'career'
                  )

    def update(self, instance, data):
        instance.availability = data.get('availability', instance.availability)
        instance.father_name = data.get('father_name', instance.availability)
        instance.mother_name = data.get('mother_name', instance.availability)
        instance.name = data.get('name', instance.availability)
        instance.graduation_year = data.get(
            'graduation_year', instance.availability)
        instance.career = data.get('career', instance.availability)

        instance.photo = data.get('photo', instance.photo)
        instance.phone = (
            data['get_contact_information']['phone']['value'])
        instance.email = (
            data['get_contact_information']['email']['value'])
        instance.skype = (
            data['get_contact_information']['skype']['value'])
        instance.linkedin = (
            data['get_contact_information']['linkedin']['value'])

        instance.is_email_preferred = (
            data['get_contact_information']['email']['preferred'])
        instance.is_phone_preferred = (
            data['get_contact_information']['phone']['preferred'])
        instance.is_skype_preferred = (
            data['get_contact_information']['skype']['preferred'])
        instance.is_linkedin_preferred = (
            data['get_contact_information']['linkedin']['preferred'])
        topics = data['topics_of_interest']
        instance.topics_of_interest.clear()
        instance.save()
        for topic in topics:
            if not topic.parent:
                continue
            instance.topics_of_interest.add(topic)

        work_experiences = data.get('work_experiences', list())
        if work_experiences:
            WorkExperience.objects.filter(mentor=instance).delete()
            for data in work_experiences.get('all', list()):
                we = WorkExperience()
                we.company = data['company']
                we.position = data['position']
                we.starting_date = data['starting_date']
                we.ending_date = data['ending_date']
                we.current_position = data['current_position']
                we.mentor = instance
                we.save()
        return instance


class MentorShipSaveSerializer(serializers.ModelSerializer):

    mentor = serializers.CharField(source='mentor.document_number')

    class Meta:
        model = Mentorship
        fields = ('mentor', 'mentee', 'period', 'topic')

    def validate_mentor(self, data):
        mentor = Mentor.objects.get(document_number=data)

        if mentor.is_active:
            return mentor
        else:
            msg = dict()
            txt = _noop("Mentor '{mentor}' is not active").format(
                mento=data
            )
            msg.update({'msg': txt})
            raise ValidationError(msg)

    def validate_period(self, data):
        try:
            period = Period.objects.get(code=data.code)
        except Period.DoesNotExist:
            msg = dict()
            txt = _noop("Period '{code}' is unknown").format(
                code=data
            )
            msg.update({'msg': txt})
            raise ValidationError(msg)

    def validate(self, data):
        data["period"] = data["mentee"].applicant.period
        data["mentor"] = data['mentor']['document_number']
        if not data["mentor"].periods.filter(code=data["period"].code).exists():
            msg = dict()
            message = "Mentor '{mentor}' is not available for period '{period}'"
            txt = _noop(message).format(
                period=data["period"],
                mentor=data["mentor"]
            )
            msg.update({'msg': txt})
            raise ValidationError(msg)
        return data

    def create(self, validated_data):
        return super(MentorShipSaveSerializer, self).create(validated_data)


class MentorshipSerializer(serializers.ModelSerializer):

    mentee = serializers.CharField(
        max_length=255,
        validators=[validate_document_number_belongs_to_a_m3_applicant]
    )

    mentor = serializers.CharField(
        max_length=20,
        validators=[validate_mentor_is_active_in_current_period]
    )

    class Meta:
        model = Mentorship
        fields = ('mentee', 'mentor')

    def validate(self, data):
        from ..models import Mentee
        from ..models import Mentor
        data['mentee'] = Mentee.objects.get_or_create_with_applicant_idn(
            data['mentee']
        )
        data['mentor'] = Mentor.objects.get(
            document_number=data['mentor']
        )
        return data
