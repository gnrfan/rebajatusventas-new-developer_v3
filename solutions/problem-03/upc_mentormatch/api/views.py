from datetime import datetime

from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_noop as _
from rest_framework.exceptions import (
    ValidationError as APIValidationError
)
from rest_framework import status, viewsets, views
from rest_framework.parsers import FileUploadParser, FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

from upc_applicants.models import Period
from upc_commons.api.exceptions import (
    Conflict,
    UnsupportedQueryParameter,
    MultipleUnsupportedQueryParameters,
    QueryParametersNotSupported,
    ResourceNotImplemented,
)
from upc_commons.api.utils import (
    check_supported_parameters,
    process_is_active_param,
    get_params_query_for_request,
    process_is_active_for_param
)
from upc_commons.models import Career
from upc_mentormatch.utils import deobfuscate, get_file_extension
from upc_mentormatch.utils.helpers import get_career_availability
from upc_mentormatch.models import (
    CategoryTopic,
    Mentor,
    MentorAvailability,
    Mentorship,
    Mentee)
from rest_framework.exceptions import ValidationError
from .exceptions import MissingCategoryConflict
from .serializers import (
    CategorySerializer,
    TopicSerializer,
    MentorAvailabilitySerializer,
    CareerAvailabilitySerializer,
    MentorSerializer,
    MentorSaveSerializer,
    PhotoSerializer,
    MentorShipSaveSerializer)


class MentorAvailabilityView(ListAPIView):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queryset = MentorAvailability.objects.all()
        self.serializer_class = MentorAvailabilitySerializer


class CareerAvailabilityView(RetrieveAPIView):

    def get(self, request, *args, **kwargs):
        q = {'availability':
             get_career_availability(request.GET.get('career_code'))
             }
        serializer = CareerAvailabilitySerializer(q)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CareerViewSet(viewsets.ViewSet):

    def list(self, request, *args, **kwargs):
        print(kwargs)
        queryset = CategoryTopic.objects.filter(interested_mentors__career__code=kwargs['career_code'])\
            .order_by('-name').distinct()
        serializer = TopicSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MentorRequestAccessView(views.APIView):
    def post(self, request, *args, **kwargs):
        if 'idn' not in request.data:
            return Response(
                {'detail': 'Ingresa tu número de DNI.'},
                status=status.HTTP_400
            )

        try:
            self.object = Mentor.objects.get(
                document_number__iexact=request.data['idn']
            )
        except Mentor.DoesNotExist:
            return Response(
                {'detail': 'Número de DNI desconocido.'},
                status=status.HTTP_409_CONFLICT
            )
        self.object.send_login_email(host=request.get_host())
        return Response(
            {
                'detail': (
                    'Te hemos enviado un correo electrónico a %s.'
                ) % self.object.email
            },
            status=status.HTTP_200_OK
        )


class MentorAuthView(views.APIView):
    def post(self, request, *args, **kwargs):
        if 'token' not in request.data:
            return Response(
                {'detail': 'A token is required.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            self.object = deobfuscate(request.data['token'])
        except Mentor.DoesNotExist:
            raise Response(
                {'detail': 'Invalid token'},
                status=status.HTTP_401_UNAUTHORIZED
            )

        if not isinstance(self.object, Mentor):
            raise Response(
                {'detail': 'Invalid token'},
                status=status.HTTP_401_UNAUTHORIZED
            )

        serializer = MentorSerializer(instance=self.object)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MentorViewSet(viewsets.ViewSet):
    # parser_classes = (MultiPartParser, FormParser,)

    def list(self, request, *args, **kwargs):
        context = dict()
        check_supported_parameters(request,
                                   ['is_active_mentor', 'is_active_topic',
                                    'identity', 'topic', 'career', 'period'],
                                   'GET'
                                   )
        params_for_query = [('is_active_mentor', 'is_active'),
                            ('identity', 'document_number'),
                            ('topic', 'topics_of_interest__in'),
                            ('career', 'career__code'),
                            ('period', 'periods__code__in')
                            ]
        career_list = list()
        if request.GET.get('career', False):
            name_career = request.GET.get('career')
            from upc_core.models import String
            careers = String.objects.filter(es__icontains=name_career)
            for career in careers:
                c = Career.objects.filter(name=career.id, status=1,
                                          modality__name='Pregrado')
                if c.exists():
                    career_list.append(c.first())
        filter_params = get_params_query_for_request(request, params_for_query)
        if career_list:
            if filter_params.get('career__code', False):
                filter_params['career__code'] = career_list[0].code

        params = process_is_active_for_param(
            request=request,
            params='is_active_topic'
        )
        context['is_active'] = params
        if filter_params:
            queryset = Mentor.objects.filter(**filter_params)
        else:
            queryset = Mentor.objects.all()
        serializer = MentorSerializer(queryset, many=True, context=context)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        context = {}
        check_supported_parameters(request,
                                   ['identity_document_number'],
                                   'GET'
                                   )
        queryset = get_object_or_404(
            Mentor,
            document_number=kwargs.get('identity_document_number')
        )
        serializer = MentorSaveSerializer(queryset, context=context)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        instance = Mentor.objects.get(pk=kwargs.get('pk'))
        fields = ['availability',
                  'contact_information',
                  'professional_experiences',
                  'topics_of_interest'
                  ]
        from schema import Schema
        dict_data = {
            'availability': int,
            'contact_information': {'phone': {'value': str, 'preferred': bool},
                                    'email': {'value': str, 'preferred': bool},
                                    'skype': {'value': str, 'preferred': bool},
                                    'linkedin': {'value': str, 'preferred': bool}},
            'professional_experiences': [{'company': str,
                                          'position': str,
                                          'starting_date': str,
                                          'ending_date': str,
                                          'is_current_job': bool}],
            'topics_of_interest': list}

        schema = Schema(dict_data)
        if not schema.is_valid(request.data):
            required_fields = list()
            for field in fields:
                if not field in request.data.keys():
                    required_fields.append(field)
                if field is 'contact_information':
                    data_contact_keys = dict_data['contact_information'].keys()
                    for dk in data_contact_keys:
                        if not dk in request.data['contact_information'].keys():
                            required_fields.append(dk)
                        else:
                            for vk in request.data['contact_information'][dk].keys():
                                if not vk in ['value', 'preferred']:
                                    required_fields.append(
                                        "{}: {}".format(dk, vk))
                if field is 'professional_experiences':
                    data_professional_keys = dict_data['professional_experiences'][0].keys(
                    )
                    for pk in data_professional_keys:
                        if not pk in request.data['professional_experiences'][0].keys():
                            required_fields.append(pk)

            if required_fields:
                msg = dict()
                txt = "Campos {} es requerido".format(required_fields)
                msg.update({'msg': txt})
                return Response(msg, status=status.HTTP_400_BAD_REQUEST)

        serializer = MentorSaveSerializer(instance,
                                          data=request.data,
                                          partial=True)
        serializer.is_valid(raise_exception=False)

        if serializer.errors:
            return Response(
                serializer.errors,
                status.HTTP_400_BAD_REQUEST
            )
        else:
            serializer.save()
            return Response(serializer.data)


class MentorPeriodViewSet(viewsets.ViewSet):
    # parser_classes = (MultiPartParser, FormParser,)

    def list(self, request, *args, **kwargs):
        date_now = datetime.now()
        extra_filter = {}
        if request.GET.get('modality', False):
            extra_filter['modality'] = request.GET['modality']

        period = Period.objects.filter(active=True, start_date__lte=date_now.date(), end_date__gte=date_now.date(),
                                       **extra_filter)
        queryset = Mentor.objects.filter(periods__in=period)
        serializer = MentorSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PhotoViewSet(viewsets.ViewSet):
    queryset = Mentor.objects.all()
    serializer_class = PhotoSerializer

    def post(self, request, *args, **kwargs):
        try:
            mentor = Mentor.objects.get(id=kwargs.get('pk'))
        except Mentor.DoesNotExist:
            return Response('', status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(data=request.data, instance=mentor)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            {'url': mentor.get_image_url()},
            status=status.HTTP_200_OK
        )
        '''
        photo = request.FILES.get('photo', None)
        if not photo:
            return Response(
                {'detail': 'a photo is required.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        if isinstance(photo, list):
            photo = photo[0]

        extension_list = ['jpeg', 'jpg', 'png']
        extension = get_file_extension(photo.name).replace('.', '')
        if extension.lower() in extension_list:
            try:
                mentor = Mentor.objects.get(id=kwargs.get('pk'))
            except Mentor.DoesNotExist:
                return Response('', status=status.HTTP_404_NOT_FOUND)
            mentor.photo = photo
            mentor.save()
            return Response(
                {'detail': 'Foto guardada'},
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                {'detail': 'Extension de archivo no permitido, extensiones validas: jpeg, jpg, png'},
                status=status.HTTP_400_BAD_REQUEST
            )
        '''


class MentorShipView(CreateAPIView):
    queryset = Mentorship.objects.all()
    serializer_class = MentorShipSaveSerializer

    def post(self, request, *args, **kwargs):
        from upc_applicants.models import Applicant
        applicant = Applicant.objects.get(idn=request.data['mentee'])
        m, created = Mentee.objects.get_or_create(
            applicant=applicant,
        )
        request.data['mentee'] = m.id
        request.data['period'] = applicant.period.id
        serializer = MentorShipSaveSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as e:
            try:
                message = e.detail
            except AttributeError:
                message = str(e)
            document = {'error': message}
            return Response(document, status=status.HTTP_409_CONFLICT)
        else:
            try:
                obj = serializer.save()
            except Exception as e:
                try:
                    message = e.detail
                except AttributeError:
                    message = str(e)
                document = {'error': message}
                return Response(document, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response({'id': obj.id})


class CategoryViewSet(viewsets.ViewSet):

    # Action methods

    def list(self, request):
        params = self.process_list_get_parameters(request)
        queryset = CategoryTopic.objects.categories(**params)
        context = {}
        context['is_active'] = params.get('is_active', None)
        serializer = CategorySerializer(
            queryset,
            context=context,
            many=True
        )
        return Response(serializer.data)

    def retrieve(self, request, category_pk=None):
        check_supported_parameters(request, [], 'GET', debug=True)
        queryset = CategoryTopic.objects.categories(is_active='both')
        category = get_object_or_404(queryset, pk=category_pk)
        serializer = CategorySerializer(category)
        return Response(serializer.data)

    # Non action (helper) methods

    def process_category_param(self, request):
        category_pk = request.GET.get('category', None)
        if category_pk is not None:
            try:
                category_pk = int(category_pk)
            except ValueError:
                msg = _('The category parameter must be a positive integer')
                raise APIValidationError(msg)
        return category_pk

    def process_list_get_parameters(self, request):
        check_supported_parameters(
            request,
            supported=['category', 'is_active'],
            source='GET'
        )
        params = {}
        is_active = process_is_active_param(request)
        if is_active is not None:
            params['is_active'] = is_active
        category_pk = self.process_category_param(request)
        if category_pk is not None:
            self.check_category_exists(category_pk)
            params['pk'] = category_pk
            params['is_active'] = 'both'
        return params

    def check_category_exists(self, category_pk):
        """
        This method raises a 409 CONFLICT error when the
        category is not found.
        """
        try:
            obj = CategoryTopic.objects.get(pk=category_pk)
        except CategoryTopic.DoesNotExist:
            raise MissingCategoryConflict(
                category_code=category_pk
            )


class TopicViewSet(viewsets.ViewSet):

    # Action methods

    def list(self, request, category_pk):
        msg = ''.join([
            "Not implemented. ",
            "See category collection or ",
            "category detail for topics"
        ])
        raise ResourceNotImplemented(msg)

    def retrieve(self, request, category_pk, topic_pk):
        check_supported_parameters(request, [], 'GET')
        queryset = CategoryTopic.objects.topics(is_active='both')
        topic = get_object_or_404(
            queryset,
            pk=topic_pk,
            parent__pk=category_pk
        )
        serializer = TopicSerializer(topic)
        return Response(serializer.data)
