import json
from collections import OrderedDict

from django.conf import  settings
from ..settings import MENTORMATCH_API_PREFIX


class MentormatchJSONAPIMiddleware(object):

    def process_response(self, request, response):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        if request.path.startswith(MENTORMATCH_API_PREFIX) and\
                response['Content-Type'] != 'application/json':
            doc = OrderedDict()
            if response.status_code == 401:
                doc['msg'] = 'This endpoint requires user authentication'
            elif response.status_code == 404:
                doc['msg'] = 'The requested resource does not exist'
            elif response.status_code == 500:
                doc['msg'] = 'Internal server error. If the problem continues contact the administrators'
            response.content = json.dumps(doc, indent=4)
            response['Content-Type'] = 'application/json'
        # Code to be executed for each request/response after
        # the view is called.

        return response