from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _
from upc_commons.api.exceptions import Conflict

class MissingCategoryConflict(Conflict):
    default_detail = _('Category with code \'{code}\' does not exist.')

    def __init__(self, category_code, detail=None, code=None):
        if detail is None:
            detail = force_text(self.default_detail).format(code=category_code)
        super(MissingCategoryConflict, self).__init__(detail, code)