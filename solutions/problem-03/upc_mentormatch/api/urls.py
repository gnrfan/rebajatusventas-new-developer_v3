from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

from . import views

router = SimpleRouter()
# router.register(r'mentors', views.MentorViewSet, 'mentor_view')

urlpatterns = (
    url(r'^', include(router.urls)),
    url(r'^availability/?$',
        views.MentorAvailabilityView.as_view(),
        name='availability_collection'
        ),
    url(r'^query-career$',
        views.CareerAvailabilityView.as_view(),
        name='career-availability'
        ),
    url(r'^categories/(?P<category_pk>\d+)/topics/?$',
        views.TopicViewSet.as_view({'get': 'list'}),
        name='topic_collection'
        ),
    url(r'^topic_of_careers/(?P<career_code>[-\w]+)/$',
        views.CareerViewSet.as_view({'get': 'list'}),
        name='career_of_topics'
        ),
    url(r'^categories/(?P<category_pk>\d+)/topics/(?P<topic_pk>\d+)/?$',
        views.TopicViewSet.as_view({'get': 'retrieve'}),
        name='topic_detail'
        ),
    url(r'^categories/?$',
        views.CategoryViewSet.as_view({'get': 'list'}),
        name='category_collection'
        ),
    url(r'^categories/(?P<category_pk>\d+)/?$',
        views.CategoryViewSet.as_view({'get': 'retrieve'}),
        name='category_detail'
        ),
    url(r'^mentors/$',
        views.MentorViewSet.as_view({'get': 'list'}),
        name='mentor'
        ),
    url(r'^mentors/period/?$',
        views.MentorPeriodViewSet.as_view({'get': 'list'}),
        name='mentor'
        ),
    url(r'^mentor/request-access/$',
        views.MentorRequestAccessView.as_view(),
        name='mentor_request_access'
        ),
    url(r'^mentor/auth/$',
        views.MentorAuthView.as_view(),
        name='mentor_auth'
        ),
    url(r'^mentor/(?P<identity_document_number>\d+)/$',
        views.MentorViewSet.as_view({'get': 'retrieve'}),
        name='mentor_detail'
        ),
    url(r'^mentor/update/(?P<pk>\d+)/$',
        views.MentorViewSet.as_view({'put': 'partial_update'}),
        name='mentor_update'
        ),
    url(r'^mentor/photo/(?P<pk>\d+)/$',
        views.PhotoViewSet.as_view({'post': 'post'}),
        name='mentor_photo'
        ),
    url(r'^mentor_ship/$',
        views.MentorShipView.as_view(),
        name='mentorship'
        ),
)
