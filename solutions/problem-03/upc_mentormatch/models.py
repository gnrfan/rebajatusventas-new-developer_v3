from collections import OrderedDict

from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField

from upc.utils import send_mail_template_file
from upc_applicants.models import (
    Applicant,
    Period
)
from upc_commons.models import Career
from .utils import (
    category_or_topic_uuid_upload_to,
    img_uuid_upload_to,
    get_absolute_url,
    reverse_absolute_url,
    get_applicants_for_mentormatch,
    obfuscate
)
from .validators import (
    validate_parent_is_another_record,
    validate_topic_parent_is_category,
    validate_category_has_no_parent,
    validate_applicant_status_for_mentormatch,
    validate_max_number_of_mentee_mentorships_per_period
)

# Create your models here.

CATEGORYTOPIC_TYPE_CHOICES = (
    ('category', _('categoría')),
    ('topic', _('tema'))
)

MENTOR_UPDATE_STATE_CHOICES = (
    ('created', _('creado')),
    ('accepted', _('aceptado')),
    ('rejected', _('rechazado')),
)


class CategoryTopicManager(models.Manager):

    def categories(self, pk=None, is_active=True):
        params = {}
        params['record_type'] = 'category'
        if str(is_active).lower() == 'both':
            is_active = None
        if is_active is not None:
            params['is_active'] = is_active
        if pk is not None:
            params['pk'] = pk
        return self.filter(**params)

    def topics(self, pk=None, is_active=True):
        params = {}
        params['record_type'] = 'topic'
        if str(is_active).lower() == 'both':
            is_active = None
        if is_active is not None:
            params['is_active'] = is_active
        if pk is not None:
            params['pk'] = pk
        return self.filter(**params)


class CategoryTopic(models.Model):

    name = models.CharField(
        verbose_name=_('nombre'),
        max_length=128
    )

    record_type = models.CharField(
        verbose_name=_('tipo de registro'),
        choices=CATEGORYTOPIC_TYPE_CHOICES,
        max_length=8
    )

    parent = models.ForeignKey('self',
        verbose_name=_('registro padre'),
        null=True,
        blank=True,
        related_name='children'
    )

    image = models.ImageField(
        verbose_name=_('imágen'),
        upload_to=category_or_topic_uuid_upload_to,
        null=True,
        blank=True
    )

    is_active = models.BooleanField(
        verbose_name=_('¿está activo?'),
        default=True
    )

    created_at = models.DateTimeField(
        verbose_name=_('fecha y hora de creación'),
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        verbose_name=_('fecha y hora de actualización'),
        auto_now=True
    )

    objects = CategoryTopicManager()

    class Meta:
        verbose_name = _('categoría ó tema')
        verbose_name_plural = _('categorias ó temas')

    def __str__(self):
        return self.name

    def __repr__(self):
        format_str = "<{class_name} \"{name}\" record_type=\"{record_type}\" parent={parent}>"
        kwargs = {
            'class_name': self.__class__.__name__,
            'name': self.name,
            'record_type': self.record_type,
            'parent': ('"%s"' % self.parent) if self.parent else 'None'
        }
        return format_str.format(**kwargs)

    def clean(self):
        validate_parent_is_another_record(self)
        validate_topic_parent_is_category(self)
        validate_category_has_no_parent(self)

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    def get_image_url(self, **kwargs):
        if self.image:
            return get_absolute_url(self.image.url, **kwargs)

    def get_endpoint_url(self, **kwargs):
        if self.record_type == 'category':
            return reverse_absolute_url(
                viewname='api:mentor_match:category_detail',
                kwargs={
                    'category_pk': self.pk
                },
                **kwargs
            )
        elif self.record_type == 'topic':
            return reverse_absolute_url(
                viewname='api:mentor_match:topic_detail',
                kwargs={
                    'category_pk': self.parent.pk,
                    'topic_pk': self.pk
                },
                **kwargs
            )


class MenteeManager(models.Manager):

    def get_or_create_with_applicant_idn(self, idn):
        qs = get_applicants_for_mentormatch()
        applicant = qs.get(idn=idn)
        try:
            mentee = Mentee.objects.get(applicant=applicant)
        except Mentee.DoesNotExist:
            mentee = Mentee(applicant=applicant)
            mentee.save()
        return mentee


class Mentee(models.Model):

    applicant = models.OneToOneField(Applicant,
        verbose_name='postulante'
    )

    created_at = models.DateTimeField(
        verbose_name=_('fecha y hora de creación'),
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        verbose_name=_('fecha y hora de actualización'),
        auto_now=True
    )

    objects = MenteeManager()

    class Meta:
        verbose_name = _('mentee')
        verbose_name_plural = _('mentees')

    def __str__(self):
        return self.applicant.full_name

    def __repr__(self):
        format_str = ''.join([
            "<{class_name} \"{full_name}\" email=\"{email}\" ",
            "document_type=\"{document_type}\" ",
            "document_number=\"{document_number}\">"
        ])
        kwargs = {
            'class_name': self.__class__.__name__,
            'full_name': self.applicant.full_name,
            'email': self.applicant.email,
            'document_type': self.applicant.idt.name,
            'document_number': self.applicant.idn
        }
        return format_str.format(**kwargs)

    def clean(self):
        validate_applicant_status_for_mentormatch(self.applicant)

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)


class MentorAvailability(models.Model):

    code = models.PositiveSmallIntegerField(
        verbose_name=_('código'),
        unique=True
    )

    description = models.CharField(
        verbose_name=_('descripción'),
        max_length=150
    )

    class Meta:
        verbose_name = _('Disponibilidad Mentor')
        verbose_name_plural = _('Disponibilidades Mentor')

    def __str__(self):
        return self.description

    def __repr__(self):
        format_str = "<{class_name} \"{description}\" code=\"{code}\">"
        kwargs = {
            'class_name': self.__class__.__name__,
            'description': self.description,
            'code': self.code
        }
        return format_str.format(**kwargs)


class Mentor(models.Model):

    father_name = models.CharField(
        verbose_name=_('Apellido Paterno'),
        max_length=150
    )

    mother_name = models.CharField(
        verbose_name=_('Apellido Materno'),
        max_length=150
    )

    name = models.CharField(
        verbose_name=_('Nombre'),
        max_length=150
    )

    document_number = models.CharField(
        verbose_name=_('Número de Documento'),
        max_length=20
    )

    email = models.EmailField(
        verbose_name=_('email'),
        null=True,
        blank=True
    )

    phone = models.CharField(
        verbose_name=_('Celular'),
        max_length=12,
        null=True,
        blank=True
    )

    photo = models.ImageField(
        verbose_name=_('Foto'),
        upload_to=img_uuid_upload_to,
        null=True,
        blank=True
    )

    cmp = models.TextField(
        verbose_name=_('cmp'),
        blank=True,
        null=True
    )

    graduation_year = models.CharField(
        verbose_name=_('Año de graduación'),
        max_length=150
    )

    is_email_preferred = models.BooleanField(
        verbose_name=_('¿Prefieres la comunicación por correo?'),
        default=False
    )

    is_linkedin_preferred = models.BooleanField(
        verbose_name=_('¿Prefieres la comunicación por LinkedIn?'),
        default=False
    )

    is_phone_preferred = models.BooleanField(
        verbose_name=_('¿Prefieres la comunicación por teléfono?'),
        default=False
    )

    is_skype_preferred = models.BooleanField(
        verbose_name=_('¿Prefieres la comunicación por Skype?'),
        default=False
    )

    is_active = models.BooleanField(
        verbose_name=_('activo'),
        default=True
    )

    linkedin = models.URLField(
        verbose_name=_('URL de cuenta en Linkedin'),
        blank=True,
        null=True
    )

    skype = models.CharField(
        verbose_name=_('Skype'),
        blank=True,
        max_length=150,
        null=True
    )

    career = models.ForeignKey(
        Career,
        verbose_name=_('Carrera')
    )

    availability = models.ForeignKey(
        MentorAvailability,
        verbose_name=_('Disponibilidad')
    )

    periods = models.ManyToManyField(
        Period,
        verbose_name=_('Periodos'),
        related_name='mentors'
    )

    topics_of_interest = models.ManyToManyField(
        CategoryTopic,
        blank=True,
        verbose_name=_('Temas de interés'),
        related_name='interested_mentors'
    )

    class Meta:
        verbose_name = _('mentor')
        verbose_name_plural = _('mentores')
        ordering = ['father_name', 'mother_name', 'name']
        permissions = (
            ("create_mentor_for_csv", _("Cargar desde CSV")),
        )

    def __str__(self):
        return self.full_name

    def __repr__(self):
        format_str = "<{class_name} \"{full_name}\" document_number=\"{document_number}\">"
        kwargs = {
            'class_name': self.__class__.__name__,
            'full_name': self.full_name,
            'document_number': self.document_number
        }
        return format_str.format(**kwargs)

    @property
    def full_name(self):
        format_str = "{name} {father_name} {mother_name}"
        kwargs = {
            'name': self.name,
            'father_name': self.father_name,
            'mother_name': self.mother_name
        }
        return format_str.format(**kwargs)

    def get_access_token(self):
        return obfuscate(self)

    @property
    def get_contact_information(obj):
        data = OrderedDict({
            "phone": {
                "value": obj.phone,
                "preferred": obj.is_phone_preferred
            },
            "email": {
                "value": obj.email,
                "preferred": obj.is_email_preferred
            },
            "skype": {
                "value": obj.skype,
                "preferred": obj.is_skype_preferred
            },
            "linkedin": {
                "value": obj.linkedin,
                "preferred": obj.is_linkedin_preferred
            }
        }, )
        return data

    def get_image_url(self, **kwargs):
        if self.photo:
            return get_absolute_url(self.photo.url, **kwargs)

    def get_endpoint_url(self, **kwargs):
        return reverse_absolute_url(
            viewname='api:mentor_match:mentor_detail',
            kwargs={
                'identity_document_number': self.document_number
            },
            **kwargs
        )

    def send_login_email(self, host=None):
        return send_mail_template_file(
            template_name='mentors/login_email.html',
            context={'object': self, 'host': host},
            subject=_("Ingresa a MentorMatch."),
            recipient_list=[self.email]
        )


class WorkExperience(models.Model):

    mentor = models.ForeignKey(
        Mentor,
        verbose_name=_('Mentor'),
        related_name='work_experiences'
    )

    current_position = models.BooleanField(
        verbose_name=_('¿Actualmente trabaja aqui?'),
        default=False
    )

    starting_date = models.DateField(
        verbose_name=_('Fecha de inicio')
    )

    ending_date = models.DateField(
        verbose_name=_('Fecha de fin'),
        blank=True,
        null=True
    )

    company = models.CharField(
        verbose_name=_('Empresa'),
        max_length=150
    )

    position = models.CharField(
        verbose_name=_('Cargo en la empresa'),
        max_length=150
    )

    class Meta:
        verbose_name = _('Experiencia laboral')
        verbose_name_plural = _('Experiencias laborales')

    def __str__(self):
        return u'{} en {}'.format(self.position, self.company)

    def __repr__(self):
        format_str = ''.join([
            "<{class_name} pk=\"{pk}\" ",
            "position=\"{position}\" at \"{company}\">"
        ])
        kwargs = {
            'class_name': self.__class__.__name__,
            'pk': self.pk,
            'company': self.company,
            'position': self.position
        }
        return format_str.format(**kwargs)


class Mentorship(models.Model):

    mentor = models.ForeignKey(Mentor,
                               verbose_name=_('Mentor'),
                               related_name='mentorships_as_mentor'
                               )

    mentee = models.ForeignKey(Mentee,
                               verbose_name=_('Mentee'),
                               related_name='mentorships_as_mentee'
                               )

    period = models.ForeignKey(Period,
                               verbose_name=_('Periodo'),
                               related_name='mentorships_in_period'
                               )

    topic = models.ForeignKey(CategoryTopic,
                              verbose_name='Topic'
                              )

    cmp = models.TextField(
        verbose_name=_('cmp'),
        blank=True,
        null=True
    )

    guid_hd = models.CharField(
        verbose_name=_("guid hd (CRM)"),
        max_length=255,
        null=True,
        blank=True
    )

    utm_source = models.CharField(
        verbose_name=_("UTM Fuente"),
        max_length=255,
        null=True,
        blank=True
    )

    utm_medium = models.CharField(
        verbose_name=_("UTM medio"),
        max_length=255,
        null=True,
        blank=True
    )

    utm_campaign = models.CharField(
        verbose_name=_("UTM campaña"),
        max_length=255,
        null=True,
        blank=True
    )

    created_at = models.DateTimeField(
        verbose_name=_('fecha y hora de creación'),
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        verbose_name=_('fecha y hora de actualización'),
        auto_now=True
    )
    send_email_mentor = models.BooleanField(
        verbose_name=_('Email enviado a mentor'),
        default=False
    )
    send_email_mentee = models.BooleanField(
        verbose_name=_('Email enviado a mentee'),
        default=False
    )

    class Meta:
        verbose_name = _('mentoría')
        verbose_name_plural = _('mentorías')
        permissions = (
            ("download_csv", "Descargar reporte de la mentorias"),
        )

    def __str__(self):
        format_str = ''.join([
            "Mentoría #{pk} ",
            "Mentor: \"{mentor}\" ",
            "Mentee: \"{mentee}\" ",
            "Periodo: \"{period}\""
        ])
        kwargs = {
            'pk': self.pk,
            'mentor': self.get_formatted_mentor(),
            'mentee': self.get_formatted_mentee(),
            'period': self.period
        }
        return format_str.format(**kwargs)

    def __repr__(self):
        format_str = ''.join([
            "<{class_name} id=\"{pk}\" ",
            "mentor=\"{mentor}\" ",
            "mentee=\"{mentee}\" ",
            "period=\"{period}\">"
        ])
        kwargs = {
            'class_name': self.__class__.__name__,
            'pk': self.pk,
            'mentor': self.get_formatted_mentor(),
            'mentee': self.get_formatted_mentee(),
            'period': self.period
        }
        return format_str.format(**kwargs)

    def get_formatted_mentor(self):
        format_str = "{full_name} ({document_number})"
        kwargs = {
            'full_name': self.mentor.full_name,
            'document_number': self.mentor.document_number,
        }
        return format_str.format(**kwargs)

    def get_formatted_mentee(self):
        format_str = "{full_name} ({idt} {idn})"
        kwargs = {
            'full_name': self.mentee.applicant.full_name,
            'idt': self.mentee.applicant.idt.name,
            'idn': self.mentee.applicant.idn
        }
        return format_str.format(**kwargs)

    def clean(self, update_fields=[]):
        try:
            p = self.period
        except Period.DoesNotExist:
            self.period = Period.objects.get_current_period_at()
        if update_fields:
            if update_fields[0] not in ['send_email_mentor', 'send_email_mentor']:
                validate_max_number_of_mentee_mentorships_per_period(self)
        else:
            validate_max_number_of_mentee_mentorships_per_period(self)

    def save(self, *args, **kwargs):
        self.clean(update_fields=kwargs.get('update_fields', list()))
        super().save(*args, **kwargs)


class MentorUpdate(models.Model):
    mentor = models.ForeignKey(Mentor, verbose_name='Mentor')
    code = models.UUIDField(verbose_name='Código')
    payload = JSONField(verbose_name='Carga Útil')
    creation_ip_address = models.GenericIPAddressField(
        verbose_name='Dirección IP de creación')
    created_at = models.DateTimeField(
        verbose_name='Fecha y hora de creación ',
        auto_now_add=True)
    review_ip_address = models.GenericIPAddressField(
        verbose_name='Dirección IP de revisión')
    reviewed_at = models.DateTimeField(verbose_name='Fecha y hora de revisión')
    state = models.CharField(
        verbose_name='Estado',
        choices=MENTOR_UPDATE_STATE_CHOICES,
        max_length=20)