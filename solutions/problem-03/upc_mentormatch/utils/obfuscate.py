from cryptography.fernet import Fernet

from django.apps import apps
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist


def obfuscate(instance):
    fernet = Fernet(settings.FERNET_KEY)

    msg = '{}-{}-{}'.format(
        instance._meta.app_label.lower(),
        instance._meta.model.__name__.lower(),
        instance.pk
    ).encode('utf-8')
    return fernet.encrypt(msg).decode()


def deobfuscate(msg):
    fernet = Fernet(settings.FERNET_KEY)
    value = fernet.decrypt(msg.encode('utf-8'))
    try:
        app_label, model_name, pk = value.decode().split('-')
    except (ValueError, UnicodeDecodeError):
        raise ObjectDoesNotExist
    return apps.get_model(app_label, model_name).objects.get(pk=pk)
