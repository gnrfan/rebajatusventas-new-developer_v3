import os
import uuid
from .files import get_file_extension
from upc_mentormatch import settings as mentormatch_settings

def category_or_topic_uuid_upload_to(instance, filename):
    ext = get_file_extension(filename)
    filename = ''.join([str(uuid.uuid4()), ext])
    prefix = mentormatch_settings.MENTORMATCH_CATEGORY_TOPIC_IMAGE_FOLDER
    return os.path.join(prefix, filename)


def img_uuid_upload_to(instance, filename):
    ext = get_file_extension(filename)
    filename = ''.join([str(uuid.uuid4()), ext])
    prefix = mentormatch_settings.MENTORMATCH_MENTOR_IMAGE_FOLDER
    return os.path.join(prefix, filename)
