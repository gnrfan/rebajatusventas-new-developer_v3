import os.path

def get_file_extension(filename, normalize=True, normalization='lowercase'):
    result = os.path.splitext(filename)[1]
    if normalize:
        if  normalization == 'lowercase':
            result = result.lower()
        elif normalization == 'uppercase':
            result = result.upper()
    return result