from django.conf import settings
from django.core.urlresolvers import reverse

def remove_scheme_from_url(url):
    parts = url.split('://')
    if len(parts) > 0:
        url = '://' + ''.join(parts[1:])
    return url


def add_url_prefix(url):
    if url.startswith('/'):
        url = ''.join([
            settings.URL_PREFIX,
            url
        ])
    return url


def get_absolute_url(relative_url, absolute=True, scheme=True):
    url = relative_url
    if absolute:
        url = add_url_prefix(url)
    if not scheme:
        url = remove_scheme_from_url(url)
    return url


def reverse_absolute_url(viewname, absolute=True, scheme=True, **kwargs):
    url = reverse(viewname, **kwargs)
    if absolute:
        url = add_url_prefix(url)
    if not scheme:
        url = remove_scheme_from_url(url)
    return url