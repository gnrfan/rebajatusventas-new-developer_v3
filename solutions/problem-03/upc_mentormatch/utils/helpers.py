def get_career_availability(career_code):
    from ..models import Mentor
    career_active = Mentor.objects.filter(
        is_active=True,
        career__code=career_code
    )
    if career_active.exists():
        return 1
    else:
        return 0


def headers_in_list(headers_csv):
    valid = False
    msg = 'ok'
    columns = ['name', 'father_name', 'mother_name', 'document_number',
               'is_active', 'career_id', 'availability_id', 'graduation_year',
               'email', 'phone', 'linkedin', 'skype',
               'is_email_preferred', 'is_phone_preferred', 
               'is_linkedin_preferred', 'is_skype_preferred', 'period', 'topics']
    if headers_csv == columns:
        valid = True
        return valid, msg
    else:
        a = set(columns)
        b = set(headers_csv)
        
        headers_extra = a.symmetric_difference(b)
        if headers_extra:
            msg = str()
            headers_lost = a.difference(b)
            headers_unknow = headers_extra.difference(headers_lost)
            if headers_lost:
                msg = "Falta header {}.".format(headers_lost)
            if headers_unknow:
                msg += " Header {} desconocido.".format(headers_unknow)
            return valid, msg