from upc_mentormatch import settings as mentormatch_settings
from upc_applicants.models import Applicant


def get_applicants_for_mentormatch():
    return Applicant.objects.filter(
        status__pk=mentormatch_settings.MENTORMATCH_APPLICANT_STATUS_PK,
        period__for_mentormatch=True
    )
