angular.module('mentorList', ['ngCookies'])

.value('API_BASE', '/api/')

.run(function($cookies, $rootScope, $window){
    $rootScope.token = $cookies.get('TOKEN');

    if (!$rootScope.token){
        $window.location.href = '/incorporate/';
    }
})

.controller('ListController', function($http, $rootScope, $scope, API_BASE){
    $scope.mentorList = null;

    $scope.getMentorList = function(){
        if ($scope.carrer){
            url = API_BASE + 'mentormatch/mentors/?career=' + $scope.career;
        } else {
            url = API_BASE + 'mentormatch/mentors/';
        }
        $http({
            method: 'GET',
            url: url,
            headers: {
                'Authorization': 'Token ' + $rootScope.token
            }
        })
            .then(function(response){
                $scope.mentorList = response.data;
            }, function(err){
                console.error(err.data);
            });
    }

    $scope.getMentorList();
});
