jQuery(document).ready(function($){
    $('div.collapse').on('show.bs.collapse', function(){
        $el = $(this).parents('.mb-5').find('a[data-toggle="collapse"]');
        $el.find('i.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
    });

    $('div.collapse').on('hide.bs.collapse', function(){
        $el = $(this).parents('.mb-5').find('a[data-toggle="collapse"]');
        $el.find('i.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
    });
})
