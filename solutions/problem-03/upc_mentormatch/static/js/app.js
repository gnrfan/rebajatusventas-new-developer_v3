angular.module('menteeApp', ['ngCookies'])

.value('API_BASE', '/api/')

.filter('slice', function() {
    return function(arr, start, end) {
        if(!arr)
            return [];
        return arr.slice(start, end);
    };
})

.run(function($cookies, $http, $rootScope, $window, API_BASE){
    $rootScope.token = $cookies.get('TOKEN');
    $rootScope.user = null;

    var getApplicant = function(){
        $http({
            method: 'GET',
            url: API_BASE + 'applicant/',
            headers: {
                'Authorization': 'Token ' + $rootScope.token
            }
        })
            .then(function(response){
                $rootScope.user = response.data;
            }, function(err){
                $cookies.remove('TOKEN');
                $window.location.href = '/incorporate/';
            })
    }
    if ($rootScope.token){
        getApplicant();
    } else {
        $window.location.href = '/incorporate/';
    }
})

.controller('FormController', function($http, $rootScope, $scope, $window, API_BASE){
    $scope.loading = true;

    $rootScope.$watch('user', function(){
        $scope.user.mentee = $rootScope.user;
        if($rootScope.user){
            $scope.getCareerName($rootScope.user.career);
            $scope.loading = false;
        }
    });

    $scope.userDefaults = {
        topic: {code: null, name: 'Selecciona tu tema de interés'},
    }
    $scope.user = {
        step: 1,
        topic: $scope.userDefaults.topic,
    };
    $scope.categoryList;
    $scope.topicList;
    $scope.metorList;

    $scope.loadModal = function(message){
        $modal = $('#notificationModal');
        $modal.find('modal-content').text(message);
        $modal.modal({
            keyboard: false,
        })
    }

    $scope.getCategoryList = function(){
        if($scope.categoryList) return;
        $scope.loading = true;
        $http({
            method: 'GET',
            url: API_BASE + 'mentormatch/categories',
            headers: {
                'Authorization': 'Token ' + $rootScope.token
            }
        })
            .then(function(response){
                $scope.categoryList = response.data;
                $scope.loading = false;
            }, function(err){
                $scope.loadModal('Error: ' + err.data);
                $scope.loading = false;
            });
    }

    $scope.getCareerName = function(career_id){
        $http.get(API_BASE + 'commons/careers/' + career_id)
            .then(function(response){
                $scope.user.career = response.data;
            }, function(err){
                $scope.loadModal('Error: ' + err.data);
            })
    }

    $scope.getMentorList = function(){
        if(!$scope.user.topic && !$scope.user.topic.code){
            return;
        }

        $scope.loading = true;
        $http({
            method: 'GET',
            url: API_BASE + 'mentormatch/mentors/?topic=' + $scope.user.topic.code,
            headers: {
                'Authorization': 'Token ' + $rootScope.token,
            }
        })
            .then(function(response){
                $scope.mentorList = response.data;
                $scope.loading = false;
            }, function(err){
                $scope.loadModal('Error: ' + err.data);
                $scope.loading = false;
            });
    }

    $scope.getTopicList = function(){
        angular.forEach($scope.user.category.topics, function(topic){
            $scope.topicList.push(topic);
        });
    }

    $scope.getCategoryList();

    $scope.nextStep = function(to){
        if(to == 3 && (!$scope.user.category || !$scope.user.category.code)){
            return;
        } else if(to == 4 && (!$scope.user.topic.code || !$scope.user.mentor)){
            return;
        } else if(to == 5 && !$scope.user.mentor){
            return;
        }

        $section = angular.element('[data-step="' + to + '"]');
        $section.fadeIn();
        angular.element('html,body').stop().animate({
            scrollTop: $section.offset().top
        }, 'slow', function(){
            $scope.checkStep();
        });
    }

    $scope.checkStep = function(){
        $s1 = angular.element('[data-step="1"]');
        $s2 = angular.element('[data-step="2"]');
        $s3 = angular.element('[data-step="3"]');
        $s4 = angular.element('[data-step="4"]');
        $s5 = angular.element('[data-step="5"]');

        if($s1.is(':visible')){s1_height = parseInt($s1.height())} else {s1_height = 0}
        if($s2.is(':visible')){s2_height = parseInt(s1_height + $s2.height())} else {s2_height = 0}
        if($s3.is(':visible')){s3_height = parseInt(s2_height + $s3.height())} else {s3_height = 0}
        if($s4.is(':visible')){s4_height = parseInt(s3_height + $s4.height())} else {s4_height = 0}
        if($s5.is(':visible')){s5_height = parseInt(s4_height + $s5.height())} else {s5_height = 0}
        currentOffset = $(document).scrollTop();
        if(currentOffset < s1_height){
            step = 1;
        } else if(currentOffset >= s1_height && currentOffset < s2_height) {
            step = 2;
        } else if(currentOffset >= s2_height && currentOffset < s3_height) {
            step = 3;
        } else if(currentOffset >= s3_height && currentOffset < s4_height) {
            step = 4;
        } else if(currentOffset >= s4_height && currentOffset < s5_height) {
            step = 5;
        }
        $scope.$apply(function(){
            $scope.user.step = step;
        });
    }

    angular.element(window).on('scroll',function(){
        $scope.checkStep();
    });

    $scope.selectCategory = function(category){
        $scope.user.category = category;
        $scope.topicList = [$scope.userDefaults.topic];
        $scope.user.topic = $scope.userDefaults.topic;
        $scope.mentorList = [];
        $scope.user.mentor = null;
        angular.element('[data-step="4"]').hide();
        angular.element('[data-step="5"]').hide();
        $scope.getTopicList();
    }

    $scope.selectTopic = function(){
        if(!$scope.user.topic.code){
            $scope.user.mentor = null;
            $scope.mentorList = [];
            return
        }
        $scope.getMentorList();
    }

    $scope.selectMentor = function(mentor){
        $scope.user.mentor = mentor;
    }

    $scope.submitData = function(){
        $scope.loading = true;
        var data = {
            'mentee': $scope.user.mentee.idn,
            'mentor': $scope.user.mentor.document_number,
            'period': '0',
            'topic': $scope.user.topic.code,
        }
        $http({
            method: 'POST',
            url: API_BASE + 'mentormatch/mentor_ship/',
            headers: {
                'Authorization': 'Token ' + $rootScope.token
            },
            data: data
        })
            .then(function(data){
                $window.location.replace('/mentormatch/gracias/'+ data.data.id);
                $scope.loading = false;
            }, function(err){
                $scope.loadModal('Error: ' + err.data);
                $scope.loading = false;
            })
    }
});
