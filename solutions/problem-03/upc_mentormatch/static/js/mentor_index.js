jQuery(document).ready(function ($) {
    let loading = false;

    $('form#loginForm').submit(function (event) {
        event.preventDefault();
        if (loading) return;
        loading = true;

        const $form = $(this);
        const idn = $form.find('#id_idn').val();
        const $help = $form.parent().find('.help-text');
        $help.removeClass('bg-danger').removeClass('bg-success').hide();

        $.post('/api/mentormatch/mentor/request-access/', { idn: idn })
            .then(function (response) {
                loading = false;
                $help.addClass('bg-success').text(response.detail).show();
            }, function (err) {
                loading = false;
                if (err.responseJSON) {
                    $help.addClass('bg-danger').text(err.responseJSON.detail).show();
                } else {
                    $help.addClass('bg-danger').text("Ha ocurrido un error desconcido.").show();
                }
            });
    })
})
