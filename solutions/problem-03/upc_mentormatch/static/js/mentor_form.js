angular.module('mentorApp', ['ngCookies'])
    .value('API_BASE', '/api/')

    .factory('careerService', function ($http, API_BASE) {
        return {
            list: function () {
                return $http.get(`${API_BASE}commons/careers/`);
            },
            get: function (id) {
                return $http.get(`${API_BASE}commons/careers/${id}`);
            }
        }
    })

    .factory('categoryService', function ($http, API_BASE) {
        return {
            list: function () {
                return $http.get(`${API_BASE}mentormatch/categories`);
            }
        }
    })

    .factory('mentorService', function ($http, API_BASE) {
        return {
            auth: function (token) {
                return $http.post(`${API_BASE}mentormatch/mentor/auth/`, { 'token': token });
            },
            update: function (pk, data) {
                return $http.put(`${API_BASE}mentormatch/mentor/update/${pk}/`, data);
            },
            photo: function (pk, data) {
                const request = {
                    method: 'POST',
                    url: `${API_BASE}mentormatch/mentor/photo/${pk}/`,
                    data: data,
                    headers: {
                        'Content-Type': undefined
                    }
                };
                return $http(request);
            }
        }
    })

    .run(function ($cookies, $rootScope, $window, mentorService) {
        const token = $cookies.get('MENTOR_TOKEN');
        mentorService.auth(token)
            .then(function (response) {
                $rootScope.$broadcast('load', response.data);
            }, function (err) {
                $cookies.remove('MENTOR_TOKEN');
                $window.location.replace('/mentormatch/mentors/');
            })
    })

    .controller('FormController', function ($scope, $window, careerService, categoryService, mentorService) {
        $scope.static_choices = {
            graduation_year: [
                { id: null, name: 'Selecciona tu año de egreso' },
                { id: 2018, name: '2018' },
                { id: 2017, name: '2017' },
                { id: 2016, name: '2016' },
                { id: 2015, name: '2015' },
                { id: 2014, name: '2014' },
                { id: 2013, name: '2013' },
                { id: 2012, name: '2012' },
                { id: 2011, name: '2011' },
                { id: 2010, name: '2010' },
                { id: 2009, name: '2009' },
                { id: 2008, name: '2008' },
                { id: 2007, name: '2007' },
            ],
            month: [
                { id: null, name: 'Mes' },
                { id: 1, name: 'Enero' },
                { id: 2, name: 'Febrero' },
                { id: 3, name: 'Marzo' },
                { id: 4, name: 'Abril' },
                { id: 5, name: 'Mayo' },
                { id: 6, name: 'Junio' },
                { id: 7, name: 'Julio' },
                { id: 8, name: 'Agosto' },
                { id: 9, name: 'Septiembre' },
                { id: 10, name: 'Octubre' },
                { id: 11, name: 'Noviembre' },
                { id: 12, name: 'Diciembre' },
            ],
            year: [
                { id: null, name: 'Año' },
                { id: 2019, name: '2019' },
                { id: 2018, name: '2018' },
                { id: 2017, name: '2017' },
                { id: 2016, name: '2016' },
                { id: 2015, name: '2015' },
                { id: 2014, name: '2014' },
                { id: 2013, name: '2013' },
                { id: 2012, name: '2012' },
                { id: 2011, name: '2011' },
                { id: 2010, name: '2010' },
                { id: 2009, name: '2009' },
                { id: 2008, name: '2008' },
                { id: 2007, name: '2007' },
            ],
        }

        $scope.dynamic_choices = {
            career: [
                { id: null, name: 'Selecciona la carrera que estudiaste en la UPC.' }
            ],
            category: []
        }

        $scope.base_user = {
            graduation_year: $scope.static_choices.graduation_year[0],
            career: $scope.dynamic_choices.career[0],
            professional_experiences: [
                {
                    start_date_month: $scope.static_choices.month[0],
                    start_date_year: $scope.static_choices.year[0],
                    is_current_job: true,
                    end_date_month: $scope.static_choices.month[0],
                    end_date_year: $scope.static_choices.year[0]
                }
            ],
            contact_information: {
                email: {
                    value: null,
                    preferred: true
                },
                cellphone: {
                    value: null,
                    preferred: true
                },
                skype: {
                    value: null,
                    preferred: false
                },
                linkedin: {
                    value: null,
                    preferred: false
                },
            },
            tempTopics: {},
        }

        $scope.$on('load', function (event, user) {
            $scope.user = Object.assign({}, $scope.base_user, user);
            // Fix choices
            if (user.graduation_year) {
                $scope.user.graduation_year = {
                    id: user.graduation_year,
                    name: String(user.graduation_year)
                }
            }

            // Fix topics
            if (user.topics.length > 0) {
                if (!$scope.user.tempTopics) $scope.user.tempTopics = [];
                user.topics.forEach(function (topic) {
                    $scope.user.tempTopics[topic.code] = true;
                })
            }

            if (user.professional_experiences.length == 0) {
                $scope.user.professional_experiences = $scope.base_user.professional_experiences;
            } else {
                user.professional_experiences.forEach(function (pe, index) {
                    start_date = pe.starting_date.split('-');
                    start_date_year = start_date[0];
                    start_date_month = start_date[1];

                    $scope.user.professional_experiences[index].start_date_month = { id: parseInt(start_date_month), name: String(start_date_month) };
                    $scope.user.professional_experiences[index].start_date_year = { id: parseInt(start_date_year), name: String(start_date_year) };

                    if (!pe.is_current_job) {
                        end_date = pe.ending_date.split('-');
                        end_date_year = end_date[0];
                        end_date_month = end_date[1];

                        $scope.user.professional_experiences[index].end_date_month = { id: parseInt(end_date_month), name: String(end_date_month) };
                        $scope.user.professional_experiences[index].end_date_year = { id: parseInt(end_date_year), name: String(end_date_year) };
                    }
                });
            }
        });

        $scope.targetField = null;

        $scope.focusSelect = function ($event) {
            if ($event.target === null) return;
            $scope.targetField = $event.target;
        };

        $scope.validateSelect = function (el) {
            if (el === undefined) {
                el = angular.element($scope.targetField);
            }

            const name = el.attr('name');
            const parts = name.split('-');
            let val;

            if (parts.length > 1) {
                val = $scope.user[parts[0]][parts[1]][parts[2]].id;
            } else {
                val = $scope.user[name].id;
            }

            if (val === null) {
                $scope.mentorForm[name].$setValidity('valid', false);
            } else {
                $scope.mentorForm[name].$setValidity('valid', true);
            }
        }

        $scope.addAnother = function () {
            const baseExperience = {
                start_date_month: $scope.static_choices.month[0],
                start_date_year: $scope.static_choices.year[0],
                is_current_job: true,
                end_date_month: $scope.static_choices.month[0],
                end_date_year: $scope.static_choices.year[0]
            }

            $scope.user.professional_experiences.push(baseExperience);
        }

        careerService.list()
            .then(function (response) {
                const initial = $scope.dynamic_choices.career[0];
                $scope.dynamic_choices.career = [];
                $scope.dynamic_choices.career.push(initial);
                response.data.forEach(function (item) {
                    $scope.dynamic_choices.career.push(item);
                });
            });

        categoryService.list()
            .then(function (response) {
                $scope.dynamic_choices.category = response.data;
            });


        angular.element("#id_photo").change(function () {
            $scope.uploadPhoto();
        });

        $scope.drop = function (ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            console.log('Data', data)
            ev.target.appendChild(document.getElementById(data));
        }

        $scope.allowDrop = function (ev) {
            ev.preventDefault();
        }

        $scope.selectImage = function () {
            angular.element('#id_photo').click();
        }

        $scope.uploadPhoto = function () {
            const value = document.getElementById('id_photo');
            console.log('Loading', value.files);

            if (!$scope.user || !value) return;

            let formData = new FormData();
            formData.append('photo', value.files[0]);

            mentorService.photo($scope.user.id, formData)
                .then(function (response) {
                    $scope.user.image_url = response.data.url;
                })
        }

        $scope.submit = function () {
            let value;
            let data = Object.assign({}, $scope.user);

            data.career = parseInt(data.career.id);
            data.graduation_year = parseInt(data.graduation_year.id);
            data.topics_of_interest = [];
            _.forEach(data.tempTopics, function (val, key) {
                if (val == true) {
                    data.topics_of_interest.push(parseInt(key));
                }
            });

            // Uploader photo
            _.unset(data, 'photo');
            $scope.uploadPhoto();

            // Fix years and months in professional experience
            value = _.get(data, 'professional_experiences', []);
            value.forEach(function (item, index) {
                start_month = item.start_date_month.id;
                start_year = item.start_date_year.id;
                data.professional_experiences[index].starting_date = `${start_year}-${start_month}-01`;

                if (!item.is_current_job) {
                    end_month = item.end_date_month.id;
                    end_year = item.end_date_year.id;
                    data.professional_experiences[index].ending_date = `${end_year}-${end_month}-01`;
                } else {
                    data.professional_experiences[index].ending_date = null;
                }
            });

            mentorService.update($scope.user.id, data)
                .then(function (response) {
                    $window.location.replace('/mentormatch/mentors/thanks/');
                }, function (err) {
                    console.log(err)
                })
        }
    });

