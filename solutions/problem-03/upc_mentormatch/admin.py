from django.contrib import admin
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _

from django.conf import settings
from upc.admin.sites import ops_site
from upc.utils.admin import CsvExportAdminMixin
from upc_core.utils.datetime import datetime_as_project_timezone as dtaptz

from upc_applicants.models import Period

from . import models
from .forms import (
    CategoryTopicAdminForm,
    MenteeAdminForm
)


def link_mentor_login(modeladmin, request, queryset):
    for q in queryset:
        q.send_login_email(request.get_host())


link_mentor_login.short_description = 'Enviar correo de login a mentores seleccionados'


def activate_mentor(modeladmin, request, queryset):
    queryset.update(is_active=True)


activate_mentor.short_description = 'Activar mentores seleccionados'


def deactivate_mentor(modeladmin, request, queryset):
    queryset.update(is_active=False)


deactivate_mentor.short_description = 'Desactivar mentores seleccionados'


def get_latest_active_period():
    try:
        return Period.objects.filter(for_mentormatch=True) \
            .order_by('-start_date').first()
    except Exception:
        return None


def get_dynamic_associate_period_desc():
    period = get_latest_active_period()
    if period:
        period_desc = period.description
    else:
        period_desc = _('Ninguno')
    template = 'Asociar mentores seleccionados '
    template += 'con el último periodo activo de '
    template += 'pregrado ({period})'
    return template.format(period=period_desc)


def associate_period(modeladmin, request, queryset):
    period = get_latest_active_period()
    if period:
        for mentor in queryset:
            mentor.periods.add(period)
            mentor.save()


associate_period.short_description = get_dynamic_associate_period_desc()


@admin.register(models.CategoryTopic, site=ops_site)
class CategoryTopicAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'record_type', 'parent', 'is_active')
    list_filter = ('record_type', )
    readonly_fields = ('pk', 'created_at', 'updated_at')
    search_fields = ('name', )
    ordering = ('id', )
    form = CategoryTopicAdminForm


@admin.register(models.Mentor, site=ops_site)
class MentorAdmin(admin.ModelAdmin):
    list_display = (
        'pk', 'full_name', 'document_number',
        'career', 'graduation_year',
        'display_periods', 'is_active'
    )
    readonly_fields = ('pk', )
    search_fields = ('name', )
    change_list_template = 'change_list.html'
    ordering = ['id', ]
    filter_horizontal = ('periods', 'topics_of_interest')
    list_filter = ('is_active', 'career', 'periods', 'topics_of_interest')
    actions = [
        activate_mentor,
        deactivate_mentor,
        associate_period
    ]

    def full_name(self, instance):
        return instance.full_name
    full_name.short_description = _('Nombre completo')

    def display_periods(self, instance):
        return ", ".join(sorted([str(p) for p in instance.periods.all()]))
    display_periods.short_description = _('Periodos')


@admin.register(models.MentorAvailability, site=ops_site)
class MentorAvailabilityAdmin(admin.ModelAdmin):
    list_display = ('pk', 'code', 'description')
    readonly_fields = ('pk', )
    search_fields = ('code', 'description')
    ordering = ('code', )


@admin.register(models.WorkExperience, site=ops_site)
class WorkExperienceAdmin(admin.ModelAdmin):
    list_display = (
        'pk', 'mentor', 'position', 'company',
        'starting_date', 'ending_date', 'current_position'
    )
    list_filter = ('mentor',)
    readonly_fields = ('pk',)


@admin.register(models.Mentee, site=ops_site)
class CategoryTopicAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'fullname',
        'document_type',
        'document_number',
        'created_at'
    )
    readonly_fields = ('pk', 'created_at', 'updated_at')
    search_fields = (
        'applicant__first_name',
        'applicant__paternal_last_name',
        'applicant__maternal_last_name',
        'applicant__email'
    )
    list_filter = ('applicant__period',)
    form = MenteeAdminForm

    def fullname(self, instance):
        return instance.applicant.full_name

    fullname.short_description = _('Nombre')

    def document_type(self, instance):
        return instance.applicant.idt.name

    document_type.short_description = _('Tipo de documento')

    def document_number(self, instance):
        return instance.applicant.idn

    document_number.short_description = _('Número de documento')


@admin.register(models.Mentorship, site=ops_site)
class MentorshipAdmin(CsvExportAdminMixin, admin.ModelAdmin):
    list_display = (
        'pk',
        'mentor',
        'mentee',
        'topic',
        'period',
        'created_at'
    )
    readonly_fields = ('pk', 'created_at', 'updated_at')
    search_fields = (
        'mentee__applicant__first_name',
        'mentee__applicant__paternal_last_name',
        'mentee__applicant__maternal_last_name',
        'mentee__applicant__email',
        'mentee__applicant__idn',
        'mentor__name',
        'mentor__father_name',
        'mentor__mother_name',
        'mentor__email',
        'mentor__document_number'
    )
    csv_fields = (
        ("MENTOR", lambda p: p.mentor),
        ("DNI MENTOR", lambda p: p.mentor.document_number),
        ("MENTEE", lambda p: p.mentee),
        ("DNI MENTEE", lambda p: p.mentee.applicant.idn),
        ("PERIODO", lambda p: p.period),
        ("utm_source", lambda p: p.utm_source),
        ("utm_medium", lambda p: p.utm_medium),
        ("utm_campaign", lambda p: p.utm_campaign),
        ("GUID HD", lambda p: p.guid_hd),
        ("FECHA DE CREACIÓN", lambda p: dtaptz(p.created_at)))
    list_filter = ('topic', 'mentor',)

    def get_actions(self, request):
        actions = super(MentorshipAdmin, self).get_actions(request)
        if request.user.has_perm('upc_mentormatch.download_csv') is False:
            del(actions['export_as_csv'])
            return actions
        return actions
