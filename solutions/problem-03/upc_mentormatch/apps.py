from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class UpcMentormatchConfig(AppConfig):
    name = 'upc_mentormatch'
    verbose_name = _("UPC :: Mentor Match")
