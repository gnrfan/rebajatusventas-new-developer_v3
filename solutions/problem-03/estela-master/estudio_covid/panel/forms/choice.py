from django import forms
from django.forms import inlineformset_factory

from questions.models import ChoiceEntry, Question


class ChoiceEntryForm(forms.ModelForm):
    class Meta:
        exclude = (
            'created_at', 'updated_at', 'created_by', 'updated_by'
        )
        model = ChoiceEntry
        widgets = {
            'text': forms.TextInput
        }


QuestionChoiceEntryFormSet = inlineformset_factory(
    Question,
    ChoiceEntry,
    form=ChoiceEntryForm,
    extra=1
)
