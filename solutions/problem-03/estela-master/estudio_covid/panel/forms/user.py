from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth import forms as auth_forms
from django.utils.translation import ugettext_lazy as _


UserModel = get_user_model()


class UserCreateForm(auth_forms.UserCreationForm):
    email = forms.EmailField(required=True)
    name = forms.CharField(required=True)
    lastname = forms.CharField(required=True)
    second_lastname = forms.CharField(required=True)

    class Meta:
        fields = (
            'email', 'password1', 'password2', 'name', 'lastname',
            'second_lastname'
        )
        model = UserModel

    def clean_email(self):
        data = self.cleaned_data['email']

        if self._meta.model.objects.filter(email=data).exists():
            raise forms.ValidationError(
                _("A user with that email already exists.")
            )

        return data


class UserUpdateForm(auth_forms.UserChangeForm):
    class Meta:
        fields = (
            'password', 'email', 'name', 'lastname', 'second_lastname',
            'display_mode'
        )
        model = UserModel
