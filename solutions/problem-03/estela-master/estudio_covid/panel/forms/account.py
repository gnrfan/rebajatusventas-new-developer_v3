from django import forms
from django.contrib.auth import forms as auth_forms, get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _


UserModel = get_user_model()


class AuthenticationForm(auth_forms.AuthenticationForm):
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        backend = ModelBackend()

        if username is not None and password:
            self.user_cache = backend.authenticate(
                self.request, username=username, password=password
            )
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data


class AccountChangeForm(auth_forms.UserChangeForm):
    email = forms.EmailField(disabled=True)
    name = forms.CharField()
    lastname = forms.CharField()
    second_lastname = forms.CharField()

    class Meta:
        fields = (
            'password', 'email', 'name', 'lastname', 'second_lastname',
            'display_mode'
        )
        model = UserModel


class AccountPasswordResetForm(auth_forms.PasswordResetForm):
    def save(
        self, domain_override=None,
        subject_template_name=(
            'panel/account/account_password_reset_subject.txt'
        ),
        email_template_name='panel/account/account_password_reset_email.html',
        use_https=False, token_generator=default_token_generator,
        from_email=None, request=None, html_email_template_name=None,
        extra_email_context=None
    ):
        """
        Generate a one-use only link for resetting password and send it to the
        user.
        """
        email = self.cleaned_data["email"]
        for user in self.get_users(email):
            if not domain_override:
                domain = request.get_host()
            else:
                domain = domain_override
            context = {
                'email': email,
                'domain': domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
                **(extra_email_context or {}),
            }
            self.send_mail(
                subject_template_name, email_template_name, context,
                from_email, email,
                html_email_template_name=html_email_template_name,
            )
