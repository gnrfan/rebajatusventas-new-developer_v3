from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from dal import autocomplete

from answers.models import Questionnaire


class QuestionnaireForm(forms.ModelForm):
    control_case = forms.ModelChoiceField(
        queryset=Questionnaire.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(
            url='panel:questionnaire_autocomplete'
        )
    )

    class Meta:
        exclude = (
            'created_at', 'updated_at', 'created_by', 'updated_by'
        )
        model = Questionnaire

    def clean_control_case(self):
        value = self.cleaned_data.get('control_case', None)
        if value:
            qs = Questionnaire.objects.filter(control_case=value).exclude(pk=self.instance.pk)
            if qs.exists():
                raise ValidationError(_("El entrevistado ya es caso de control de otra ficha."))
        return value



class QuestionnaireResolveTestForm(forms.Form):
    name = forms.CharField(
        label=_("name")
    )

    def __init__(self, questionnaire, *args, **kwargs):
        self.questionnaire = questionnaire
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        raise NotImplementedError(
            'This is a sample/test form that must not be used.'
        )
