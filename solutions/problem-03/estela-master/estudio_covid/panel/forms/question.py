from django import forms

from questions.models import Question


class QuestionForm(forms.ModelForm):
    class Meta:
        exclude = (
            'created_at', 'updated_at', 'created_by', 'updated_by'
        )
        model = Question
        widgets = {
            'text': forms.TextInput
        }
