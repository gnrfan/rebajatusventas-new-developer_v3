/* global ClipboardJS, get_format, gettext, initDateTimeWidget, jQuery, XMLHttpRequest */
(function (globals) {
  var django = globals.django || (globals.django = {})

  django.getCookie = function (name) {
    var cookieValue = null
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';')
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim()
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
          break
        }
      }
    }
    return cookieValue
  }

  django.apiCall = function (url, method, params, cb) {
    var xhttp = new XMLHttpRequest()
    xhttp.open(method, url, true)
    xhttp.setRequestHeader('Content-type', 'application/json;charset=UTF-8')
    xhttp.setRequestHeader('X-CSRFToken', django.getCookie('csrftoken'))
    xhttp.send(JSON.stringify(params))
    xhttp.onreadystatechange = function () {
      if (xhttp.readyState === 4 && xhttp.responseText) {
        try {
          var responseJson = JSON.parse(xhttp.responseText)
        } catch (SyntaxError) {
          responseJson = {}
        }
      }
      if (cb) cb(responseJson, xhttp.status)
    }
  }

  var pythonToJsFormats = Object({
    '%a': 'ddd',
    '%A': 'dddd',
    '%w': 'd',
    '%d': 'DD',
    '%b': 'MMM',
    '%B': 'MMMM',
    '%m': 'MM',
    '%y': 'YY',
    '%Y': 'YYYY',
    '%H': 'HH',
    '%I': 'hh',
    '%p': 'A',
    '%M': 'mm',
    '%S': 'ss',
    '%f': 'SSS',
    '%z': 'ZZ',
    '%Z': 'z',
    '%j': 'DDDD',
    '%U': 'ww',
    '%W': 'ww',
    '%c': 'ddd MMM DD HH:mm:ss YYYY',
    '%x': 'MM/DD/YYYY',
    '%X': 'HH:mm:ss',
    '%%': '%'
  })

  django.convert_format = (format) => {
    var converted = format

    for (var name in pythonToJsFormats) {
      if (Object.prototype.hasOwnProperty.call(pythonToJsFormats, name)) {
        converted = converted.split(name).join(pythonToJsFormats[name])
      }
    }

    return converted
  }

  django.initDateTimeWidget = (input) => {
    var isInit = input.data('DateTimePicker')
    if (isInit) isInit.destroy()
    if (input.attr('id').indexOf('__prefix__') > -1) return

    var type = input.data('datetimepicker')
    var options = {}

    if (type === 'datetime') {
      options = {
        format: convert_format(get_format('DATETIME_INPUT_FORMATS')[2]),
        viewMode: input.data('viewmode') || 'days'
      }
    } else if (type === 'date') {
      options = {
        format: convert_format(get_format('DATE_INPUT_FORMATS')[0]),
        viewMode: input.data('viewmode') || 'days'
      }
    } else if (type === 'time') {
      options = {
        format: convert_format(get_format('TIME_INPUT_FORMATS')[2])
      }
    } else {
      return
    }

    input.datetimepicker(options)
  }

  if (!django.djaneiro_initialized) {
    globals.apiCall = django.apiCall
    globals.convert_format = django.convert_format
    globals.getCookie = django.getCookie
    globals.initDateTimeWidget = django.initDateTimeWidget

    django.djaneiro_initialized = true
  }
}(this))

jQuery(function () {
  if ($.fn.datetimepicker) {
    $.extend(true, $.fn.datetimepicker.defaults, {
      icons: {
        time: 'far fa-clock',
        date: 'far fa-calendar',
        up: 'fas fa-arrow-up',
        down: 'fas fa-arrow-down',
        previous: 'fas fa-chevron-left',
        next: 'fas fa-chevron-right',
        today: 'fas fa-calendar-check',
        clear: 'far fa-trash-alt',
        close: 'far fa-times-circle'
      },
      sideBySide: true,
      showClear: true,
      showClose: true,
      showTodayButton: true,
      useCurrent: false
    })

    $('[data-datetimepicker="datetime"], [data-datetimepicker="date"], [data-datetimepicker="time"]').each(function () {
      initDateTimeWidget($(this))
    })
  }
})
