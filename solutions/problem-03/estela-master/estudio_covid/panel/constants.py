from django.utils.translation import ugettext_lazy as _


ACTIONS = {
    'add': {
        'title': _("add"),
        'level': 'primary',
        'icon': 'plus',
        'permission_prefix': 'add',
    },
    'change': {
        'title': _("change"),
        'level': 'success',
        'icon': 'pencil',
        'permission_prefix': 'change',
    },
    'delete': {
        'title': _("delete"),
        'level': 'danger',
        'icon': 'trash',
        'permission_prefix': 'delete',
    },
    'resolve': {
        'title': _("resolve"),
        'level': 'info',
        'icon': 'th-list',
        'permission_prefix': 'add',
    }
}
