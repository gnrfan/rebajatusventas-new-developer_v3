from django.contrib.auth import get_user_model
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView, DeleteView, ListView, UpdateView
)

from boilerplate.mixins import (
    ActionListMixin, CreateMessageMixin, DeleteMessageMixin, UpdateMessageMixin
)

from panel import forms


UserModel = get_user_model()


class UserListView(ActionListMixin, PermissionRequiredMixin, ListView):
    action_list = ('add', )
    model = UserModel
    paginate_by = 30
    permission_required = 'common:view_customuser'
    template_name = 'panel/user/user_list.html'

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.is_superuser:
            qs = qs.filter(
                is_superuser=False
            )
        if not self.request.user.is_staff:
            qs = qs.filter(
                is_staff=False
            )
        return qs


class UserCreateView(
    PermissionRequiredMixin, CreateMessageMixin, CreateView
):
    form_class = forms.UserCreateForm
    model = UserModel
    permission_required = 'common:add_customuser'
    success_url = reverse_lazy("panel:user_list")
    template_name = 'panel/user/user_form.html'


class UserDeleteView(
    PermissionRequiredMixin, DeleteMessageMixin, DeleteView
):
    model = UserModel
    permission_required = 'common:delete_customuser'
    success_url = reverse_lazy("panel:user_list")
    template_name = 'panel/user/user_form.html'

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.is_superuser:
            qs = qs.filter(
                is_superuser=False
            )
        if not self.request.user.is_staff:
            qs = qs.filter(
                is_staff=False
            )
        return qs


class UserUpdateView(
    PermissionRequiredMixin, UpdateMessageMixin, UpdateView
):
    form_class = forms.UserUpdateForm
    model = UserModel
    permission_required = 'common:change_customuser'
    success_url = reverse_lazy("panel:user_list")
    template_name = 'panel/user/user_form.html'

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.is_superuser:
            qs = qs.filter(
                is_superuser=False
            )
        if not self.request.user.is_staff:
            qs = qs.filter(
                is_staff=False
            )
        return qs


class UserPasswordView(
    PermissionRequiredMixin, UpdateMessageMixin, UpdateView
):
    form_class = SetPasswordForm
    model = UserModel
    permission_required = 'common:change_customuser'
    success_url = reverse_lazy('panel:user_list')
    template_name = 'panel/user/user_form.html'

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.is_superuser:
            qs = qs.filter(
                is_superuser=False
            )
        if not self.request.user.is_staff:
            qs = qs.filter(
                is_staff=False
            )
        return qs

    def get_form(self):
        kwargs = self.get_form_kwargs()
        kwargs.pop('instance')

        return self.get_form_class()(
            user=self.get_object(), **kwargs
        )
