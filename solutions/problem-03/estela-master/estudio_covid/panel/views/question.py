from django.contrib.auth.mixins import PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView, DeleteView, DetailView, ListView, UpdateView
)

from boilerplate.mixins import (
    ActionListMixin, CreateMessageMixin, DeleteMessageMixin,
    ExtraFormsAndFormsetsMixin, UpdateMessageMixin
)

from panel import forms
from questions.models import Question


class QuestionCreateView(
    ExtraFormsAndFormsetsMixin, PermissionRequiredMixin, CreateMessageMixin,
    CreateView
):
    form_class = forms.QuestionForm
    model = Question
    template_name = 'panel/question/question_form.html'
    permission_required = 'questions:add_question'

    def get_formset_list(self):
        return (
            forms.QuestionChoiceEntryFormSet,
        )


class QuestionDeleteView(
    PermissionRequiredMixin, DeleteMessageMixin, DeleteView
):
    model = Question
    permission_required = 'questions:delete_question'
    template_name = 'panel/question/question_form.html'
    success_url = reverse_lazy('panel:question_list')


class QuestionDetailView(PermissionRequiredMixin, DetailView):
    model = Question
    template_name = 'panel/question/question_detail.html'
    permission_required = 'questions:view_question'


class QuestionListView(ActionListMixin, PermissionRequiredMixin, ListView):
    action_list = ('add', )
    model = Question
    paginate_by = 30
    template_name = 'panel/question/question_list.html'
    permission_required = 'questions:view_question'


class QuestionUpdateView(
    ExtraFormsAndFormsetsMixin, PermissionRequiredMixin, UpdateMessageMixin,
    UpdateView
):
    form_class = forms.QuestionForm
    model = Question
    permission_required = 'questions:change_question'
    template_name = 'panel/question/question_form.html'

    def get_formset_list(self):
        return (
            forms.QuestionChoiceEntryFormSet,
        )
