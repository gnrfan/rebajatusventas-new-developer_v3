from uuid import UUID
from django import forms
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (
    CreateView, DeleteView, DetailView, ListView, UpdateView
)
from django.views.generic.edit import FormMixin

from boilerplate.mixins import (
    ActionListMixin, CreateMessageMixin, DeleteMessageMixin, UpdateMessageMixin
)

from panel.forms import QuestionnaireForm, QuestionnaireResolveTestForm
from answers.models import Questionnaire, Answer
from questions.models import Question, ChoiceEntry
from questions import constants


class QuestionnaireCreateView(
    PermissionRequiredMixin, CreateMessageMixin, CreateView
):
    form_class = QuestionnaireForm
    model = Questionnaire
    template_name = 'panel/questionnaire/questionnaire_form.html'
    permission_required = 'answers:add_questionnaire'


class QuestionnaireDeleteView(
    PermissionRequiredMixin, DeleteMessageMixin, DeleteView
):
    model = Questionnaire
    permission_required = 'answers:delete_questionnaire'
    template_name = 'panel/questionnaire/questionnaire_form.html'
    success_url = reverse_lazy('panel:questionnaire_list')


class QuestionnaireDetailView(PermissionRequiredMixin, DetailView):
    model = Questionnaire
    template_name = 'panel/questionnaire/questionnaire_detail.html'
    permission_required = 'answers:view_questionnaire'


class QuestionnaireListView(
    ActionListMixin, PermissionRequiredMixin, ListView
):
    action_list = ('add', )
    model = Questionnaire
    paginate_by = 30
    template_name = 'panel/questionnaire/questionnaire_list.html'
    permission_required = 'answers:view_questionnaire'


class QuestionnaireUpdateView(
    PermissionRequiredMixin, UpdateMessageMixin, UpdateView
):
    form_class = QuestionnaireForm
    model = Questionnaire
    permission_required = 'answers:change_questionnaire'
    template_name = 'panel/questionnaire/questionnaire_form.html'


class QuestionnaireResolveView(PermissionRequiredMixin, FormMixin, DetailView):
    form_class = QuestionnaireResolveTestForm
    model = Questionnaire
    template_name = 'panel/questionnaire/questionnaire_resolve.html'
    permission_required = 'answers:add_questionnaire'

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_class(self):
        # TODO: Modificar con la función que te devuelve la
        #  clase del formulario, OJO: sin instanciar
        # return self.form_class

        def form_init(self, instance=None, *args, **kwargs):
            if instance is None:
                instance = Questionnaire()
            super(QuestionnaireForm, self).__init__(instance=instance, *args, **kwargs)

        def form_save(self, *args, **kwargs):
            instance = super(QuestionnaireForm, self).save(*args, **kwargs)
            cleaned_data = self.cleaned_data
            for field_name in self.dynamic_field_names:
                if field_name.endswith("__other"):
                    continue
                question = Question.objects.get(variable_name=field_name)
                answer, created = Answer.objects.get_or_create(questionnaire=instance, question=question)
                if question.question_type in constants.DIRECT_VALUE_QUESTION_TYPES:
                    if question.question_type == constants.QUESTION_TYPE_BOOLEAN:
                        answer.value = 'SI' if cleaned_data[field_name] else 'NO'
                    else: 
                        answer.value = str(cleaned_data[field_name])
                elif question.question_type in constants.CHOICE_VALUE_QUESTION_TYPES:
                    answer.selected_entries.clear()
                    uuid_list = []
                    if type(cleaned_data[field_name]) is list:
                        for value in cleaned_data[field_name]:
                            if value != "__other__":
                                uuid_list.append(UUID(value))
                    else:
                        if cleaned_data[field_name] != "__other__":
                            uuid_list.append(UUID(cleaned_data[field_name]))
                    for uuid in uuid_list:
                        choice_entry = ChoiceEntry.objects.get(uuid=uuid)
                        answer.selected_entries.add(choice_entry)
                    if question.other_answer:
                        related_name = field_name + "__other"
                        related_value = cleaned_data[related_name]
                        if len(related_value.strip()) > 0:
                            answer.value = related_value.strip()
                answer.save()
            instance.save()


        def form_clean(self):
            for field_name, value in self.cleaned_data.copy().items():
                if field_name.endswith("__other"):
                    related_field_name = field_name[:-len("__other")]
                    condition1 = self.cleaned_data.get(related_field_name, None) == "__other__"
                    condition1 = condition1 and len(value.strip()) > 0
                    condition2 = "__other__" in self.cleaned_data.get(related_field_name, [])
                    condition2 = condition2 and len(value.strip()) > 0
                    condition3 = self.cleaned_data.get(related_field_name, None) != "__other__"
                    condition3 = condition2 and len(value.strip()) == 0
                    condition4 = "__other__" not in self.cleaned_data.get(related_field_name, [])
                    condition4 = condition4 and len(value.strip()) == 0
                    ok = condition1 or condition2 or condition3 or condition4
                    if not ok:
                        if not condition1:
                            error_message = _("Debe ingresar un valor para la opción 'Otro'.")
                        elif not condition2:
                            error_message = _("Debe elegir la opción 'Otro' ó dejar su valor en blanco.")
                        self.add_error(related_field_name, error_message) 
            return self.cleaned_data

        form_attrs = {}
        form_attrs['__init__'] = form_init
        form_attrs['clean'] = form_clean
        form_attrs['save'] = form_save
        form_attrs['dynamic_field_names'] = []

        form_fields = {}

        for question in Question.objects.all():
            kwargs = dict(
                label=question.text,
                help_text=question.interviewer_instructions,
                required=False
            )
            if question.question_type == constants.QUESTION_TYPE_INTEGER:
                field = forms.IntegerField
                if question.min_value is not None:
                    kwargs['min_value'] = question.min_value
                if question.max_value is not None:
                    kwargs['max_value'] = question.max_value
            elif question.question_type == constants.QUESTION_TYPE_BOOLEAN:
                field = forms.ChoiceField
                kwargs['choices'] = [
                    (True, _('Si')),
                    (False, _('No'))
                ]
                kwargs['widget'] = forms.RadioSelect
            elif question.question_type == constants.QUESTION_TYPE_DATE:
                field = forms.DateField
            elif question.question_type == constants.QUESTION_TYPE_CHOICES:
                field = forms.ChoiceField
                kwargs['choices'] = [
                    (choice.uuid, choice.text)
                    for choice in question.choice_entries.all()
                ]
                kwargs['widget'] = forms.CheckboxInput
                if question.other_answer:
                    kwargs['choices'].append(('__other__', _("Otro")))
            elif question.question_type == constants.QUESTION_TYPE_FLOAT:
                field = forms.DecimalField
            elif question.question_type == constants.QUESTION_TYPE_MULTIPLE_CHOICE:
                field = forms.MultipleChoiceField
                kwargs['choices'] = [
                    (choice.uuid, choice.text)
                    for choice in question.choice_entries.all()
                ]
                kwargs['widget'] = forms.CheckboxSelectMultiple
                if question.other_answer:
                    kwargs['choices'].append(('__other__', _("Otro")))
            elif question.question_type == constants.QUESTION_TYPE_SINGLE_CHOICE:
                field = forms.ChoiceField
                kwargs['choices'] = [
                    (choice.uuid, choice.text)
                    for choice in question.choice_entries.all()
                ]
                kwargs['widget'] = forms.RadioSelect
                if question.other_answer:
                    kwargs['choices'].append(('__other__', _("Otro")))
            else:
                field = forms.CharField

            form_fields[question.variable_name] = field(**kwargs)
            form_attrs['dynamic_field_names'].append(question.variable_name)

            if question.other_answer:
                field_name = "%s__other" % question.variable_name
                form_fields[field_name] = forms.CharField(
                    required=False,
                    widget=forms.TextInput(attrs={
                        'data-isother': True,
                        'placeholder': _('Otra: %s') % question.text
                    })
                )
                form_attrs['dynamic_field_names'].append(field_name)

        form_attrs.update(form_fields)

        return type(
            'QuestionnaireResolveForm', (QuestionnaireForm,), form_attrs
        )

    def get_form_kwargs(self):
        # TODO: Al __init__ del formulario se le está pasando el
        # parámetro `instance` para saber a que cuestionario
        # debemos guardarle las respuestas.
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.object
        kwargs['initial'] = self.get_initial()
        return kwargs

    def get_initial(self):
        initial = super().get_initial()
        if 'subject_identifier' not in initial:
            initial['subject_identifier'] = self.object.subject_identifier
        if self.object.control_case and 'control_case' not in initial:
            initial['control_case'] = self.object.control_case.pk
        for answer in self.object.answers.all():
            question = answer.question
            if question.variable_name in initial:
                continue
            if question.question_type == constants.QUESTION_TYPE_INTEGER:
                initial[question.variable_name] = int(answer.value)
            elif question.question_type == constants.QUESTION_TYPE_FLOAT:
                initial[question.variable_name] = float(answer.value)
            elif question.question_type == constants.QUESTION_TYPE_DATE:
                initial[question.variable_name] = answer.value
            elif question.question_type == constants.QUESTION_TYPE_BOOLEAN:
                initial[question.variable_name] = True if answer.value == 'SI' else False
            elif question.question_type == constants.QUESTION_TYPE_SINGLE_CHOICE:
                choice_entry = answer.selected_entries.first()
                if choice_entry:
                    initial[question.variable_name] = choice_entry.uuid
                if question.other_answer and answer.value is not None and len(answer.value) > 0:
                    initial[question.variable_name] = "__other__"
                    initial[question.variable_name + "__other"] = answer.value
            elif question.question_type == constants.QUESTION_TYPE_MULTIPLE_CHOICE:
                if answer.selected_entries.exists():
                    initial[question.variable_name] = [str(choice_entry.uuid) for choice_entry in answer.selected_entries.all()]
                else:
                    initial[question.variable_name] = []
                if question.other_answer and answer.value is not None and len(answer.value) > 0:
                    initial[question.variable_name].append("__other__")
                    initial[question.variable_name + "__other"] = answer.value

        return initial

    def get_success_url(self):
        return self.object.get_absolute_url()

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
