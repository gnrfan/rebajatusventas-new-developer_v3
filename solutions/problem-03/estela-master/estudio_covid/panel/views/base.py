from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView, View

from sentry_sdk import last_event_id


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'panel/index.html'
    raise_exception = False


class ErrorTestView(View):
    raise_exception = True

    def get(self, request, *args, **kwargs):
        if any([request.user.is_staff, request.user.is_superuser]):
            raise NotImplementedError
        return HttpResponse('How did you get here?', content_type='text/plain')


def handler500(request, *args, **kwargs):
    return render(request, '500.html', {
        'sentry_event_id': last_event_id(),
    }, status=500)
