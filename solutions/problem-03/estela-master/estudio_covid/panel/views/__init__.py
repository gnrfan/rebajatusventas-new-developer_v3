from panel.views.base import (
    IndexView,
    ErrorTestView
)
from panel.views.account import (
    AccountDetailView,
    AccountLoginView,
    AccountLogoutView,
    AccountPasswordChangeDoneView,
    AccountPasswordChangeView,
    AccountPasswordResetCompleteView,
    AccountPasswordResetConfirmView,
    AccountPasswordResetDoneView,
    AccountPasswordResetView,
    AccountUpdateView
)
from panel.views.question import (
    QuestionCreateView,
    QuestionDeleteView,
    QuestionDetailView,
    QuestionListView,
    QuestionUpdateView
)
from panel.views.questionnaire import (
    QuestionnaireCreateView,
    QuestionnaireDeleteView,
    QuestionnaireDetailView,
    QuestionnaireListView,
    QuestionnaireResolveView,
    QuestionnaireUpdateView
)
from panel.views.user import (
    UserCreateView,
    UserDeleteView,
    UserListView,
    UserPasswordView,
    UserUpdateView
)


__all__ = [
    # Base
    'IndexView',
    'ErrorTestView',
    # Account
    'AccountDetailView',
    'AccountLoginView',
    'AccountLogoutView',
    'AccountPasswordChangeDoneView',
    'AccountPasswordChangeView',
    'AccountPasswordResetCompleteView',
    'AccountPasswordResetConfirmView',
    'AccountPasswordResetDoneView',
    'AccountPasswordResetView',
    'AccountUpdateView',
    # Question
    'QuestionCreateView',
    'QuestionDeleteView',
    'QuestionDetailView',
    'QuestionListView',
    'QuestionUpdateView',
    # Questionnaire
    'QuestionnaireCreateView',
    'QuestionnaireDeleteView',
    'QuestionnaireDetailView',
    'QuestionnaireListView',
    'QuestionnaireResolveView',
    'QuestionnaireUpdateView',
    # User
    'UserCreateView',
    'UserDeleteView',
    'UserListView',
    'UserPasswordView',
    'UserUpdateView'
]
