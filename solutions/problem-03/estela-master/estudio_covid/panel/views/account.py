from django.contrib.auth import get_user_model, login as auth_login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (
    LoginView, LogoutView, PasswordResetView, PasswordResetDoneView,
    PasswordResetConfirmView, PasswordResetCompleteView, PasswordChangeView,
    PasswordChangeDoneView
)
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, UpdateView

from boilerplate.mixins import UpdateMessageMixin

from panel import forms


UserModel = get_user_model()


class AccountDetailView(LoginRequiredMixin, DetailView):
    model = UserModel
    template_name = 'panel/account/account_detail.html'

    def get_object(self):
        return self.request.user


class AccountLoginView(LoginView):
    form_class = forms.AuthenticationForm
    redirect_authenticated_user = False
    success_url = reverse_lazy('panel:index')
    template_name = 'panel/account/account_login.html'

    def form_valid(self, form):
        """Security check complete. Log the user in."""
        user = form.get_user()

        auth_login(
            self.request,
            user,
            backend='django.contrib.auth.backends.ModelBackend'
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        url = self.get_redirect_url()
        return url or self.success_url


class AccountLogoutView(LogoutView):
    next_page = reverse_lazy('panel:account_login')
    template_name = 'panel/account/account_logout.html'


class AccountPasswordResetView(PasswordResetView):
    email_template_name = 'panel/account/account_password_reset_email.txt'
    html_email_template_name = (
        'panel/account/account_password_reset_email.html'
    )
    subject_template_name = 'panel/account/account_password_reset_subject.txt'
    success_url = reverse_lazy('panel:account_password_reset_done')
    template_name = 'panel/account/account_password_reset_form.html'


class AccountPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'panel/account/account_password_reset_done.html'


class AccountPasswordResetConfirmView(PasswordResetConfirmView):
    post_reset_login = True
    post_reset_login_backend = 'django.contrib.auth.backends.ModelBackend'
    success_url = reverse_lazy('panel:account_password_reset_complete')
    template_name = 'panel/account/account_password_reset_confirm.html'


class AccountPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = 'panel/account/account_password_reset_complete.html'


class AccountPasswordChangeView(PasswordChangeView):
    success_url = reverse_lazy('panel:account_password_change_done')
    template_name = 'panel/account/account_password_change_form.html'


class AccountPasswordChangeDoneView(PasswordChangeDoneView):
    template_name = 'panel/account/account_password_change_done.html'


class AccountUpdateView(UpdateMessageMixin, UpdateView):
    form_class = forms.AccountChangeForm
    model = UserModel
    success_message = _("Your account has been update successfully.")
    success_url = reverse_lazy('panel:account_detail')
    template_name = 'panel/account/account_form.html'

    def get_object(self):
        return self.request.user
