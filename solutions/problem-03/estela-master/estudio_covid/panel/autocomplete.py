from django.contrib.auth.mixins import PermissionRequiredMixin
from dal import autocomplete

from answers.models import Questionnaire


class QuestionnaireAutocompleteView(
    PermissionRequiredMixin, autocomplete.Select2QuerySetView
):
    model = Questionnaire
    model_field_name = 'subject_identifier'
    permission_required = 'answers:view_questionnaire'
