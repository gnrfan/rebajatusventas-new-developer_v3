import ast
from django import template

register = template.Library()


@register.filter(name='add_attrs')
def add_attrs(value, arg):
    custom_arg = "{{ {} }}".format(arg)
    attrs_dict = ast.literal_eval(custom_arg)
    return value.as_widget(attrs=attrs_dict)
