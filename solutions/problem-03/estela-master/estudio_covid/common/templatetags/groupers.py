import itertools
from django import template

register = template.Library()


@register.filter(name='regroup_in_chunks')
def regroup_in_chunks(iterable, max_chunk_size):
    l = list(iterable)
    return [ l[i:i+max_chunk_size] for i in range(0, len(l), max_chunk_size) ]


@register.filter(name='chunkdict')
def chunkdict(iterable, max_chunk_size):
    results = []
    l = list(iterable)
    chunks = [ l[i:i+max_chunk_size] for i in range(0, len(l), max_chunk_size) ]
    for chunk, group in enumerate(chunks):
        for item in group:
            results.append({'item': item, 'chunk': chunk})
    return results