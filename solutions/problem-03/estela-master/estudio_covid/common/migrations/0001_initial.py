# Generated by Django 3.1.2 on 2020-10-02 01:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Fecha y hora de creación')),
                ('updated_at', models.DateTimeField(blank=True, null=True, verbose_name='Fecha y hora de actualización')),
                ('password', models.CharField(max_length=128, verbose_name='Contraseña')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='Último inicio de sesión')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Correo electrónico')),
                ('name', models.CharField(blank=True, max_length=128, null=True, verbose_name='Nombres')),
                ('lastname', models.CharField(blank=True, max_length=128, null=True, verbose_name='Apellido paterno')),
                ('second_lastname', models.CharField(blank=True, max_length=128, null=True, verbose_name='Apellido materno')),
                ('display_mode', models.CharField(choices=[('pe', 'Nombre Apellido_Paterno Apellido_Materno (Perú)'), ('formal', 'Apellido_Paterno Apellido_Materno, Nombres (Formal)')], default='pe', max_length=8, verbose_name='Modo de presentación del nombre')),
                ('is_staff', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.localtime)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='common_customuser_created', to=settings.AUTH_USER_MODEL, verbose_name='Creado por')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='common_customuser_updated', to=settings.AUTH_USER_MODEL, verbose_name='Actualizado por')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'Usuario',
                'verbose_name_plural': 'Usuarios',
                'ordering': ('name', 'lastname', 'second_lastname'),
            },
        ),
    ]
