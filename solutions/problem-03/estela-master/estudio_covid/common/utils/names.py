def get_initials(*args):
    parts = []
    for p in args:
        if p is None:
            p = ''
        p = ''.join([c for c in p if c == ' ' or c.isalpha()])
        p = p.strip().upper()
        if p:
            parts.append(p)
    return ' '.join(["%s." % p[0] for p in parts]).strip()
