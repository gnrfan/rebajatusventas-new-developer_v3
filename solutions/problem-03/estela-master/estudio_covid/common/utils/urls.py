from urllib.parse import urlparse
from django.conf import settings

def prefix_with_site_url(path_or_url):
    parts = urlparse(path_or_url)
    if not parts.scheme and not parts.netloc:
        return settings.SITE_URL + path_or_url
    else:
        return path_or_url


def url_is_relative(path_or_url):
    parts = urlparse(path_or_url)
    return parts.scheme == '' and parts.netloc == ''


def remove_initial_slash_if_needed(path):
    if path.startswith('/'):
        return path[1:]
    return path