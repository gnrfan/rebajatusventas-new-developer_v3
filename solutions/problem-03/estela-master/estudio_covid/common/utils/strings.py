import string

def extract_named_placeholders(template):
    try:
        return [v[1] for v in string.Formatter().parse(template) if v[1] is not None]
    except:
        return None

def interpolate_from_attributes(template, obj, raise_exception=True):
    placeholders = extract_named_placeholders(template)
    if not placeholders:
        return template
    params = {}
    for p in placeholders:
        try:
            attr = getattr(obj, p)
            if callable(attr):
                value = attr()
            else:
                value = attr
            params[p] = value
        except AttributeError as e:
            if raise_exception:
                raise e
            else:
                return None
    return template.format(**params)

def strip_if_str(value):
    if type(value) is str:
        return value.strip()
    else:
        return value

