def empty_list_if_none(items):
    return items if items is not None else []