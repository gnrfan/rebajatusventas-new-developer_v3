import re

def get_full_class_name(obj):
    module = obj.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return obj.__class__.__name__
    return module + '.' + obj.__class__.__name__


def get_base_class_name(cls):
    parts = cls.split('.')
    if parts:
        return parts[-1]


def get_status_code_from_exception(exc):
    m = re.match(r'Http(?P<status_code>[0-9]{3})', repr(exc))
    if m and len(m.groups()) > 0:
        status_code = m.groups()[0]
        if status_code.isdigit():
            status_code = int(status_code)
        return status_code
    else:
        return 500