from datetime import timedelta
from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.utils.timezone import localtime

def timestamp_or_default(ts=None):
    return ts if ts else localtime()


def ts_in_range(start_ts, end_ts, ts=None):
    ts = timestamp_or_default(ts)
    return (
        (
            (start_ts is not None) and 
            (start_ts <= ts) and 
            (end_ts is None)
        ) 
        or
        (
            (start_ts is not None) and 
            (start_ts <= ts) and 
            (end_ts is not None) and
            (ts <= end_ts)
        )
    )


def timestamp_for_keys(ts=None):
    if not ts:
        ts = localtime()
    fmtstr = getattr(
        settings,
        'TIMESTAMP_KV_FORMAT',
        '%Y:%m:%d:%H:%M'
    )
    return ts.strftime(fmtstr)


def localtime_to_the_minute():
    params = {'second': 0, 'microsecond': 0}
    return localtime().replace(**params)


def localtime_to_the_minute_for_keys(ts=None):
    if ts is None:
        ts = localtime_to_the_minute()
    format = getattr(
        settings,
        'LOCALTIME_KV_FORMAT',
        '%Y:%m:%d:%H:%M'
    )
    return ts.strftime(format)

def get_unix_timestamp(ts=None):
    if not ts:
        ts = localtime()
    return ts.timestamp()

def n_hours_ago(n=1, ts=None):
    if ts is None:
        ts = localtime()
    return ts - timedelta(hours=n)

def one_hour_ago(ts=None):
    return n_hours_ago(1, ts)    

def n_days_ago(n=1, ts=None):
    if ts is None:
        ts = localtime()
    return ts - timedelta(days=n)

def one_day_ago(ts=None):
    return n_days_ago(1, ts)    

def n_weeks_ago(n=1, ts=None):
    if ts is None:
        ts = localtime()
    return ts - timedelta(weeks=n)

def one_week_ago(ts=None):
    return n_weeks_ago(1, ts)    

def n_months_ago(n=1, ts=None):
    if ts is None:
        ts = localtime()
    return ts - relativedelta(months=n)

def one_month_ago(ts=None):
    return n_months_ago(1, ts)

def date_to_datetime(value):
    args = value.timetuple()[:6]
    return datetime(*args)

def hour_to_12(hour):
    if hour < 0 or hour > 23:
        raise ValueError("Hour out of range")
    elif hour >= 13 and hour <= 23:
        return hour - 12
    else:
        return hour

def hour_am_pm(hour, uppercase=True):
    if hour < 0 or hour > 23:
        raise ValueError("Hour out of range")
    elif hour >= 12 and hour <= 23:
        result = "pm"
    else:
        result = "am"
    if uppercase:
        result = result.upper()
    return result

