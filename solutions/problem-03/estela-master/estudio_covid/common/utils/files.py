import os

def extract_file_extension(filepath, force_lowercase=True):
    filename, file_extension = os.path.splitext(filepath)
    if force_lowercase and file_extension:
        file_extension = file_extension.lower()
    return file_extension