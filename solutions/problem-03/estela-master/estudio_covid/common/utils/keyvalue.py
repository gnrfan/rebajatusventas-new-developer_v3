def build_key(value, prefix=None, suffix=None, **kwargs):
    key_type = kwargs.get('key_type', None)
    separator = kwargs.get('separator', ':')
    parts=[]
    if prefix:
        parts.append(prefix)
    parts.append(value)
    if key_type:
        parts.append(key_type)
    if suffix:
        parts.append(suffix)
    return separator.join(parts)