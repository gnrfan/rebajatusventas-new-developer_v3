import os
import uuid
from .files import extract_file_extension
from django.utils.deconstruct import deconstructible
from .strings import (
    extract_named_placeholders,
    interpolate_from_attributes
)

@deconstructible
class UUIDUploadRenamer(object):

    def __init__(self, path):
        self.path = path

    def __call__(self, instance, filename):
        ext = extract_file_extension(filename)
        fname = ''.join([str(uuid.uuid4()), ext])
        return os.path.join(self.path, fname)
 
@deconstructible
class UUIDAttributeUploadRenamer(object):

    def __init__(self, path):
        self.path = path

    def __call__(self, instance, filename):
        npath = interpolate_from_attributes(self.path, instance)
        ext = extract_file_extension(filename)
        fname = ''.join([str(uuid.uuid4()), ext])
        return os.path.join(npath, fname)