import pytz
from django.utils import timezone


def get_timezone(tz=None, raise_exception=True, use_default=True):
    if tz is None:
        return timezone.get_current_timezone()
    elif type(tz) is str:
        try:
            return pytz.timezone(tz)
        except pytz.exceptions.UnknownTimeZoneError as e:
            if raise_exception:
                raise e
            elif use_default:
                return timezone.get_current_timezone()
    if issubclass(type(tz), pytz.tzinfo.DstTzInfo):
        return tz


def normalize_datetime(value, tz=None, *args, **kwargs):
    tz = get_timezone(tz, *args, **kwargs)
    if tz:
        return tz.normalize(value)


def normalized_now(tz=None, *args, **kwargs):
    ts = timezone.now()
    return normalize_datetime(ts, tz, *args, **kwargs)


def starting_ending_date(tz_start, tz_end, *args, **kwargs):
    now = timezone.now()

    started = False
    if now >= tz_start:
        started = True

    finished = False
    if now > tz_end:
        finished = True

    return started, finished