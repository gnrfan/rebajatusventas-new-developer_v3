import re
from django.utils.text import slugify


def expand_email(email):
    replacements = {
        '.': ' dot ',
        '@': ' at '
    }
    result = email.lower()
    for k, v in replacements.items():
        result = result.replace(k, v)
    return result


def slugify_email(email):
    return slugify(expand_email(email))


def email_is_valid(email):
    expresion_regular = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0" \
                        r"b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-" \
                        r"9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0" \
                        r"-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|" \
                        r"[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x" \
                        r"0c\x0e-\x7f])+)\])"

    return re.match(expresion_regular, email) is not None