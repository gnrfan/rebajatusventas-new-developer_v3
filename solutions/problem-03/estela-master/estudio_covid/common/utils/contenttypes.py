from django.core import signing
from django.conf import settings
from django.contrib.contenttypes.models import ContentType

def dict_from_model_instance(instance):
    ct = ContentType.objects.get_for_model(instance)
    return {
        'app_label': ct.app_label,
        'model': ct.model,
        'object_id': instance.pk
    }

def sign_dict_from_model_instance(instance):
    d = dict_from_model_instance(instance)
    return signing.dumps(d)

def load_instance_from_signed_dict(signed_dict):
    try:
        d = signing.loads(signed_dict)
        ct = ContentType.objects.get(
            app_label=d['app_label'],
            model=d['model']
        )
        return ct.model_class().objects.get(pk=d['object_id'])
    except Exception as e:
        raise e

def model_instance_for_keys(instance, data=None):
    if not data:
        data = dict_from_model_instance(instance)
    format = getattr(
        settings,
        'CONTENT_TYPE_KV_FORMAT',
        '{app_label}:{model}:{object_id}'
    )
    return format.format(**data)