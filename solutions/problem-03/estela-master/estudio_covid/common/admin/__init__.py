from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from ..models import CustomUser

class BaseModelAdmin(admin.ModelAdmin):
    readonly_fields = [
        'uuid',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ]


class BaseTabularInline(admin.TabularInline):
    readonly_fields = [
        'uuid',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ]

@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    
    filter_horizontal = ('groups', 'user_permissions',)
    
    fieldsets = (
        (None, {'fields': ('uuid', 'email', 'password')}),
        (_('Personal info'), {
            'fields': (
                'name',
                'lastname',
                'second_lastname',
                'display_mode',
            )
        }),
        (_('Permissions'), {
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions'
            ),
        }),
        (_('Important dates'), {
            'fields': (
                'last_login',
                'date_joined',
                'created_at',
                'updated_at',
                'created_by',
                'updated_by'
            )
        }),
    )

    list_display = [
        'uuid',
        'email',
        'name',
        'lastname',
        'second_lastname',
        'is_superuser',
        'is_staff'
    ]

    readonly_fields = (
        'uuid',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    )

    search_fields = (
        'uuid',
        'email',
        'name',
        'lastname',
        'second_lastname'
    )

    ordering = (
        '-created_at',
    )