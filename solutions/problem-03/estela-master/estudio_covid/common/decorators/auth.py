from django.contrib.auth import logout

def automatic_logout(view):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            logout(request)
        return view(request, *args, **kwargs)
    return wrapper