from django.utils.html import format_html
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import CreateView as OriginalCreateView
from django.views.generic.edit import UpdateView as OriginalUpdateView
from django.views.generic.edit import DeleteView as OriginalDeleteView
from django.db.models import ProtectedError
from django.contrib import messages

class CreateView(OriginalCreateView):
    pass


class UpdateView(OriginalUpdateView):

    flash_messages = True
    
    def form_valid(self, form):
        """If the form is valid, redirect to the supplied URL."""
        if "_save" in self.request.POST:
            return super().form_valid(form)
        elif "_continue" in self.request.POST:
            return self.form_valid_continue(form)
        elif "_addanother" in self.request.POST:
            return self.form_valid_addanother(form)
    
    def form_valid(self, form):
        if self.flash_messages:
            messages.success(
                self.request,
                self.get_success_message()
            )
        return super().form_valid(form)

    def form_valid_continue(self, form):
        if self.flash_messages:
            messages.success(
                self.request,
                self.get_success_message_continue()
            )
        redirect_url = self.request.path # FIXME
        #redirect_url = add_preserved_filters({'preserved_filters': preserved_filters, 'opts': opts}, redirect_url)
        return HttpResponseRedirect(redirect_url)

    def form_valid_addanother(self, form):
        if self.flash_messages:
            messages.success(
                self.request,
                self.get_success_message_addanother()
            )
        redirect_url = self.get_success_url() # FIXME
        #redirect_url = add_preserved_filters({'preserved_filters': preserved_filters, 'opts': opts}, redirect_url)
        return HttpResponseRedirect(redirect_url)
 
    def get_success_message(self):
        return format_html(
            _("El objeto “{obj}” fué actualizado exitosamente."),
            obj=self.object
        )

    def get_success_message_continue(self):
        return format_html(
            _("El objeto “{obj}” fué actualizado exitosamente. "
              "Continúe actualizándolo."),
            obj=self.object
        )
    
    def get_success_message_addanother(self):
        return format_html(
            _("El objeto “{obj}” fué actualizado exitosamente. "
              "Puede agregar otro a continuación."),
            obj=self.object
        )

class DeleteView(OriginalDeleteView):

    flash_messages = True

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        context = self.get_context_data(object=self.object)

        try:
            self.object.delete()
            if self.flash_messages:
                messages.success(request, self.get_success_message())
            return HttpResponseRedirect(success_url)
        except ProtectedError as e:
            context['protected_objects'] = e.protected_objects
            return self.render_to_response(context)
        
    def get_success_message(self):
        return format_html(
            _("El objeto “{obj}” fué eliminado exitosamente."),
            obj=self.object
        )
            