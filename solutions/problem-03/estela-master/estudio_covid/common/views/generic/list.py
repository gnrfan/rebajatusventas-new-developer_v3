from collections import OrderedDict
from django.views.generic.list import ListView as OriginalListView
from django.utils.http import urlencode
from django.conf import settings

class ListView(OriginalListView):

    paginate_by_kwarg = 'per_page'
    paginate_by_choices = None
    filters_form_class = None

    def get_paginate_by(self, queryset):
        """
        Get the number of items to paginate by from GET parameters.
        If `paginate_by_choices` is not None then check the value against 
        available options.
        If `paginate_by` is set to `None`, no pagination occurs.
        """
        paginate_by = self.kwargs.get(self.paginate_by_kwarg)
        if not paginate_by:
            try:
                paginate_by = int(self.request.GET.get(self.paginate_by_kwarg))
                # paginate_by must be a valid positive integer higher than zero
                if paginate_by < 1:
                    paginate_by = None
            except (TypeError, ValueError):
                pass
            if paginate_by and self.paginate_by_choices:
                enforcing = getattr(settings, 'CUSTOM_ADMIN_ENFORCE_PAGINATE_BY_CHOICES', True)
                if enforcing and (not paginate_by in dict(self.paginate_by_choices).keys()):
                    paginate_by = self.paginate_by
        if not paginate_by:
            paginate_by = self.paginate_by
        return paginate_by


    def get_context_data(self, *, object_list=None, **kwargs):
        context = {}
        if self.has_filters_form():
            context['filters_form'] = self.get_filters_form()
        if self.has_url_params():
            context['url_params'] = self.get_encoded_url_params(ampersand=True)
        context.update(kwargs)
        return super().get_context_data(**context)


    def get_filters_form_class(self):
        return self.filters_form_class


    def has_filters_form(self):
        filters_form_class = self.get_filters_form_class()
        return filters_form_class is not None 


    def get_filters_form(self):
        if self.has_filters_form():
            filters_form_class = self.get_filters_form_class()
            return filters_form_class(
                initial=self.request.GET
            )

    def get_url_params(self, excluded=None):
        if excluded is None:
            excluded = self.get_excluded_url_params()
        params = OrderedDict()
        for param_name, param_value in self.request.GET.items():
            if param_name in excluded:
                continue
            params[param_name] = param_value
        return params

    def has_url_params(self, excluded=None):
        return len(self.get_url_params(excluded=excluded)) > 0

    def get_encoded_url_params(self, excluded=None, ampersand=False):
        params = self.get_url_params(excluded=excluded)
        result = urlencode(params)
        if ampersand and len(result)>0:
            result = '&' + result
        return result

    def get_excluded_url_params(self):
        return ['page']