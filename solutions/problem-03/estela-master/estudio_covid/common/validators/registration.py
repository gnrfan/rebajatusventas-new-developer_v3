import re
import django.contrib.auth.password_validation as validators

from django.core import exceptions
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from ..models import CustomUser


def validate_dni(dni):
    if not re.match(r"^[0-9]{8}$", dni):
        raise ValidationError(
            _(f"El valor '{dni}' no es un DNI válido"),
            params={"document_number": dni}
        )


def validate_ce(ce):
    if not re.match(r"^([A-Z0-9])+$", ce):
        raise ValidationError(
            _(f"El valor '{ce}' no es un CE válido"),
            params={"document_number": ce}
        )


def validate_pasaporte(pasaporte):
    if not re.match(r"^([A-Z0-9])+$", pasaporte):
        raise ValidationError(
            _(f"El valor '{pasaporte}' no es un Pasaporte válido"),
            params={"document_number": pasaporte},
        )

def validate_unique_identity(identity):
    qs = PlatformUser.objects.filter(
        document_type=identity['document_type'],
        document_number=identity['document_number']
    )
    if qs.exists():
        msg  = 'El tipo y número de documento indicado '
        msg += 'ya se encuentran asociados a una '
        msg += 'cuenta de usuario.'
        raise ValidationError(_(msg))


def validate_unique_email(email):
    qs = PlatformUser.objects.filter(email=email)
    if qs.exists():
        msg  = 'La dirección de correo electrónico '
        msg += 'ya se encuentran asociada a una '
        msg += 'cuenta de usuario.'
        raise ValidationError(_(msg))


def password_is_valid(password, user=None):

    errors = None
    try:
        if user:
            validators.validate_password(password=password, user=user)
        else:
            validators.validate_password(password=password)

    except exceptions.ValidationError as error:
        errors = list(error)

    return errors
