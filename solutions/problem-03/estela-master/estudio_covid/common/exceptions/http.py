class Http400(Exception):
    pass

class Http401(Exception):
    pass

class Http403(Exception):
    pass

class Http405(Exception):
    pass

class Http406(Exception):
    pass

class Http409(Exception):
    pass