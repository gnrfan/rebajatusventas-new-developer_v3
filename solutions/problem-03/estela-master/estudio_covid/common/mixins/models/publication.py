from common.utils.time import ts_in_range

class PublicationModelMixin(object):

    def is_currently_published(self, ts=None):
        return ts_in_range(
            start_ts=self.publication_date,
            end_ts=self.published_until,
            ts=ts
        )