class SocialSharingModelMixin(object):

    def share_on_facebook(self):
        from public.utils.sharing import share_on_facebook
        return share_on_facebook(
            url=self.get_absolute_url(),
            text=self.title
        )


    def share_on_twitter(self):
        from public.utils.sharing import share_on_twitter
        return share_on_twitter(
            url=self.get_absolute_url(),
            text=str(self)
        )


    def share_on_linkedin(self):
        from public.utils.sharing import share_on_linkedin
        return share_on_linkedin(
            url=self.get_absolute_url(),
            text=str(self)
        )

    def share_on_whatsapp(self):
        from public.utils.sharing import share_on_whatsapp
        return share_on_whatsapp(
            url=self.get_absolute_url(),
            text=str(self)
        )