from collections import OrderedDict
from rest_framework.response import Response

class JSONErrorResponseMixin(object):

    def error_response(self, message, status_code=400):
        doc = OrderedDict()
        doc['status'] = 'error'
        doc['message'] = message
        resp = Response(doc)
        resp.status_code = status_code
        return resp