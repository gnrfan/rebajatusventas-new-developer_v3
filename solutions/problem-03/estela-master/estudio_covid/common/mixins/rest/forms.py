from collections import OrderedDict
from django.utils.translation import ugettext_lazy as _

class RESTfulFormMixin(object):

    def get_blank_doc(self):
        return OrderedDict()

    def get_status_code(self):
        if self.errors:
            status_code = getattr(self, 'http_status_code', None)
            if status_code:
                return status_code
            else:
                return 400
        else:
            return 200

    def get_errors(self):
        return getattr(self, 'errors', {})

    def has_errors(self):
        errors = self.get_errors()
        return len(errors) > 0

    def has_multiple_errors(self):
        errors = self.get_errors()
        return len(errors) >= 2

    def has_one_error(self):
        errors = self.get_errors()
        return len(errors) == 1
    
    def get_single_error(self):
        errors = self.get_errors()
        if len(errors) > 0:
            return list(errors.values())[0]

    def get_error_message(self):
        if self.has_multiple_errors():
            return _('Múltiples errores registrados')
        elif self.has_one_error():
            return self.get_single_error()

    def get_valid_doc(self, message=None):
        doc = self.get_blank_doc()
        doc["status"] = "ok"
        doc["message"] = message if message else "Registration completed."
        return doc

    def get_errors_doc(self, message=None):
        doc = self.get_blank_doc()
        doc["status"] = "error"
        doc["message"] = message if message else self.get_error_message()
        if self.has_multiple_errors():
            doc["details"] = self.get_errors()
        return doc
