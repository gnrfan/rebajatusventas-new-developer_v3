from django.contrib.contenttypes.models import ContentType

class GenericRelationManagerMixin(object):

    def new_for_model(self, model, save=False):
        try:
            ct = ContentType.objects.get_for_model(model)
        except ContentType.DoesNotExist:
            return
        instance = self.model(
            content_type=ct,
            object_id=model.pk
        )
        if save:
            instance.save()
        return instance


    def get_for_model(self, model):
        try:
            ct = ContentType.objects.get_for_model(model)
        except ContentType.DoesNotExist:
            return 
        try:
            return self.get(
                content_type=ct,
                object_id=model.pk
            )
        except self.model.DoesNotExist:
            return
    

    def delete_for_model(self, model):
        instance = self.get_for_model(model)
        if not instance:
            return False
        instance.delete()


    def get_for_model_class(self, model_class):
        try:
            ct = ContentType.objects.get_for_model(model_class)
        except ContentType.DoesNotExist:
            return 
        return self.filter(content_type=ct)