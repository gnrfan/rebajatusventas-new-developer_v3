from django.db.models import Q
from django.utils.timezone import localtime
from common.utils.time import timestamp_or_default

class PublicationManagerMixin(object):

    def published(self, ts=None):
        ts = timestamp_or_default(ts)
        q1 = Q(publication_date__lte=ts, published_until__gte=ts)
        q2 = Q(publication_date__lte=ts, published_until__isnull=True)
        return self.filter(q1 | q2)

    def non_published(self, ts=None):
        ts = timestamp_or_default(ts)
        q1 = Q(publication_date__gt=ts)
        q2 = Q(published_until__lt=ts)
        return self.filter(q1 | q2)

    def most_recently_published(self):
        return self.published().order_by('-publication_date')