import uuid
import unicodedata
from django.db import models
from django.conf import settings
from django.urls import reverse_lazy
from django.utils.timezone import localtime
from django.utils.translation import ugettext_lazy as _
from django.utils.module_loading import import_string
from django.utils.crypto import get_random_string, salted_hmac
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth import password_validation
from django.contrib.auth.hashers import (
    check_password, is_password_usable, make_password,
)
from .middleware import CurrentUserMiddleware
from .utils.email import slugify_email
from .utils.names import get_initials
from . import constants


class BaseManager(models.Manager):

    def default(self):
        return self.all()


class BaseQuerySet(models.QuerySet):
    pass


# Create your models here.
class BaseModel(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


    created_at = models.DateTimeField(
        verbose_name=_('Fecha y hora de creación'),
        auto_now_add=True,
        null=True,
        blank=True
    )

    updated_at = models.DateTimeField(
        verbose_name=_('Fecha y hora de actualización'),
        null=True,
        blank=True
    )

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
        verbose_name=_("Creado por"),
        related_name="%(app_label)s_%(class)s_created",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL,
        verbose_name=_("Actualizado por"),
        related_name="%(app_label)s_%(class)s_updated",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True

    @staticmethod
    def get_current_user():
        """
        Get the currently logged in user over middleware.
        Can be overwritten to use e.g. other middleware or additional functionality.
        :return: user instance
        """
        return CurrentUserMiddleware.get_current_user()

    def set_user_fields(self, user):
        """
        Set user-related fields before saving the instance.
        If no user with primary key is given the fields are not set.
        :param user: user instance of current user
        """
        if user and user.pk:
            if not self.pk and not self.created_by:
                self.created_by = user
            if self.is_being_updated():
                if not self.updated_by:
                    self.updated_by = user

    def is_being_updated(self):
        return self.created_at and localtime() > self.created_at

    def save(self, *args, **kwargs):
        current_user = self.get_current_user()
        if current_user is not None:
            self.set_user_fields(current_user)
        if self.is_being_updated():
            self.updated_at = localtime()
        super(BaseModel, self).save(*args, **kwargs)

    def get_content_type(self):
        return ContentType.objects.get_for_model(self)

    def fields_for__repr__(self):
        return {}

    def fields_for__repr__as_string(self):
        fields = self.fields_for__repr__()
        if fields:
            parts=[]
            for k, v in fields.items():
                if type(v) not in [int, float, bool]:
                    fragment = "{name}=\"{value}\"".format(
                        name=k,
                        value=v
                    )
                else:
                    fragment = "{name}={value}".format(
                        name=k,
                        value=v
                    )
                parts.append(fragment)
            return ' '.join(parts)
        else:
            return ''

    def __repr__(self):
        values = self.fields_for__repr__()
        if values:
            template  = "<{cls} object with id=\"{pk}\" "
            template += "{fields} "
            template += "at 0x{hexv:x}>"
            return template.format(
                cls=self.__class__.__name__,
                pk=self.pk,
                fields=self.fields_for__repr__as_string(),
                hexv=id(self)
            )
        else:
            template  = "<{cls} object with id=\"{pk}\" "
            template += "and description=\"{description}\" "
            template += "at 0x{hexv:x}>"
            return template.format(
                cls=self.__class__.__name__,
                pk=self.pk,
                description=str(self),
                hexv=id(self)
            )


class AbstractBaseUser(BaseModel):
    password = models.CharField(_('Contraseña'), max_length=128)
    last_login = models.DateTimeField(_('Último inicio de sesión'), blank=True, null=True)

    is_active = True

    REQUIRED_FIELDS = []

    # Stores the raw password if set_password() is called so that it can
    # be passed to password_changed() after the model is saved.
    _password = None

    class Meta:
        abstract = True

    def __str__(self):
        return self.get_username()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self._password is not None:
            password_validation.password_changed(self._password, self)
            self._password = None

    def get_username(self):
        """Return the username for this User."""
        return getattr(self, self.USERNAME_FIELD)

    def clean(self):
        setattr(self, self.USERNAME_FIELD, self.normalize_username(self.get_username()))

    def natural_key(self):
        return (self.get_username(),)

    @property
    def is_anonymous(self):
        """
        Always return False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    @property
    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def set_password(self, raw_password):
        self.password = make_password(raw_password)
        self._password = raw_password

    def check_password(self, raw_password):
        """
        Return a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        def setter(raw_password):
            self.set_password(raw_password)
            # Password hash upgrades shouldn't be considered password changes.
            self._password = None
            self.save(update_fields=["password"])
        return check_password(raw_password, self.password, setter)

    def set_unusable_password(self):
        # Set a value that will never be a valid hash
        self.password = make_password(None)

    def has_usable_password(self):
        """
        Return False if set_unusable_password() has been called for this user.
        """
        return is_password_usable(self.password)

    def _legacy_get_session_auth_hash(self):
        # RemovedInDjango40Warning: pre-Django 3.1 hashes will be invalid.
        key_salt = 'django.contrib.auth.models.AbstractBaseUser.get_session_auth_hash'
        return salted_hmac(key_salt, self.password).hexdigest()

    def get_session_auth_hash(self):
        """
        Return an HMAC of the password field.
        """
        key_salt = "django.contrib.auth.models.AbstractBaseUser.get_session_auth_hash"
        return salted_hmac(
            key_salt,
            self.password
        ).hexdigest()

    @classmethod
    def get_email_field_name(cls):
        try:
            return cls.EMAIL_FIELD
        except AttributeError:
            return 'email'

    @classmethod
    def normalize_username(cls, username):
        return unicodedata.normalize('NFKC', username) if isinstance(username, str) else username


class PlatformUserManager(BaseUserManager):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """
    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('El correo es un campo obligatorio.'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(_('Correo electrónico'), unique=True)

    name = models.CharField(
        verbose_name=_('Nombres'),
        max_length=128,
        blank=True,
        null=True
    )

    lastname = models.CharField(
        verbose_name=_('Apellido paterno'),
        max_length=128,
        blank=True,
        null=True
    )

    second_lastname = models.CharField(
        verbose_name=_('Apellido materno'),
        max_length=128,
        blank=True,
        null=True
    )

    display_mode = models.CharField(
        verbose_name=_('Modo de presentación del nombre'),
        max_length=8,
        choices=constants.USER_DISPLAY_MODE_FOR_NAME_CHOICES,
        default=constants.USER_DISPLAY_MODE_FOR_NAME_PE
    )

    is_staff = models.BooleanField(
        verbose_name=_('¿Es parte del equipo?'),
        default=False
    )

    is_active = models.BooleanField(
        verbose_name=_('¿Cuenta activa?'),
        default=True
    )

    is_researcher = models.BooleanField(
        verbose_name=_('¿Es investigador?'),
        default=False
    )

    is_interviewer = models.BooleanField(
        verbose_name=_('¿Es entrevistador?'),
        default=True
    )

    date_joined = models.DateTimeField(default=localtime)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = PlatformUserManager()

    class Meta:
        verbose_name = _('Usuario')
        verbose_name_plural = _('Usuarios')
        ordering = ('name', 'lastname', 'second_lastname')

    def __str__(self):
        return self.fullname

    @property
    def action_list(self):
        return ('change', 'delete')

    def get_absolute_url(self):
        return reverse_lazy('panel:customuser_change', args=[self.uuid])

    def save(self, *args, **kwargs):
        # FIXME: Find a better solution
        if not self.password.startswith('pbkdf2_sha256'):
            self.set_password(self.password)
        return super().save(*args, **kwargs)

    def get_user_display_template(self, mode=None):
        if mode is None:
            mode = self.display_mode
        return constants.USER_DISPLAY_MODE_TEMPLATE_MAP[mode]

    def get_full_name(self, mode=None):
        template = self.get_user_display_template(mode)
        return template.format(
            name=self.name,
            lastname=self.lastname,
            second_lastname=self.second_lastname
        ).strip()

    def get_full_name_us(self):
        return self.get_full_name(mode=constants.USER_DISPLAY_MODE_FOR_NAME_US)

    def get_full_name_pe(self):
        return self.get_full_name(mode=constants.USER_DISPLAY_MODE_FOR_NAME_PE)

    def get_full_name_formal(self):
        return self.get_full_name(mode=constants.USER_DISPLAY_MODE_FOR_NAME_FORMAL)

    @property
    def fullname(self):
        return self.get_full_name()

    @property
    def initials(self):
        return get_initials(self.name, self.last_login, self.second_lastname)

    @property
    def slug_from_email(self):
        return slugify_email(self.email)
