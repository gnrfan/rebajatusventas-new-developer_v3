from django.utils.translation import ugettext_lazy as _

IDENTITY_DOCUMENT_TYPE_CE = 'ce'
IDENTITY_DOCUMENT_TYPE_DNI = 'dni'
IDENTITY_DOCUMENT_TYPE_PASAPORTE = 'pasaporte'

IDENTITY_DOCUMENT_TYPE_CHOICES = (
    (IDENTITY_DOCUMENT_TYPE_DNI, _("DNI")),
    (IDENTITY_DOCUMENT_TYPE_CE, _("Carnet de extranjería")),
    (IDENTITY_DOCUMENT_TYPE_PASAPORTE, _("Pasaporte"))
)

USER_DISPLAY_MODE_FOR_NAME_PE = 'pe'
USER_DISPLAY_MODE_FOR_NAME_FORMAL = 'formal'

USER_DISPLAY_MODE_FOR_NAME_CHOICES = (
    (USER_DISPLAY_MODE_FOR_NAME_PE, _("{nombres} {apellido_paterno} {apellido_materno} (Perú)")),
    (USER_DISPLAY_MODE_FOR_NAME_FORMAL, _("{apellido_paterno} {apellido_materno}, {nombres} (Formal)")),
)

USER_DISPLAY_MODE_TEMPLATE_PE = '{name} {lastname} {second_lastname}'
USER_DISPLAY_MODE_TEMPLATE_FORMAL = '{lastname} {second_lastname}, {name}'

USER_DISPLAY_MODE_TEMPLATE_MAP = {
    USER_DISPLAY_MODE_FOR_NAME_PE: USER_DISPLAY_MODE_TEMPLATE_PE,
    USER_DISPLAY_MODE_FOR_NAME_FORMAL: USER_DISPLAY_MODE_TEMPLATE_FORMAL
}

DEFAULT_RANK_VALUE = 100