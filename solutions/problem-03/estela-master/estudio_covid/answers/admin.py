from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from common.admin import BaseModelAdmin, BaseTabularInline
from .models import Questionnaire, Answer

class AnswerInline(BaseTabularInline):
    fields = ['question', 'selected_entries', 'value']
    model = Answer
    extra = 1

class QuestionnaireAdmin(BaseModelAdmin):

    list_display = [
        'subject_identifier',
        'control_case',
        'is_control_case'
    ]

    inlines = [AnswerInline]

    def answer_count(self, instance):
        return instance.answers.count()
    answer_count.short_description = _('Número de respuestas')

    def completion_percentage(self, instance):
        return instance.completion_percentage_formatted()
    completion_percentage.short_description = _("Porcentaje de respuestas")

    def is_control_case(self, instance):
        return instance.is_control_case()
    is_control_case.short_description = _("¿Es caso de control?")
    is_control_case.boolean = True


admin.site.register(Questionnaire, QuestionnaireAdmin)