from django.db import models
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from common.models import BaseModel
from questions.models import Question, ChoiceEntry

class Questionnaire(BaseModel):

    subject_identifier = models.CharField(
        verbose_name=_("Nombre y/o identificador único"),
        max_length=255,
        unique=True
    )

    control_case = models.OneToOneField('self',
        verbose_name=_("Caso de control"),
        related_name='control_case_pair',
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = _("Cuestionario")
        verbose_name_plural = _("Cuestionarios")

    def __str__(self):
        return self.subject_identifier

    @property
    def action_list(self):
        completion_percentage = self.completion_percentage()
        if completion_percentage == 100:
            return ()
        elif completion_percentage == 90:
            return ('resolve', )
        else:
            return ('resolve', 'change', 'delete')

    def completion_percentage(self):
        return self.answers.count() / Question.objects.count()

    def completion_percentage_formatted(self):
        return "{percentage:.2f}%%" % {'percentage': self.completion_percentage()}

    def is_control_case(self):
        try:
            self.control_case_pair
            return True
        except self._meta.model.control_case_pair.RelatedObjectDoesNotExist:
            return False

    def get_absolute_url(self):
        return reverse_lazy('panel:questionnaire_detail', args=[self.pk])



class Answer(BaseModel):

    questionnaire = models.ForeignKey(Questionnaire,
        verbose_name=_("Cuestionario"),
        related_name='answers',
        on_delete=models.PROTECT
    )

    question = models.ForeignKey(Question,
        verbose_name=_("Pregunta"),
        related_name='answers',
        on_delete=models.PROTECT
    )

    selected_entries = models.ManyToManyField(ChoiceEntry,
        verbose_name=_("Alternativas seleccionadas"),
        blank=True
    )

    value = models.TextField(
        verbose_name=_("Valor de la respuesta"),
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = _("Respuesta")
        verbose_name_plural = _("Respuestas"),
        unique_together = [('questionnaire', 'question')]
