from django.utils.translation import ugettext_lazy as _

QUESTION_TYPE_INTEGER = 'integer'
QUESTION_TYPE_FLOAT = 'float'
QUESTION_TYPE_DATE = 'date'
QUESTION_TYPE_BOOLEAN = 'boolean'
QUESTION_TYPE_SINGLE_CHOICE = 'single_choice'
QUESTION_TYPE_MULTIPLE_CHOICE = 'multiple_choice'

QUESTION_TYPE_CHOICES = (
    (QUESTION_TYPE_INTEGER, _("Número entero")),
    (QUESTION_TYPE_FLOAT, _("Número decimal")),
    (QUESTION_TYPE_DATE, _("Fecha")),
    (QUESTION_TYPE_BOOLEAN, _("Si / No")),
    (QUESTION_TYPE_SINGLE_CHOICE, _("Alternativa única")),
    (QUESTION_TYPE_MULTIPLE_CHOICE, _("Alternativas múltiples"))
)

QUESTION_FIELD_MAPPING = {
    QUESTION_TYPE_INTEGER: [
        {
            'field': 'min_value',
            'optional': True
        },
        {
            'field': 'max_value',
            'optional': True
        }
    ],
    QUESTION_TYPE_FLOAT: [
        {
            'field': 'min_value',
            'optional': True
        },
        {
            'field': 'max_value',
            'optional': True
        }
    ],
    QUESTION_TYPE_SINGLE_CHOICE: [
        {
            'field': 'other_answer',
            'optional': True
        }
    ],
    QUESTION_TYPE_MULTIPLE_CHOICE: [
        {
            'field': 'other_answer',
            'optional': True
        }
    ]
}

DIRECT_VALUE_QUESTION_TYPES = [
    QUESTION_TYPE_INTEGER,
    QUESTION_TYPE_FLOAT,
    QUESTION_TYPE_DATE,
    QUESTION_TYPE_BOOLEAN
]

CHOICE_VALUE_QUESTION_TYPES = [
    QUESTION_TYPE_SINGLE_CHOICE,
    QUESTION_TYPE_MULTIPLE_CHOICE
]