from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from common.admin import BaseModelAdmin, BaseTabularInline
from .models import Question, ChoiceEntry

class ChoiceEntryInline(BaseTabularInline):
    fields = ['text', 'ordering_position']
    model = ChoiceEntry
    extra = 5

class QuestionAdmin(BaseModelAdmin):

    list_display = [
        'variable_name',
        'text',
        'question_type',
        'ordering_position',
        'other_answer',
        'choice_entries_count'
    ]

    list_filter = ['question_type', 'other_answer']

    inlines = [ChoiceEntryInline]

    def choice_entries_count(self, instance):
        return instance.choice_entries.count()
    choice_entries_count.short_description = _('Número de alternativas')

admin.site.register(Question, QuestionAdmin)