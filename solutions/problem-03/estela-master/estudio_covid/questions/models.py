from django.db import models
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from common.models import BaseModel
from . import constants

# Create your models here.

class Question(BaseModel):

    text = models.TextField(
        verbose_name=_("Texto de la pregunta"),
    )

    question_type = models.CharField(
        verbose_name=_("Tipo de pregunta"),
        max_length=16,
        choices=constants.QUESTION_TYPE_CHOICES
    )

    interviewer_instructions = models.TextField(
        verbose_name=_("Indicaciones para el entrevistador"),
        help_text=_("Información adicional de utilidad sobre la pregunta."),
        null=True,
        blank=True
    )

    ordering_position = models.PositiveIntegerField(
        verbose_name=_("Orden de aparición en el cuestionario"),
        help_text=_("Indique un valor entero positivo único para cada pregunta."),
        unique=True
    )

    min_value = models.IntegerField(
        verbose_name=_("Valor mínimo permitido"),
        help_text=_("Indique un valor entero para fines de validación."),
        null=True,
        blank=True
    )

    max_value = models.IntegerField(
        verbose_name=_("Valor máximo permitido"),
        help_text=_("Indique un valor entero para fines de validación."),
        null=True,
        blank=True
    )

    other_answer = models.BooleanField(
        verbose_name=_("¿Aceptar otra respuesta?"),
        default=False
    )

    variable_name = models.SlugField(
        verbose_name=_("Nombre de variable"),
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = _("Pregunta")
        verbose_name_plural = _("Preguntas")
        ordering = ('ordering_position', 'created_at')

    def __str__(self):
        return self.text

    @property
    def action_list(self):
        return ('change', 'delete')

    def get_absolute_url(self):
        return reverse_lazy('panel:question_detail', args=[self.pk])

    def save(self, *args, **kwargs):
        if self.variable_name is None:
            self.variable_name = self.generate_default_variable_name()
        return super().save(*args, **kwargs)

    def generate_default_variable_name(self):
        num = self._meta.model.objects.count() + 1
        return f"var_{num}"


class ChoiceEntry(BaseModel):

    question = models.ForeignKey(Question,
        verbose_name=_("Pregunta"),
        related_name='choice_entries',
        on_delete=models.PROTECT
    )

    text = models.TextField(
        verbose_name=_("Texto de la alternativa"),
    )

    ordering_position = models.PositiveIntegerField(
        verbose_name=_("Orden de aparición dentro de la pregunta"),
        help_text=_("Indique un valor entero positivo único para cada alternativa.")
    )

    class Meta:
        verbose_name = _("Alternativa")
        verbose_name_plural = _("Alternativas")
        ordering = ('ordering_position', 'created_at')
        unique_together = [('question', 'ordering_position')]

    def __str__(self):
        return self.text