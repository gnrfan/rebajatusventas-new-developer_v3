import random
import pprint

START_OF_RANGE = 1
END_OF_RANGE = 100_000_00

def generate_number_list(size=1_000_000, sorted=True):
    results = []
    for i in range(size):
        results.append(random.randint(START_OF_RANGE, END_OF_RANGE+1))
    if sorted:
        results.sort()
    return results

def pick_pair(number_list):
    return tuple(random.sample(number_list, 2))

if __name__ == '__main__':
    list1 = generate_number_list(100)
    print("List:\n")
    pprint.pprint(list1)
    pair = pick_pair(list1)
    print(f"Pair: {pair}")
    