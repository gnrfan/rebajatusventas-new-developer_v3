def find_pair(number_list, desired_sum):
    """
    This functions uses two pointers in order to find the pairs
    that satisfy the desired sum more efficiently.

    Time complexity is O(n).        
    """

    left_pointer = 0
    right_pointer = len(number_list) - 1

    while left_pointer < right_pointer:
        current_sum = number_list[left_pointer] + number_list[right_pointer]
        if current_sum == desired_sum:
            return number_list[left_pointer], number_list[right_pointer]
        elif current_sum < desired_sum:
            left_pointer += 1
        else:
            right_pointer -= 1
    
    return None