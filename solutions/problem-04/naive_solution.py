def find_pair(number_list, desired_sum):
    """
    This function compares each pair using nested loops
    so its time complexity is O(n^2).
    """
    for i in range(len(number_list)):
        for j in range(i + 1, len(number_list)):
            if number_list[i] + number_list[j] == desired_sum:
                pair = (number_list[i], number_list[j])
                return pair
    return None