# Solution

## Running tests

```
python -m unittest tests
```

## Running the code for comparing time among solutions

```
python runner.py
```