import inspect
import unittest
from naive_solution import find_pair as naive_find_pair
from efficient_solution import find_pair as efficient_find_pair

class TestFindPair(unittest.TestCase):
    def __init__(self, methodName, find_pair=None):
        super().__init__(methodName)
        self.find_pair = find_pair

    def test_pair_exists(self):
        """
        Test case for when a pair exists in the list that sums up to the desired sum
        """
        number_list = [1, 2, 3, 4, 5]
        desired_sum = 7
        expected_pair = (2, 5)
        
        result = self.find_pair(number_list, desired_sum)
        self.assertIsNotNone(result)
        self.assertIn(result, [expected_pair, (5, 2)])

    def test_pair_does_not_exist(self):
        """
        Test case for when no pair exists in the list that sums up to the desired sum
        """
        number_list = [1, 2, 3, 4, 5]
        desired_sum = 12
        
        result = self.find_pair(number_list, desired_sum)
        self.assertIsNone(result)

    def test_empty_list(self):
        """
        Test case for when the input list is empty
        """
        number_list = []
        desired_sum = 10
        
        result = self.find_pair(number_list, desired_sum)
        self.assertIsNone(result)

    def test_single_element_list(self):
        """
        Test case for when the input list has only one element
        """
        number_list = [5]
        desired_sum = 10
        
        result = self.find_pair(number_list, desired_sum)
        self.assertIsNone(result)


if __name__ == '__main__':
    suite = unittest.TestSuite()
    name_method_pairs = inspect.getmembers(
        TestFindPair,
        predicate=inspect.isfunction
    )
    for name, method in name_method_pairs:
        if name.startswith('test_'):
            test_case = TestFindPair(name, naive_find_pair)
            suite.addTest(test_case)
            test_case = TestFindPair(name, efficient_find_pair)
            suite.addTest(test_case)

    runner = unittest.TextTestRunner()
    runner.run(suite)