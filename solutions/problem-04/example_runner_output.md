# Example output

```
python runner.py 
Naive solution: 
Average execution time per call: 0.000006 seconds
Average execution time per call: 0.000007 seconds
Matching pair: (7999031, 9264548)
Average execution time per call: 2.951768 seconds
Efficient solution: 
Average execution time per call: 0.000005 seconds
Average execution time per call: 0.000004 seconds
Matching pair: (1904321, 8405717)
Average execution time per call: 0.006919 seconds
```