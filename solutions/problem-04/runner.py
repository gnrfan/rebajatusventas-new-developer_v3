import timeit
from util import (
    generate_number_list,
    pick_pair
)
from naive_solution import find_pair as naive_find_pair
from efficient_solution import find_pair as efficient_find_pair


NUM_TIMES = 1
SIZE_OF_LIST = 10_000


def first_example(find_pair, verbose=False):
    number_list = [2, 3, 6, 7]
    desired_sum = 9
    solution = find_pair(number_list, desired_sum)
    if verbose:
        if solution:
            print(f"Matching pair: {solution}")
        else:
            print("No solution")


def second_example(find_pair, verbose=False):
    number_list = [1, 3, 3, 7]
    desired_sum = 9
    solution = find_pair(number_list, desired_sum)
    if verbose:
        if solution:
            print(f"Matching pair: {solution}")
        else:
            print("No solution")

def third_example(find_pair, verbose=False):
    number_list = generate_number_list(SIZE_OF_LIST)
    pair_for_sum = pick_pair(number_list)
    desired_sum = sum(pair_for_sum)
    solution = find_pair(number_list, desired_sum)
    if verbose:
        if solution:
            print(f"Matching pair: {solution}")
        else:
            print("No solution")    


if __name__ == '__main__':
    print("Naive solution: ")
    execution_time = timeit.timeit(
        lambda: first_example(naive_find_pair), number=NUM_TIMES
    ) / NUM_TIMES
    print(f"Average execution time per call: {execution_time:.6f} seconds")
    execution_time = timeit.timeit(
        lambda: second_example(naive_find_pair), number=NUM_TIMES
    ) / NUM_TIMES
    print(f"Average execution time per call: {execution_time:.6f} seconds")
    execution_time = timeit.timeit(
        lambda: third_example(naive_find_pair, verbose=True), number=NUM_TIMES
    ) / NUM_TIMES
    print(f"Average execution time per call: {execution_time:.6f} seconds")
    print("Efficient solution: ")
    execution_time = timeit.timeit(
        lambda: first_example(efficient_find_pair), number=NUM_TIMES
    ) / NUM_TIMES
    print(f"Average execution time per call: {execution_time:.6f} seconds")
    execution_time = timeit.timeit(
        lambda: second_example(efficient_find_pair), number=NUM_TIMES
    ) / NUM_TIMES
    print(f"Average execution time per call: {execution_time:.6f} seconds")
    execution_time = timeit.timeit(
        lambda: third_example(efficient_find_pair, verbose=True), number=NUM_TIMES
    ) / NUM_TIMES
    print(f"Average execution time per call: {execution_time:.6f} seconds")